import React from 'react'

import{
  createAppContainer,
  createSwitchNavigator
} from 'react-navigation'

import { createStackNavigator,TransitionPresets } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'

import LoginScreen from './src/screens/LoginScreen'
import RegistrationEmailScreen from './src/screens/RegistrationEmailScreen'
import RegistrationEmailConfirmScreen from './src/screens/RegistrationEmailConfirmScreen'
import RegistrationFinishedScreen from './src/screens/RegistrationFinishedScreen'
import ForgotPasswordScreen from './src/screens/ForgotPasswordScreen'

import ExploreScreen from './src/screens/home/ExploreScreen'
import SavedScreen from './src/screens/home/SavedScreen'
import EventsScreen from './src/screens/home/EventsScreen'
import CreateEvent from './src/screens/home/event/CreateEvent'
import EventDetailScreen from './src/screens/home/event/EventDetailScreen'
import BookingScreen from './src/screens/home/BookingScreen'
import ProfileScreen from './src/screens/home/ProfileScreen'
import ProductBundleDetailScreen from './src/screens/home/explore/ProductBundleDetailScreen'
import ProductItemDetailScreen from './src/screens/home/explore/ProductItemDetailScreen'
import StartBookingScreen from './src/screens/home/explore/StartBookingScreen'
import RatingScreen from './src/screens/home/explore/RatingScreen'
import OrganizationDetailScreen from './src/screens/home/explore/OrganizationDetailScreen'
import SearchListScreen from './src/screens/home/explore/SearchListScreen'
import SearchListScreenCategory from './src/screens/home/explore/SearchListScreenCategory'
import SearchListSubCategoryScreen from './src/screens/home/explore/SearchListSubCategoryScreen'
import BookingDetailScreen from './src/screens/home/booking/BookingDetailScreen'
import PayBookingScreen from './src/screens/home/booking/PayBookingScreen'
import PayFailCardScreen from './src/screens/home/booking/PayFailCardScreen'
import PaySuccessCardScreen from './src/screens/home/booking/PaySuccessCardScreen'
import SearchListPriceRangeScreen from './src/screens/home/explore/SearchListPriceRangeScreen'
import ShowProfileScreen from './src/screens/home/profile/ShowProfileScreen'
import EditProfileInfoScreen from './src/screens/home/profile/EditProfileInfoScreen'

import {setNavigator} from './src/navigationRef'
import {Provider} from 'react-redux'
import {createStore,applyMiddleware} from 'redux'
import reducers from './src/reducers'
import ReduxThunk from 'redux-thunk'

const switchNavigator = createSwitchNavigator({
  loginFlow:createStackNavigator({
    Login:LoginScreen,
    Registration:RegistrationEmailScreen,
    Forgot:ForgotPasswordScreen,
    EmailConfirm:RegistrationEmailConfirmScreen,
    RegistrationFinished:RegistrationFinishedScreen
  },{
    defaultNavigationOptions: {
      ...TransitionPresets.SlideFromRightIOS,
    },
  }),
  mainFlow:createBottomTabNavigator({
    Explore: ExploreScreen,
    Saved: SavedScreen,
    Events: EventsScreen,
    Bookings:BookingScreen,
    Profile:ProfileScreen
  }),
  exploreFlow: createStackNavigator({
    ProductBundleDetail: ProductBundleDetailScreen,
    ProductItemDetail: ProductItemDetailScreen,
    RatingScreen: RatingScreen,
    OrganizationDetailScreen:OrganizationDetailScreen,
    StartBookingScreen:StartBookingScreen,
  }),
  searchProductFlow: createStackNavigator({
    SearchListScreen:SearchListScreen,
    SearchListScreenCategory:SearchListScreenCategory,
    SearchListSubCategoryScreen:SearchListSubCategoryScreen,
    SearchListPriceRangeScreen:SearchListPriceRangeScreen
  },{
    defaultNavigationOptions: {
      ...TransitionPresets.SlideFromRightIOS,
    },
  }),
  eventFlow: createSwitchNavigator({
    CreateEvent: CreateEvent,
    EventDetailScreen:EventDetailScreen
  },{
    defaultNavigationOptions: {
      ...TransitionPresets.SlideFromRightIOS,
    },
  }),
  bookingFlow: createSwitchNavigator({
    BookingDetailScreen: BookingDetailScreen,
    PayBookingScreen:PayBookingScreen,
    PayFailCardScreen:PayFailCardScreen,
    PaySuccessCardScreen:PaySuccessCardScreen
  },{
    defaultNavigationOptions: {
      ...TransitionPresets.SlideFromRightIOS,
    },
  }),
  profileFlow: createSwitchNavigator({
    ShowProfileScreen: ShowProfileScreen,
    EditProfileInfoScreen:EditProfileInfoScreen
  },{
    defaultNavigationOptions: {
      ...TransitionPresets.SlideFromRightIOS,
    },
  })
})

const App =  createAppContainer(switchNavigator)

export default () => {
  const store = createStore(reducers, {},applyMiddleware(ReduxThunk))
  return (
    <Provider store={store}>
      <App ref={(navigator)=>{setNavigator(navigator)}} />
    </Provider>
  )
}