export const PENDING = {displayName:'Pending',value:'PENDING'}
export const CANCELLED = {displayName:'Cancel/Reject',value:'CANCEL_REJECT'}
export const APPROVED = {displayName:'Approved',value:'APPROVED'}
export const BOOKED = {displayName:'Booked',value:'BOOKED'}