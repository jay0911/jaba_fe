import {
    EMAIL_CHANGED,
    GENERIC_CHANGE,
    PROFILE_CHANGE,
    SEND_EMAIL,
    SENT_EMAIL,
    LOADING_FAILED,
    LOADING_FAILED_ERRMSG,
    CODE_CHANGED,
    SENT_VERIFY_CODE,SEND_VERIFY_CODE,RESET_CONFIRM,
    SEND_REGISTRATION,RESET_REGISTRATION,
    LOGIN_FORM_CHANGE,
    SEND_LOGIN,
    LOADING_FAILED_ERRMSG_CONFIRM,LOADING_FAILED_ERRMSG_EMAIL,LOADING_FAILED_ERRMSG_CODE,
    FORGOT_EMAIL_SEND,FORGOT_EMAIL_SENT,
    PRODUCT_LOAD_MORE,PRODUCT_DONT_LOAD_MORE,PRODUCT_LOAD_MORE_TRUE,INITIAL_PRODUCT_BUNDLE,
    SELECT_PRODUCT_BUNDLE,
    GENERIC_EVENT_CHANGE,
    LOAD_PROVINCE,
    LOAD_MUNICIPALITY,
    LOAD_BARANGAY,
    SAVE_EVENT,
    DISPLAY_ERR_MODAL_EVENT,
    EVENT_MODAL_CLOSE,
    RESET_EVENT,
    LOAD_EVENT_TYPE,
    SET_EVENT_ACC_ID,
    REFRESH_EVENT,
    LOAD_PLANNING_EVENT,
    LOAD_CANCELLED_EVENT,
    LOAD_HAPPENING_EVENT,
    LOAD_PAST_EVENT,
    INIT_BOOKING_DETAIL_REDUCER,INIT_EVENT_LIST_REDUCER,INIT_EVENT_REDUCER,
    GENERIC_ID_REDUCER_CHANGE,RESET_ID_REDUCERS,
    LOAD_EVENT_DETAIL,CANCEL_EVENT,OPEN_CANCEL_EVENT_MODAL,LOAD_EVENT_DETAIL_CANCEL,
    CHECK_USER_HAS_EVENT,RESET_CHECK_USER_HAS_EVENT,
    SET_BOOKING_FINAL_MODAL,RESET_BOOKING,SET_BOOKING_WARNING_MODAL,SET_BOOKING_ITEMREMOVAL_MODAL,SET_BOOKING_DEFAULT,LOAD_EVENT_BOOKING_DROPDOWN,GENERIC_BOOKING_CHANGE,CHOSE_BOOKING_EVENT,COMPUTE_BOOKING_AMOUNT,SET_BARGAIN_ITEMS,
    PRODUCTITEM_SAVED_LIST_CHANGE_PROPERTY,PRODUCT_SAVED_LIST_CHANGE_PROPERTY,PRODUCT_LIST_CHANGE_PROPERTY,INITIAL_PRODUCT_BUNDLE_SAVED,PRODUCT_LOAD_MORE_SAVED,PRODUCT_LOAD_MORE_TRUE_SAVED,PRODUCT_DONT_LOAD_MORE_SAVED,SET_PAID_INFO_BOOKING,CHANGE_CHECKOUT_FLAG,SAVE_CARD_PAYMENT_DETAILS,GENERIC_BOOKING_DETAIL_CHANGE,PAY_BOOKING_MODAL,CANCEL_BOOKING_MODAL,LOAD_CANCELREJECT_BOOKING,LOAD_BOOKED_BOOKING,LOAD_APPROVED_BOOKING,LOAD_PENDING_BOOKING,SELECT_BOOKING,
    PRODUCT_SELECTED_LOAD_BUNDLE,PRODUCT_SELECTED_LOAD_BUNDLE_ITEMS,PRODUCT_SELECTED_LOAD_BUNDLE_ORG,PRODUCT_SELECTED_LOAD_BUNDLE_OTHER_PRODUCTS,
    GENERIC_SEARCH_CHANGE,INIT_PRODUCT_LOAD_MORE_SAVED,INIT_PRODUCT_LOAD_MORE,GENERIC_LOADER,
    RESET_SEARCH,GENERIC_PROFILE_CHANGE,RESET_PROFILE_CHANGE,GENERIC_PROFILE_EDIT_CHANGE,PROFILE_INFORMATION_EDIT_CHANGE, PROFILE_INFORMATION_EDIT_CHANGE_WITHOUTCONTACT} from './types'

import {createSession,sendEmail,verifyEmail,register,login,forgotPassword,
    findProductBundle,
    findItemList,
    getOrgDetails,
    getProvince,
    getMunicipality,
    getBarangay,
    getEventTypes,
    saveEvent,
    eventListing,
    refreshToken,
    updateEventStatus,
    checkUserHasEvent,
    eventBookingDropDown,
    saveBooking,
    bookingListing,
    updateBooking,
    payCheckout,
    verifyPayment,
    getBookedPaymentDetails,
    getAccountBookingCount,
    getCategory,
    findProductSaved,
    likeProduct,
    unlikeProduct,
    addressProductSearch,
    getSubCategoryListingFromCat,
    getSubCategoryListingApi,
    findProductBundleWithoutSavedUserMarked,
    prefillProfile,
    editProfile
} from '../api/apis'
import AsyncStorage from '@react-native-community/async-storage'

export const selectLibrary = (id) => {
    return {
        type: 'select_library',
        payload: id
    }
}


export const reset = () => {
    return {
        type: RESET_REGISTRATION
    }
}

export const resetBooking = () => {
    return {
        type: RESET_BOOKING
    }
}

export const initBookingDetail = () => {
    return {
        type: INIT_BOOKING_DETAIL_REDUCER
    }
}

export const initEventList = () => {
    return {
        type: INIT_EVENT_LIST_REDUCER
    }
}

export const initEvents = () => {
    return {
        type: INIT_EVENT_REDUCER
    }
}

export const resetEvent = () => {
    return {
        type: RESET_EVENT
    }
}

export const resetSearch = () => {
    return {
        type: RESET_SEARCH
    }
}

export const refreshEvent = () => {
    return {
        type: REFRESH_EVENT
    }
}

export const resetConfirm = () => {
    return {
        type: RESET_CONFIRM
    }
}

export const emailChanged = (email) => {
    return {
        type: EMAIL_CHANGED,
        payload: email
    }
}

export const setEventAccountId = (accountId) => {
    return {
        type: SET_EVENT_ACC_ID,
        payload: accountId
    }
}

export const openCancelEventModal = () => {
    return {
        type: OPEN_CANCEL_EVENT_MODAL
    }
}

export const closeCancelEventModal = () => {
    return {
        type: CANCEL_EVENT
    }
}

export const resetModalCheckUserHasEvent = () => {
    return {
        type: RESET_CHECK_USER_HAS_EVENTfindProduct
    }
}

export const computeBooking = (newPrice) => {
    return{
        type:COMPUTE_BOOKING_AMOUNT,
        payload:newPrice
    }
}

export const replaceItems = (items) => {
    return{
        type:SET_BARGAIN_ITEMS,
        payload:items
    }
}

export const setBookingWarningModal = (modal,errMsg) => {
    return {
        type:SET_BOOKING_WARNING_MODAL,
        payload:{
            showModal:modal,
            errMsg:errMsg
        }
    }
}

export const setBookingFinalModal = (modal,errMsg) => {
    return {
        type:SET_BOOKING_FINAL_MODAL,
        payload:{
            showModal:modal,
            errMsg:errMsg
        }
    }
}

export const setCancelBookingModal = (modal,errMsg) => {
    return {
        type:CANCEL_BOOKING_MODAL,
        payload:{
            showModal:modal,
            errMsg:errMsg
        }
    }
}

export const payModal = (modal,errMsg) => {
    return {
        type:PAY_BOOKING_MODAL,
        payload:{
            showModal:modal,
            errMsg:errMsg
        }
    }
}

export const addressProductSearchApi = (value) => {
    return (dispatch) => {
        if(value.length!=0){
            var request = `{"filterBy": {"matches": [{"key": "address","value": "${value}"}]},"pagination": { "from": 0,"size": 10}}`
            addressProductSearch(request).then(async respose => {
                console.log(respose.data.data.address)
                dispatch({type: GENERIC_SEARCH_CHANGE,payload:{prop:'searchAddressProductList',value:respose.data.data.address}})
            }).catch(respose => {      
                createSession();
            })
        }
    }
}

export const updateMyBooking = (navigation,screen,id,status) => {
    return (dispatch) => {
        dispatch({type: GENERIC_BOOKING_DETAIL_CHANGE,payload:{prop:'loading',value:true}})
        updateBooking(id,status).then(async respose => {
            navigation.navigate(screen)
            dispatch({type: GENERIC_BOOKING_DETAIL_CHANGE,payload:{prop:'loading',value:false}})
        }).catch(respose => {      
            createSession();
        })
    }
}

export const setBookingItemRemovalModal = (modal,errMsg) => {
    return {
        type:SET_BOOKING_ITEMREMOVAL_MODAL,
        payload:{
            showModal:modal,
            errMsg:errMsg
        }
    }
}

export const changeCheckoutFlagPaymentCard = (flag) => {
    return {
        type: CHANGE_CHECKOUT_FLAG,
        payload:flag
    }
}

export const payCheckoutCardNow = (navigation,screen,accountId,amount,bookingId) => {
    return (dispatch) => {
        var request = {accountId,amount,bookingId};
        dispatch({type: GENERIC_BOOKING_DETAIL_CHANGE,payload:{prop:'loading',value:true}})
        payCheckout(request).then(async respose => {
            dispatch({type: GENERIC_BOOKING_DETAIL_CHANGE,payload:{prop:'checkoutId',value:respose.data.data}})
            navigation.navigate(screen)
            dispatch({type: GENERIC_BOOKING_DETAIL_CHANGE,payload:{prop:'loading',value:false}})
        }).catch(respose => {      
            createSession();
        })
    }
}

export const getPaidDetailsBooking = (bookingId) => {
    return (dispatch) => {
        getBookedPaymentDetails(bookingId).then(async respose => {
            dispatch({type: SET_PAID_INFO_BOOKING,payload:respose.data.data})
        }).catch(respose => {      
            createSession();
        })
    }
}

export const verifyMyCardPayment = (navigation,checkoutId) => {
    return (dispatch) => {
        var request = {checkoutId};
        verifyPayment(request).then(async respose => {
            dispatch({type: SAVE_CARD_PAYMENT_DETAILS,payload:respose.data.data})
            navigation.navigate('PaySuccessCardScreen')
        }).catch(respose => {      
            navigation.navigate('PayFailCardScreen')
        })
    }
}

export const bookMePlease = (navigation,screen,request) => {
    return (dispatch) => {
        console.log("BOOOOOOOOOKING THIS PRODUCt----------------")
        console.log(request)
        dispatch({type: GENERIC_BOOKING_CHANGE,payload:{prop:'loading',value:true}})
        saveBooking(request).then(async respose => {
            navigation.navigate(screen)
            dispatch({type: GENERIC_BOOKING_CHANGE,payload:{prop:'eventPage',value:false}})
        }).catch(respose => {      
            createSession();
        })
    }
}

export const checkAccountHasEvent = (accountId,navigation,screen,bundle,itemDetails) => {
    return (dispatch) => {
        checkUserHasEvent(accountId).then(async respose => {

            if(!respose.data.data){
                dispatch({type: CHECK_USER_HAS_EVENT,payload:'Please create you event first before Booking'})
            }else{
                dispatch({type: SET_BOOKING_DEFAULT,payload:{itemDetails,bundle}})
                navigation.navigate(screen) 
            }
        }).catch(respose => {      
            createSession();
        })
    }
}

export const updateYourEventStatus = (eventId,eventStatus,navigation,screen) => {
    return (dispatch) => {
        var request = {  
            "eventId": eventId,
            "status": eventStatus
        };
        updateEventStatus(request).then(async respose => {
            navigation.navigate(screen) 
            dispatch({type: CANCEL_EVENT})
        }).catch(respose => {      
            createSession();
        })
    }
}

export const loadEventDropDown = () => {
    return (dispatch) => {
        eventBookingDropDown().then(async respose => {
            dispatch({type: LOAD_EVENT_BOOKING_DROPDOWN,payload:respose.data.data})
        }).catch(respose => {      
            createSession();
        })
    }
}

export const loadEvents = (accountId,eventStatus) => {
    return (dispatch) => {
        dispatch({type: GENERIC_LOADER,payload:{prop:'eventPage',value:true}})
        eventListing(accountId,eventStatus).then(async respose => {
            if(eventStatus==='PLANNING'){
                dispatch({type: LOAD_PLANNING_EVENT,payload:respose.data.data})
            }
            if(eventStatus==='CANCELLED'){
                dispatch({type: LOAD_CANCELLED_EVENT,payload:respose.data.data})
            }
            if(eventStatus==='HAPPENING'){
                dispatch({type: LOAD_HAPPENING_EVENT,payload:respose.data.data})
            }
            if(eventStatus==='PAST'){
                dispatch({type: LOAD_PAST_EVENT,payload:respose.data.data})
            }
            dispatch({type: GENERIC_LOADER,payload:{prop:'eventPage',value:false}})
        }).catch(respose => { 
            dispatch({type: GENERIC_LOADER,payload:{prop:'eventPage',value:false}})     
            createSession();
        })
    }
}

export const loadBookings = (accountId,eventStatus) => {
    return (dispatch) => {
        dispatch({type: GENERIC_LOADER,payload:{prop:'bookingPage',value:true}})
        bookingListing(accountId,eventStatus).then(async respose => {
            if(eventStatus==='PENDING'){
                dispatch({type: LOAD_PENDING_BOOKING,payload:respose.data.data})
            }
            if(eventStatus==='APPROVED'){
                dispatch({type: LOAD_APPROVED_BOOKING,payload:respose.data.data})
            }
            if(eventStatus==='BOOKED'){
                dispatch({type: LOAD_BOOKED_BOOKING,payload:respose.data.data})
            }
            if(eventStatus==='CANCEL_REJECT'){
                dispatch({type: LOAD_CANCELREJECT_BOOKING,payload:respose.data.data})
            }
            dispatch({type: GENERIC_LOADER,payload:{prop:'bookingPage',value:false}})
        }).catch(respose => {   
            dispatch({type: GENERIC_LOADER,payload:{prop:'bookingPage',value:false}})   
            createSession();
        })
    }
}

export const saveMyEvent = (dateFrom,dateTo,event,navigation,screen) => {
    return (dispatch) => {
        const {province,municipality,barangay,eventName,otherAddress,eventType,accountId} = event;
        if(!eventName){
            dispatch({type: DISPLAY_ERR_MODAL_EVENT,payload:'Please input Event name'})
            return
        }
        if(!eventType){
            dispatch({type: DISPLAY_ERR_MODAL_EVENT,payload:'Please select Event type'})
            return
        }
        if(!province){
            dispatch({type: DISPLAY_ERR_MODAL_EVENT,payload:'Please select province'})
            return
        }
        if(!municipality){
            dispatch({type: DISPLAY_ERR_MODAL_EVENT,payload:'Please select municipality'})
            return
        }
        if(!barangay){
            dispatch({type: DISPLAY_ERR_MODAL_EVENT,payload:'Please select barangay'})
            return
        }

        var dFrom = new Date(dateFrom.getTime() - dateFrom.getTimezoneOffset()*60*1000);
        var dTo = new Date(dateTo.getTime() - dateTo.getTimezoneOffset()*60*1000);
        var d = new Date();
        d.setHours(0);
        d.setMinutes(0);
        d.setSeconds(0);
        d.setMilliseconds(0);

        var currentDate = new Date(d.getTime() - d.getTimezoneOffset()*60*1000)

        if(currentDate>dFrom){
            dispatch({type: DISPLAY_ERR_MODAL_EVENT,payload:'Date start must not be past date'})
            return
        }

        if(currentDate>dTo){
            dispatch({type: DISPLAY_ERR_MODAL_EVENT,payload:'Date end must not be past date'})
            return
        }

        if(dFrom>dTo){
            dispatch({type: DISPLAY_ERR_MODAL_EVENT,payload:'Date start must not be greater than date end'})
            return
        }

        var request = {  
            "accountId": accountId,
            "barangayCode": barangay,
            "eventEndDtime": dTo,
            "eventName": eventName,
            "eventStartDtime": dFrom,
            "eventTypeCode": eventType,
            "municipalityCode": municipality,
            "otherAddress": otherAddress,
            "provinceCode": province
        };
        dispatch({type: GENERIC_EVENT_CHANGE,payload:{prop:'loading',value:true}})
        saveEvent(JSON.stringify(request)).then(async respose => {
            navigation.navigate(screen)        
            dispatch({type: REFRESH_EVENT})
        }).catch(respose => {      
            createSession();
        })
        
    }
}

export const getaccBookingCount = (accountId,bookingstatus) => {
    return (dispatch) => {
        getAccountBookingCount(accountId,bookingstatus).then(async respose => {
            console.log('hello its my booking count'+respose.data.data)
        }).catch(respose => {      
            createSession();
        })
    }
}

export const registerAcc = (user,navigation,screen) => {
    return (dispatch) => {
        const {firstname,lastname,password,email} = user;
        if(!firstname){
            dispatch({type: LOADING_FAILED_ERRMSG_CONFIRM,payload:'Please input first name'})
            return
        }
        if(!lastname){
            dispatch({type: LOADING_FAILED_ERRMSG_CONFIRM,payload:'Please input last name'})
            return
        }
        if(!password){
            dispatch({type: LOADING_FAILED_ERRMSG_CONFIRM,payload:'Please input password'})
            return
        }

        dispatch({type: SEND_REGISTRATION})

        register(user).then(async respose => {
            dispatch({type:GENERIC_ID_REDUCER_CHANGE,payload:{prop:'accountId',value:respose.data.data}})
            dispatch({type: RESET_REGISTRATION})
            await AsyncStorage.setItem('account_id',respose.data.data)   
            await AsyncStorage.setItem('username',email)   
            await AsyncStorage.setItem('password',password)   
            navigation.navigate(screen)
            createSession();
        }).catch(respose => {  
            if(respose.response.status===417){
                dispatch({type: LOADING_FAILED_ERRMSG_CONFIRM,payload: respose.response.data.message})
            }         
            createSession();
            dispatch({type: LOADING_FAILED})
        })
    }
}

export const productListChangeProperty = (serverData,productId,isSaved,accountId) => {
    return (dispatch) => {
        let newServerData = serverData.map(a => {return {...a}})

        newServerData.find(a => a.id === productId).isSaved = isSaved;

        dispatch({type: GENERIC_LOADER,payload:{prop:'loadProductBundlePage',value:true}})

        if(isSaved==='true'){
            likeProduct({accountId,productId}).then(async respose => {
                dispatch({type:PRODUCT_LIST_CHANGE_PROPERTY,payload:newServerData})

                dispatch({type: GENERIC_LOADER,payload:{prop:'loadProductBundlePage',value:false}})
            }).catch(respose => {  

            })
        }else{
            unlikeProduct({accountId,productId}).then(async respose => {
                dispatch({type:PRODUCT_LIST_CHANGE_PROPERTY,payload:newServerData})

                dispatch({type: GENERIC_LOADER,payload:{prop:'loadProductBundlePage',value:false}})
            }).catch(respose => {  

            })
        }
        
    }
}

export const productSavedListChangeProperty = (serverData,productId,accountId,isSaved) => {
    return (dispatch) => {
        let newServerData = serverData.filter( el => el.id !== productId );
        dispatch({type: GENERIC_LOADER,payload:{prop:'likedPage',value:true}})
        if(isSaved==='true'){
            unlikeProduct({accountId,productId}).then(async respose => {
                dispatch({type:PRODUCT_SAVED_LIST_CHANGE_PROPERTY,payload:newServerData})
                dispatch({type: GENERIC_LOADER,payload:{prop:'likedPage',value:false}})
            }).catch(respose => {  
                dispatch({type: GENERIC_LOADER,payload:{prop:'likedPage',value:false}})
            })
        }

    }
}

export const productItemSaveChangeProperty = (bundle,accountId,isSaved) => {
    return (dispatch) => {
        let newBundleData = {...bundle,isSaved}
    
        if(isSaved==='true'){
            likeProduct({accountId,productId:newBundleData.id}).then(async respose => {
                dispatch({type:PRODUCTITEM_SAVED_LIST_CHANGE_PROPERTY,payload:newBundleData})
            }).catch(respose => {  

            })
        }else{
            unlikeProduct({accountId,productId:newBundleData.id}).then(async respose => {
                dispatch({type:PRODUCTITEM_SAVED_LIST_CHANGE_PROPERTY,payload:newBundleData})
            }).catch(respose => {  

            })
        }

    }
}

export const codeChanged = (code) => {
    return {
        type: CODE_CHANGED,
        payload: code
    }
}

export const verifyCodeEmail = (email,code,navigation,screen) => {
    return (dispatch) => {

        if(!code){
            dispatch({type: LOADING_FAILED_ERRMSG_CODE,payload:'Invalid code'})
            return
        }

        dispatch({type: SEND_VERIFY_CODE})
        verifyEmail({email,code}).then(async respose => {
            if(respose.data.data){
                navigation.navigate(screen)
                dispatch({type: SENT_VERIFY_CODE})
            }else{
                dispatch({type: LOADING_FAILED_ERRMSG_CODE,payload:'Invalid code'})
            }
        }).catch(respose => {        
            createSession();
            dispatch({type: LOADING_FAILED})
        })
    }
}


export const sendEmailAction = (email,navigation,screen) => {
    return (dispatch) => {
        if(!email){
            dispatch({type: LOADING_FAILED_ERRMSG_EMAIL,payload:'Invalid email'})
            return
        }
        dispatch({type: SEND_EMAIL})
        sendEmail({email}).then(async respose => {
            navigation.navigate(screen)
            dispatch({type: SENT_EMAIL})
        }).catch(respose => { 
            if(respose.response.status===417){
                dispatch({type: LOADING_FAILED_ERRMSG_EMAIL,payload: respose.response.data.message})
            }             
            createSession();
            dispatch({type: LOADING_FAILED})
        })
    }
}

export const forgotMe = ({email}) => {
    return (dispatch) => {
        if(!email){
            dispatch({type: LOADING_FAILED_ERRMSG,payload:'Invalid email'})
            return
        }
        dispatch({type: FORGOT_EMAIL_SEND})
        forgotPassword({email}).then(async respose => {
            dispatch({type: FORGOT_EMAIL_SENT})
        }).catch(respose => { 
            if(respose.response.status===417){
                dispatch({type: LOADING_FAILED_ERRMSG,payload: respose.response.data.message})
            }             
            createSession();
            dispatch({type: LOADING_FAILED})
        })
    }
}

export const profileChange = ({prop,value}) => {
    return {
        type: PROFILE_CHANGE,
        payload: {prop,value}
    }
}

export const loginFormChange = ({prop,value}) => {
    return {
        type: LOGIN_FORM_CHANGE,
        payload: {prop,value}
    }
}

export const genericChange = ({prop,value}) => {
    return {
        type: GENERIC_CHANGE,
        payload: {prop,value}
    }
}

export const closeLoginModal = () => {
    return {
        type: CLOSE_LOGIN_MODAL
    }
}

export const closeEventModal = () => {
    return {
        type: EVENT_MODAL_CLOSE
    }
}

export const resetIds = () => {
    return {
        type: RESET_ID_REDUCERS
    }
}

export const loginUser = ({username,password},navigation,screen) => {
    return (dispatch) => {
        //signinapi(email,password)
        //.then(ret=> dispatch({type:LOGIN_USER_SUCCESS,payload:user}))

        //redux thunk asyncronus call for api call
        if(!username){
            dispatch({type: LOADING_FAILED_ERRMSG,payload:'Invalid email'})
            return
        }
        if(!password){
            dispatch({type: LOADING_FAILED_ERRMSG,payload:'Please input password'})
            return
        }
        dispatch({type: SEND_LOGIN})
        login({username,password}).then(async respose => {
            dispatch({type:GENERIC_ID_REDUCER_CHANGE,payload:{prop:'accountId',value:respose.data.data}})
            await AsyncStorage.setItem('account_id',respose.data.data) 
            await AsyncStorage.setItem('username',username) 
            await AsyncStorage.setItem('password',password) 
            createSession();
            navigation.navigate(screen)
            dispatch({type: RESET_REGISTRATION})
        }).catch(respose => { 
            if(respose.response.status===417){
                dispatch({type: LOADING_FAILED_ERRMSG,payload: respose.response.data.message})
            }             
            createSession();
            dispatch({type: LOADING_FAILED})
        })
    }
}

export const findProduct = (request,serverData,accountId) => {
    return (dispatch) => {
        dispatch({type: GENERIC_LOADER,payload:{prop:'loadProductBundlePage',value:true}})
        findProductBundle(JSON.parse(request),accountId).then(async respose => {
            dispatch({type: PRODUCT_LOAD_MORE,payload:respose.data.data.products})
            if(respose.data.data.totalHits<=serverData.length+respose.data.data.products.length){
                dispatch({type: PRODUCT_DONT_LOAD_MORE})
            }
            dispatch({type: GENERIC_LOADER,payload:{prop:'loadProductBundlePage',value:false}})
        }).catch(respose => {  
            dispatch({type: GENERIC_LOADER,payload:{prop:'loadProductBundlePage',value:false}})
            createSession();
        })
    }
}

export const findProductInit = (request,serverData,accountId) => {
    return (dispatch) => {
        dispatch({type: GENERIC_LOADER,payload:{prop:'loadProductBundlePage',value:true}})
        findProductBundle(JSON.parse(request),accountId).then(async respose => {
            dispatch({type: INIT_PRODUCT_LOAD_MORE,payload:respose.data.data.products})
            if(respose.data.data.totalHits<=serverData.length+respose.data.data.products.length){
                dispatch({type: PRODUCT_DONT_LOAD_MORE})
            }
            dispatch({type: GENERIC_LOADER,payload:{prop:'loadProductBundlePage',value:false}})
        }).catch(respose => {  
            dispatch({type: GENERIC_LOADER,payload:{prop:'loadProductBundlePage',value:false}})
            createSession();
        })
    }
}

export const findSavedProduct = (accountId,offset,serverData) => {
    return (dispatch) => {
        
        dispatch({type: GENERIC_LOADER,payload:{prop:'likedPage',value:true}})

        findProductSaved(accountId,offset).then(async respose => {
            console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@-----------saveing succ--------------@@@@@@@@@@')
            dispatch({type: PRODUCT_LOAD_MORE_SAVED,payload:respose.data.data.products})

            if(respose.data.data.totalHits<=serverData.length+respose.data.data.products.length){
                dispatch({type: PRODUCT_DONT_LOAD_MORE_SAVED})
            }
            
            dispatch({type: GENERIC_LOADER,payload:{prop:'likedPage',value:false}})
        }).catch(respose => {  
            console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------------@@@@@@@@@@')
            console.log(respose)
            dispatch({type: GENERIC_LOADER,payload:{prop:'likedPage',value:false}})
            createSession();
        })
    }
}

export const findSavedProductInit = (accountId,offset,serverData) => {
    return (dispatch) => {
        
        dispatch({type: GENERIC_LOADER,payload:{prop:'likedPage',value:true}})

        findProductSaved(accountId,offset).then(async respose => {
            console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@-----------saveing succ--------------@@@@@@@@@@')
            dispatch({type: INIT_PRODUCT_LOAD_MORE_SAVED,payload:respose.data.data.products})

            if(respose.data.data.totalHits<=serverData.length+respose.data.data.products.length){
                dispatch({type: PRODUCT_DONT_LOAD_MORE_SAVED})
            }
            
            dispatch({type: GENERIC_LOADER,payload:{prop:'likedPage',value:false}})
        }).catch(respose => {  
            console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------------@@@@@@@@@@')
            console.log(respose)
            dispatch({type: GENERIC_LOADER,payload:{prop:'likedPage',value:false}})
            createSession();
        })
    }
}

export const initSavedProduct = () => {
    return {
        type: INITIAL_PRODUCT_BUNDLE_SAVED
    }
}

export const productSavedLoadMoreTrue = () => {
    return {
        type: PRODUCT_LOAD_MORE_TRUE_SAVED
    }
}

export const initfindProduct = () => {
    return {
        type: INITIAL_PRODUCT_BUNDLE
    }
}


export const productLoadMoreTrue = () => {
    return {
        type: PRODUCT_LOAD_MORE_TRUE
    }
}

export const selectProductBundle = (product,navigation,screen,fromScreen,accountId) => {
    let arrayIds = product.itemIds;
    var newVal = "";

    for(let i=0;i<arrayIds.length;i++){
        if(i===0){
            newVal = newVal+`{"key": "id","value": "${arrayIds[i]}"}`
        }else{
            newVal = newVal+`,{"key": "id","value": "${arrayIds[i]}"}`
        }   
    }

    const baseReq = `{"filterBy": {"matches": [${newVal}]},"pagination": {"from": 0,"size": 10},"sortBy": {"fieldsAscOrDesc": {"id.keyword": "asc"}}}`
    const otherProductReq = `{"filterBy": {"matches": [{ "key": "isActive","value": true}],"matchesNot": [{"key": "id","value": "${product.id}"}]},"pagination": {"from": 0,"size": 10},"sortBy": {"fieldsAscOrDesc": {"id.keyword": "asc"}}}`;
    return (dispatch) => {

        dispatch({type: PRODUCT_SELECTED_LOAD_BUNDLE,
            payload:{
                product:product
            }})

        findItemList(JSON.parse(baseReq)).then(async resp => {
            dispatch({type: PRODUCT_SELECTED_LOAD_BUNDLE_ITEMS,
                payload:{
                    itemDetails:resp.data.data
                }})
        }).catch(respose => {  
            navigation.push('mainFlow')
        })

        getOrgDetails(product.organization.id).then(async orgResp => {
            dispatch({type: PRODUCT_SELECTED_LOAD_BUNDLE_ORG,
                payload:{
                    org:orgResp.data.data 
                }})
        }).catch(err => {  
            navigation.push('mainFlow')
        })

        findProductBundle(JSON.parse(otherProductReq),accountId).then(async otherProduct => {
            dispatch({type: PRODUCT_SELECTED_LOAD_BUNDLE_OTHER_PRODUCTS,
                payload:{
                    otherProducts:otherProduct.data.data.products
                }})

        }).catch(errfindOther => {  
            navigation.push('mainFlow')
        })

        if(screen ==='exploreFlow'){
            navigation.navigate('ProductBundleDetail',{fromScreen:fromScreen})
        }else{
            navigation.push(screen)
        }
    }
}

export const bookingPropsChange = ({prop,value}) => {
    return {
        type: GENERIC_BOOKING_CHANGE,
        payload: {prop,value}
    }
}

export const chooseEventInBooking = (event) => {
    return {
        type: CHOSE_BOOKING_EVENT,
        payload: event
    }
}

export const eventPropsChange = ({prop,value}) => {
    return {
        type: GENERIC_EVENT_CHANGE,
        payload: {prop,value}
    }
}

export const loadProvince = (navigation,screen) => {
    return (dispatch) => {
        getProvince().then(async respose => {
            dispatch({type: LOAD_PROVINCE,payload:respose.data.data})
            navigation.navigate(screen)
        }).catch(respose => {  
            createSession();
        })
    }
}

export const loadEventDetail = (detail,navigation,screen,isCancelled) => {
    return (dispatch) => {
        if(isCancelled){
            dispatch({type: LOAD_EVENT_DETAIL_CANCEL,payload:detail})
        }else{
            dispatch({type: LOAD_EVENT_DETAIL,payload:detail})
        }
        navigation.navigate(screen)
    }
}

export const loadBookingDetail = (detail,navigation,screen) => {
    return (dispatch) => {
        dispatch({type: SELECT_BOOKING,payload:detail})
        navigation.navigate(screen)
    }
}

export const loadMunicipality = (code) => {
    return (dispatch) => {
        getMunicipality(code).then(async respose => {
            dispatch({type: LOAD_MUNICIPALITY,payload:respose.data.data})
            dispatch({type: LOAD_BARANGAY,payload:[]})
        }).catch(respose => {  
            createSession();
        })
    }
}

export const loadBarangay = (code) => {
    return (dispatch) => {
        getBarangay(code).then(async respose => {
            dispatch({type: LOAD_BARANGAY,payload:respose.data.data})
        }).catch(respose => {  
            createSession();
        })
    }
}

export const loadEventTypes = () => {
    return (dispatch) => {
        getEventTypes().then(async events => {
            dispatch({type: LOAD_EVENT_TYPE,payload:events.data.data})
        }).catch(respose => {  
            createSession();
        })
    }
}


export const genericSearchChange = ({prop,value}) => {
    return {
        type: GENERIC_SEARCH_CHANGE,
        payload: {prop,value}
    }
}

export const getCategoryList = () => {
    return (dispatch) => {
        getCategory().then(async category => {
            console.log(category.data.data)
            dispatch({type: GENERIC_SEARCH_CHANGE,payload: {prop:'categories',value:category.data.data}})
        }).catch(respose => {  
            createSession();
        })
    }
}

export const getSubCategoryListFromCategory = (category) => {
    return (dispatch) => {
        getSubCategoryListingFromCat(category).then(async subcat => {
            console.log(subcat.data.data)
            dispatch({type: GENERIC_SEARCH_CHANGE,payload: {prop:'subcategories',value:subcat.data.data}})
        }).catch(respose => {  
            createSession();
        })
    }
}

export const getSubCategoryListing = () => {
    return (dispatch) => {
        getSubCategoryListingApi().then(async subcat => {
            console.log(subcat.data.data)
            dispatch({type: GENERIC_SEARCH_CHANGE,payload: {prop:'subcategories',value:subcat.data.data}})
        }).catch(respose => {  
            createSession();
        })
    }
}



export const getMaxPrice = () => {
    return (dispatch) => {
        const request = `{"filterBy": {"matches": [{"key": "isActive","value": "true"}]},"pagination": {"from": 0,"size": 1},"sortBy": {"fieldsAscOrDesc": {"price": "desc"}},"withoutImage": true}`
        findProductBundleWithoutSavedUserMarked(request).then(async reply => {
            console.log('----')
            if(reply.data.data.productWithImage.length>0){
                dispatch({type: GENERIC_SEARCH_CHANGE,payload: {prop:'maxPriceRange',value:reply.data.data.productWithImage[0].price}})
                dispatch({type: GENERIC_SEARCH_CHANGE,payload: {prop:'high',value:reply.data.data.productWithImage[0].price}})
            }else{
                dispatch({type: GENERIC_SEARCH_CHANGE,payload: {prop:'maxPriceRange',value:1}})
            }
        }).catch(respose => {  
            createSession();
        })
    }
}


export const resetProfile = () => {
    return {
        type: RESET_PROFILE_CHANGE
    }
}

export const showUploadProfilePhotoErrorModal = (data) => {
    return {
        type: GENERIC_PROFILE_CHANGE,payload: {prop:'modalVisible',value:data}
    }
}

export const showUploadProfilePhotoErrorModalMessage = (data) => {
    return {
        type: GENERIC_PROFILE_CHANGE,payload: {prop:'modalErrorMessage',value:data}
    }
}

export const prefillMyProfile = (accountId,navigation,navigateMe) => {
    return (dispatch) => {
        prefillProfile(accountId).then(async reply => {
            dispatch({type: GENERIC_PROFILE_CHANGE,payload: {prop:'profileInfo',value:reply.data.data}})
        }).then(()=>{
            if(navigateMe){
                navigation.navigate('ProfileScreen')
            }
        }).catch(respose => {  
            createSession();
        })
    }
}

export const prefillMyProfileEdit = (accountId,navigation,navigateMe) => {
    return (dispatch) => {
        prefillProfile(accountId).then(async reply => {
            dispatch({type: GENERIC_PROFILE_CHANGE,payload: {prop:'profileEditInfo',value:reply.data.data}})
        }).then(()=>{
            if(navigateMe){
                navigation.navigate('EditProfileInfoScreen')
            }
        }).catch(respose => {  
            createSession();
        })
    }
}

export const editMyProfile = (request,accountId,navigation,navigateMe) => {
    return (dispatch) => {
        console.log('editing')
        editProfile(accountId,JSON.stringify(request)).then(async reply => {
            console.log('editing start')
            console.log(reply)
            console.log('editing end')
            prefillProfile(accountId).then(async reply => {
                dispatch({type: GENERIC_PROFILE_CHANGE,payload: {prop:'profileInfo',value:reply.data.data}})
            }).then(()=>{
                if(navigateMe){
                    navigation.navigate('ShowProfile')
                }
            }).catch(respose => {  
                console.log(respose)
                createSession();
            })
        }).catch(respose => {  
            console.log(respose)
            createSession();
        })
    }
}

export const profileEditContactChange = (profile,value) => {

    if(profile.contact){
        if(profile.contact.contactInformations){
            var contactInformations = profile.contact.contactInformations.map(u=>u.key==='mobilenumber'?{...u,value:value}:u)

            return {
                type: PROFILE_INFORMATION_EDIT_CHANGE_WITHOUTCONTACT,
                payload: {prop:'contact',value:{'contactInformations':contactInformations}}
            }
        }

    }else{
        return {
            type: PROFILE_INFORMATION_EDIT_CHANGE_WITHOUTCONTACT,
            payload: {prop:'contact',value:{'contactInformations':[{
                "key": "mobilenumber",
                "value": value
              }]}}
        }
    }
}

export const tryme = (param) => {
    return (dispatch) => {
        console.log(param)
    }
}


export const profileEditChange = ({prop,value}) => {
    return {
        type: GENERIC_PROFILE_EDIT_CHANGE,
        payload: {prop,value}
    }
}


