export const EMAIL_CHANGED = 'email_changed'
export const CODE_CHANGED = 'code_changed'
export const PROFILE_CHANGE = 'profile_change'
export const LOGIN_USER_SUCCESS = 'login_user_success'
export const LOGIN_USER_FAIL = 'login_user_fail'
export const SEND_EMAIL = 'send_email'
export const SENT_EMAIL = 'sent_email'
export const LOADING_FAILED = 'load_fail'
export const LOADING_FAILED_ERRMSG = 'load_fail_err_msg'
export const RESET_CONFIRM = 'reset_confirm'
export const CLOSE_LOGIN_MODAL = 'close_login_modal'

export const SEND_VERIFY_CODE = 'send_verify_code'
export const SENT_VERIFY_CODE = 'sent_verify_code'

export const SEND_REGISTRATION = 'send_registration'
export const SENT_REGISTRATION = 'sent_registration'

export const SEND_LOGIN = 'send_login'
export const SENT_LOGIN = 'send_login'
export const LOGIN_FORM_CHANGE = 'login_change'

export const RESET_REGISTRATION = 'reset_registration'

export const LOADING_FAILED_ERRMSG_EMAIL = 'load_fail_err_msg_email'
export const LOADING_FAILED_ERRMSG_CODE = 'load_fail_err_msg_code'
export const LOADING_FAILED_ERRMSG_CONFIRM = 'load_fail_err_msg_confirm'

export const FORGOT_EMAIL_SENT = 'forgot_email_sent'
export const FORGOT_EMAIL_SEND = 'forgot_email_send'

export const GENERIC_CHANGE = 'generic_change'

export const PRODUCT_LOAD_MORE='product_load_more'
export const PRODUCT_DONT_LOAD_MORE='product_dont_load_more'
export const PRODUCT_LOAD_MORE_TRUE='product_load_more_true'
export const SELECT_PRODUCT_BUNDLE='select_product_bundle'
export const INITIAL_PRODUCT_BUNDLE='init_product_bundle'

export const INIT_DETAILS_LIST='init_item_details'
export const LOADMORE_DETAILS_LIST='load_item_details'


export const GENERIC_EVENT_CHANGE ='generic_change_events'

export const LOAD_PROVINCE ='load_province'
export const LOAD_MUNICIPALITY ='load_municipality'
export const LOAD_BARANGAY ='load_barangay'

export const SAVE_EVENT ='save_event'
export const EVENT_MODAL_CLOSE='event_modal_close'
export const DISPLAY_ERR_MODAL_EVENT='display_err_modal_event'
export const REFRESH_EVENT='refresh_event'
export const RESET_EVENT='reset_event'
export const LOAD_EVENT_TYPE='load_event_type'
export const SET_EVENT_ACC_ID='set_event_acc_id'

export const LOAD_PLANNING_EVENT='load_planning_event'
export const LOAD_CANCELLED_EVENT='load_cancelled_event'
export const LOAD_HAPPENING_EVENT='load_happening_event'
export const LOAD_PAST_EVENT='load_past_event'
export const LOAD_EVENT_DETAIL='load_event_detail'
export const LOAD_EVENT_DETAIL_CANCEL='load_event_detail_cancel'
export const CANCEL_EVENT='cancel_event'
export const OPEN_CANCEL_EVENT_MODAL='cancel_event_modal'

export const CHECK_USER_HAS_EVENT='check_user_has_event'
export const RESET_CHECK_USER_HAS_EVENT='reset_check_user_has_event'

export const SET_BOOKING_DEFAULT='set_booking_default'
export const LOAD_EVENT_BOOKING_DROPDOWN='load_event_booking_dropdown'
export const GENERIC_BOOKING_CHANGE='generic_booking_change'
export const CHOSE_BOOKING_EVENT='chose_booking_event'

export const COMPUTE_BOOKING_AMOUNT='compute_booking_amount'
export const SET_BARGAIN_ITEMS='set_bargain_items_booking'
export const SET_BOOKING_WARNING_MODAL='set_booking_warning_modal'
export const SET_BOOKING_ITEMREMOVAL_MODAL='set_booking_itemremoval_modal'
export const SET_BOOKING_FINAL_MODAL='set_booking_final_modal'
export const RESET_BOOKING='reset_booking'
export const LOAD_PENDING_BOOKING='load_pending_booking'
export const LOAD_APPROVED_BOOKING='load_approved_booking'
export const LOAD_BOOKED_BOOKING='load_booked_booking'
export const LOAD_CANCELREJECT_BOOKING='load_cancelreject_booking'
export const SELECT_BOOKING='select_booking'
export const CANCEL_BOOKING_MODAL='cancel_booking_modal'
export const PAY_BOOKING_MODAL='pay_booking_modal'
export const GENERIC_BOOKING_DETAIL_CHANGE='generic_booking_detail_change'
export const SAVE_CARD_PAYMENT_DETAILS='save_card_payment_details'
export const CHANGE_CHECKOUT_FLAG='change_checkout_flag'
export const SET_PAID_INFO_BOOKING='set_paid_info_booking'

export const INITIAL_PRODUCT_BUNDLE_SAVED='INITIAL_PRODUCT_BUNDLE_SAVED'
export const PRODUCT_LOAD_MORE_TRUE_SAVED='PRODUCT_LOAD_MORE_TRUE_SAVED'
export const PRODUCT_LOAD_MORE_SAVED='PRODUCT_LOAD_MORE_SAVED'
export const PRODUCT_DONT_LOAD_MORE_SAVED='PRODUCT_DONT_LOAD_MORE_SAVED'

export const GENERIC_ID_REDUCER_CHANGE='GENERIC_ID_REDUCER_CHANGE'
export const RESET_ID_REDUCERS='RESET_ID_REDUCERS'

export const INIT_BOOKING_DETAIL_REDUCER='INIT_BOOKING_DETAIL_REDUCER'
export const INIT_EVENT_LIST_REDUCER='INIT_EVENT_LIST_REDUCER'
export const INIT_EVENT_REDUCER='INIT_EVENT_REDUCER'
export const PRODUCT_LIST_CHANGE_PROPERTY='PRODUCT_LIST_CHANGE_PROPERTY'

export const PRODUCT_SAVED_LIST_CHANGE_PROPERTY='PRODUCT_LIST_CHANGE_PROPERTY'
export const PRODUCTITEM_SAVED_LIST_CHANGE_PROPERTY='PRODUCTITEM_SAVED_LIST_CHANGE_PROPERTY'

export const PRODUCT_SELECTED_LOAD_BUNDLE='PRODUCT_SELECTED_LOAD_BUNDLE'
export const PRODUCT_SELECTED_LOAD_BUNDLE_ITEMS='PRODUCT_SELECTED_LOAD_BUNDLE_ITEMS'
export const PRODUCT_SELECTED_LOAD_BUNDLE_ORG='PRODUCT_SELECTED_LOAD_BUNDLE_ORG'
export const PRODUCT_SELECTED_LOAD_BUNDLE_OTHER_PRODUCTS='PRODUCT_SELECTED_LOAD_BUNDLE_OTHER_PRODUCTS'
export const INIT_PRODUCT_LOAD_MORE = 'INIT_PRODUCT_LOAD_MORE'
export const INIT_PRODUCT_LOAD_MORE_SAVED = 'INIT_PRODUCT_LOAD_MORE_SAVED'

export const GENERIC_LOADER='GENERIC_LOADER'
export const GENERIC_SEARCH_CHANGE='GENERIC_SEARCH_CHANGE'
export const RESET_SEARCH='RESET_SEARCH'




export const GENERIC_PROFILE_CHANGE='GENERIC_PROFILE_CHANGE'
export const RESET_PROFILE_CHANGE='RESET_PROFILE_CHANGE'
export const GENERIC_PROFILE_EDIT_CHANGE='GENERIC_PROFILE_EDIT_CHANGE'
export const PROFILE_INFORMATION_EDIT_CHANGE='PROFILE_INFORMATION_EDIT_CHANGE'
export const PROFILE_INFORMATION_EDIT_CHANGE_WITHOUTCONTACT='PROFILE_INFORMATION_EDIT_CHANGE_WITHOUTCONTACT'






