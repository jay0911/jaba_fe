import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import base64 from 'react-native-base64';
import {
    MAIN_SERVICE,
    AUTH_SERVICE} from '../api/backendserver'

export const createSession = async() => {
    try{
        const username = "Client"
        const password = "{noop}Client_Secret"
        const authHeader = 'Basic ' + base64.encode(`${username}:${password}`);

        const formData = new FormData();
        formData.append('grant_type', "password");
        const user = await AsyncStorage.getItem('username');
        const pass = await AsyncStorage.getItem('password');
        formData.append('username', user);
        formData.append('password', pass);

        axios({
            method: 'post',
            url: `${AUTH_SERVICE}/oauth/token`,
            data: formData,
            headers: { 'Authorization': authHeader,'Content-Type':'application/x-www-form-urlencoded' }
        }).then(async respose => {
            console.log(respose.data.access_token);  
            await AsyncStorage.setItem('token',respose.data.access_token)   
            await AsyncStorage.setItem('refresh_token',respose.data.refresh_token)   
        }).catch(respose => {
            console.log(respose);  
        })
    }catch(err){
        alert(err)
    }
}

export const refreshToken = async() => {
    try{
        const username = "Client"
        const password = "{noop}Client_Secret"
        const authHeader = 'Basic ' + base64.encode(`${username}:${password}`);

        const refresh_token = await AsyncStorage.getItem('refresh_token');

        const formData = new FormData();
        formData.append('grant_type', "refresh_token");
        formData.append('username', "marketplace");
        formData.append('password', "marketplace");
        formData.append('refresh_token', refresh_token);
        
        axios({
            method: 'post',
            url: `${AUTH_SERVICE}/oauth/token`,
            data: formData,
            headers: { 'Authorization': authHeader,'Content-Type':'application/x-www-form-urlencoded' }
        }).then(async respose => {
            console.log(respose.data.access_token);  
            await AsyncStorage.setItem('token',respose.data.access_token)   
        }).catch(respose => {
            console.log(respose);  
        })
    }catch(err){
        alert(err)
    }
}

export const sendEmail = async({email}) => {
    try{
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/account/mobile/resend/${email}`,
        })
    }catch(err){
        alert(err)
    }
}
 
export const verifyEmail = async({email,code}) => {
    try{
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/account/mobile/verify/${email}/${code}`,
        })
    }catch(err){
        alert(err)
    }
}

export const eventBookingDropDown = async() => {
    try{
        const token = await AsyncStorage.getItem('token')
        const id = await AsyncStorage.getItem('account_id');
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'get',
            url: `${MAIN_SERVICE}/v1/event/list-active/${id}`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}

export const saveBooking = async(request) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/booking/mobile/save`,
            headers: { 'Authorization': authHeader, 'Content-Type': 'application/json' },
            data: request
        })
    }catch(err){
        alert(err)
    }
}

export const register = async({email,firstname,lastname,password}) => {
    try{
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/account/mobile/register`,
            headers: { 'Content-Type': 'application/json' },
            data: {email,firstname,lastname,password}
        })
    }catch(err){
        alert(err)
    }
}

export const login = async({username,password}) => {
    try{
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/account/login`,
            headers: { 'Content-Type': 'application/json' },
            data: {username,password}
        })
    }catch(err){
        alert(err)
    }
}

export const updateBooking = async(bookingId,status) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/booking/update-status/${bookingId}?status=${status}`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}

export const payCheckout = async(request) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/payment-card/v1/payment-card`,
            headers: { 'Authorization': authHeader, 'Content-Type': 'application/json' },
            data: request
        })
    }catch(err){
        alert(err)
    }
}

export const getBookedPaymentDetails = async(request) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/booking/detail/paid/${request}`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}

export const verifyPayment = async(request) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/payment-card/verify-payment`,
            headers: { 'Authorization': authHeader, 'Content-Type': 'application/json' },
            data: request
        })
    }catch(err){
        alert(err)
    }
}

export const forgotPassword = async({email}) => {
    try{
        return axios({
            method: 'get',
            url: `${MAIN_SERVICE}/v1/account/forgot/${email}`,
        })
    }catch(err){
        alert(err)
    }
}

export const getCategory = async() => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'get',
            url: `${MAIN_SERVICE}/v1/category/list`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}

export const findProductBundle = async(request,accountId) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/product-bundle/find/sort-filter/mobile/${accountId}`,
            headers: { 'Authorization': authHeader, 'Content-Type': 'application/json' },
            data: request
        })
    }catch(err){
        alert(err)
    }
}

export const findProductBundleWithoutSavedUserMarked = async(request) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/product-bundle/find/sort-filter`,
            headers: { 'Authorization': authHeader, 'Content-Type': 'application/json' },
            data: request
        })
    }catch(err){
        alert(err)
    }
}

export const likeProduct = async(request) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/saved-product/add`,
            headers: { 'Authorization': authHeader, 'Content-Type': 'application/json' },
            data: request
        })
    }catch(err){
        alert(err)
    }
}

export const unlikeProduct = async(request) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/saved-product/remove`,
            headers: { 'Authorization': authHeader, 'Content-Type': 'application/json' },
            data: request
        })
    }catch(err){
        alert(err)
    }
}

export const findProductSaved = async(accountId,offset) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/saved-product/products/${accountId}/${offset}`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}

export const findItemList = async(request) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/item/find/sort-filter/mobile`,
            headers: { 'Authorization': authHeader, 'Content-Type': 'application/json' },
            data: request
        })
    }catch(err){
        alert(err)
    }
}

export const getOrgDetails = async(request) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'get',
            url: `${MAIN_SERVICE}/v1/organization/${request}`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}


export const getProvince = async() => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'get',
            url: `${MAIN_SERVICE}/v1/address/province`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}

export const getMunicipality = async(code) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'get',
            url: `${MAIN_SERVICE}/v1/address/municipality/${code}`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}

export const getBarangay = async(code) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'get',
            url: `${MAIN_SERVICE}/v1/address/barangay/${code}`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}

export const getEventTypes = async() => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'get',
            url: `${MAIN_SERVICE}/v1/event-type/list`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}

export const saveEvent = async(request) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/event/save`,
            headers: { 'Authorization': authHeader, 'Content-Type': 'application/json' },
            data: request
        })
    }catch(err){
        alert(err)
    }
}

export const addressProductSearch = async(request) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/address/search/product-bundle`,
            headers: { 'Authorization': authHeader, 'Content-Type': 'application/json' },
            data: request
        })
    }catch(err){
        alert(err)
    }
}

export const eventListing = async(accountId,status) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'get',
            url: `${MAIN_SERVICE}/v1/event/list/${accountId}?status=${status}`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}


export const bookingListing = async(accountId,status) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/booking/mobile/list/${accountId}?status=${status}`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}

export const checkUserHasEvent = async(accountId) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'get',
            url: `${MAIN_SERVICE}/v1/event/has-event/${accountId}`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}

export const updateEventStatus = async(request) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/event/update/status`,
            headers: { 'Authorization': authHeader, 'Content-Type': 'application/json' },
            data: request
        })
    }catch(err){
        alert(err)
    }
}

export const getAccountBookingCount = async(accountId,status) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/booking/count/${accountId}?status=${status}`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}

export const getAccountBookingCountAll = async(accountId) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/booking/count/all/${accountId}`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}

export const getSubCategoryListingFromCat = async(word) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'get',
            url: `${MAIN_SERVICE}/v1/itemtype/list/group?category=${word}`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}

export const getSubCategoryListingApi = async() => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'get',
            url: `${MAIN_SERVICE}/v1/itemtype/list`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}


export const prefillProfile = async(accountId) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'get',
            url: `${MAIN_SERVICE}/v1/profile/prefill/${accountId}`,
            headers: { 'Authorization': authHeader }
        })
    }catch(err){
        alert(err)
    }
}



export const editProfile = async(accountId,request) => {
    try{
        const token = await AsyncStorage.getItem('token')
        const authHeader = 'Bearer ' + token
        return axios({
            method: 'post',
            url: `${MAIN_SERVICE}/v1/profile/edit/${accountId}`,
            headers: { 'Authorization': authHeader, 'Content-Type': 'application/json' },
            data: request
        })
    }catch(err){
        alert(err)
    }
}



