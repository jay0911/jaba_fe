import {INIT_EVENT_REDUCER,SET_EVENT_ACC_ID,REFRESH_EVENT,LOAD_EVENT_TYPE,RESET_EVENT,SAVE_EVENT,EVENT_MODAL_CLOSE,DISPLAY_ERR_MODAL_EVENT,LOAD_BARANGAY,GENERIC_EVENT_CHANGE,LOAD_PROVINCE,LOAD_MUNICIPALITY} from '../actions/types'

const INITIAL_STATE = { eventName:'',otherAddress:'',errorMessage:'',loading:false,modalVisible:false,
eventTypeList:[],
eventType:'',
accountId:'',
addressList:{province:[],municipality:[],barangay:[]},province:'',municipality:'',barangay:'' }

export default (state=INITIAL_STATE, action) => {
    switch(action.type){
        case INIT_EVENT_REDUCER:
            return INITIAL_STATE
        case SET_EVENT_ACC_ID:
            return {...state,accountId:action.payload}
        case REFRESH_EVENT:
            return {...state,eventName:'',otherAddress:'',errorMessage:'',modalVisible:false,loading:false,
            eventType:'',
            addressList:{...state.addressList},province:'',municipality:'',barangay:'' }
        case RESET_EVENT:
            return {...state,errorMessage:'',modalVisible:false,
            addressList:{...state.addressList}}
        case GENERIC_EVENT_CHANGE:
            return {...state,[action.payload.prop]:action.payload.value}
        case LOAD_EVENT_TYPE:
            return {...state,addressList:{...state.addressList},eventTypeList:action.payload}
        case LOAD_PROVINCE:
            return {...state,addressList:{...state.addressList,province:action.payload}}
        case LOAD_MUNICIPALITY:
            return {...state,addressList:{...state.addressList,municipality:action.payload}}
        case LOAD_BARANGAY:
            return {...state,addressList:{...state.addressList,barangay:action.payload}}
        case EVENT_MODAL_CLOSE:
            return {...state,modalVisible:false}
        case DISPLAY_ERR_MODAL_EVENT:
            return {...state,addressList:{...state.addressList},errorMessage:action.payload,modalVisible:true}
        case SAVE_EVENT:
            return {...state,loading:false,errorMessage:''}
        default:
            return state
    }
}