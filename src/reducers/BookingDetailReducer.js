import {INIT_BOOKING_DETAIL_REDUCER,SET_PAID_INFO_BOOKING,CHANGE_CHECKOUT_FLAG,SAVE_CARD_PAYMENT_DETAILS,GENERIC_BOOKING_DETAIL_CHANGE,PAY_BOOKING_MODAL,CANCEL_BOOKING_MODAL,SELECT_BOOKING,LOAD_CANCELREJECT_BOOKING,LOAD_BOOKED_BOOKING,LOAD_APPROVED_BOOKING,LOAD_PENDING_BOOKING} from '../actions/types'

const INITIAL_STATE = { loading:false,paidDetails:null, checkoutFlag:false,verificationInfo:null,checkoutId:'',bookingPending:[],bookingReject:[],bookingBooked:[],bookingApproved:[],detail:'',loadCancelModal:false,cancelModalMessage:'',loadPayModal:false,payModalMessage:'' }

export default (state=INITIAL_STATE, action) => {
    switch(action.type){
        case INIT_BOOKING_DETAIL_REDUCER:
            return INITIAL_STATE
        case LOAD_PENDING_BOOKING:
            return {...state,bookingPending:action.payload}
        case LOAD_APPROVED_BOOKING:
            return {...state,bookingApproved:action.payload}
        case LOAD_BOOKED_BOOKING:
            return {...state,bookingBooked:action.payload}
        case GENERIC_BOOKING_DETAIL_CHANGE:
            return {...state,[action.payload.prop]:action.payload.value}
        case LOAD_CANCELREJECT_BOOKING:
            return {...state,bookingReject:action.payload}
        case SELECT_BOOKING:
            return {...state,detail:action.payload}
        case CANCEL_BOOKING_MODAL:
            return {...state,loadCancelModal:action.payload.showModal,cancelModalMessage:action.payload.errMsg}
        case PAY_BOOKING_MODAL:
            return {...state,loadPayModal:action.payload.showModal,payModalMessage:action.payload.errMsg}
        case SAVE_CARD_PAYMENT_DETAILS:
            return {...state,verificationInfo:action.payload}
        case CHANGE_CHECKOUT_FLAG:
            return {...state,checkoutFlag:action.payload}
        case SET_PAID_INFO_BOOKING:
            return {...state,paidDetails:action.payload}
        default:
            return state
    }
}
