import {LOADING_FAILED_ERRMSG_CONFIRM,LOADING_FAILED_ERRMSG_EMAIL,LOADING_FAILED_ERRMSG_CODE,RESET_REGISTRATION,SENT_REGISTRATION,SEND_REGISTRATION,RESET_CONFIRM,LOADING_FAILED_ERRMSG,SENT_VERIFY_CODE,SEND_VERIFY_CODE,CODE_CHANGED,LOADING_FAILED,SENT_EMAIL,SEND_EMAIL,EMAIL_CHANGED,LOGIN_USER_SUCCESS,LOGIN_USER_FAIL,PROFILE_CHANGE} from '../actions/types'

const INITIAL_STATE = { errorMessageEmail:'',errorMessageConfirm:'',errorMessageCode:'',loading:false,code:'',email:'',user:{firstname:'',lastname:'',password:''} }

export default (state=INITIAL_STATE, action) => {
    switch(action.type){
        case EMAIL_CHANGED:
            return {...state,email:action.payload}
        case CODE_CHANGED:
            return {...state,code:action.payload}
        case LOGIN_USER_SUCCESS:
            return state
        case LOGIN_USER_FAIL:
            return state
        case SEND_EMAIL:
            return {...state,loading:true,errorMessageEmail:''}
        case SENT_EMAIL:
            return {...state,loading:false,errorMessageEmail:''}
        case SEND_VERIFY_CODE:
            return {...state,loading:true,errorMessageCode:''}
        case SENT_VERIFY_CODE:
            return {...state,loading:false,errorMessageCode:''}
        case SEND_REGISTRATION:
            return {...state,loading:true,errorMessageConfirm:''}
        case SENT_REGISTRATION:
            return {...state,loading:false,errorMessageConfirm:''}
        case LOADING_FAILED:
            return {...state,loading:false}
        case LOADING_FAILED_ERRMSG_EMAIL:
            return {...state,loading:false,errorMessageEmail:action.payload}
        case LOADING_FAILED_ERRMSG_CODE:
            return {...state,loading:false,errorMessageCode:action.payload}
        case LOADING_FAILED_ERRMSG_CONFIRM:
            return {...state,loading:false,errorMessageConfirm:action.payload}
        case RESET_CONFIRM:
            return {...state,loading:false,errorMessageCode:'',errorMessageConfirm:'',errorMessageEmail:'',code:''}
        case RESET_REGISTRATION:
            return INITIAL_STATE
        case PROFILE_CHANGE:
            return {...state, user:{
                                ...state.user,[action.payload.prop]:action.payload.value
                                }
                    }
        default:
            return state
    }
}