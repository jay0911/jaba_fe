import {GENERIC_LOADER} from '../actions/types'

const INITIAL_STATE = { loadProductBundlePage:false,likedPage:false,eventPage:false ,bookingPage:false}

export default (state=INITIAL_STATE, action) => {
    switch(action.type){
        case GENERIC_LOADER:
            return {...state,[action.payload.prop]:action.payload.value}
        default:
            return state
    }
}
