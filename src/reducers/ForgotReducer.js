import {RESET_REGISTRATION,FORGOT_EMAIL_SEND,FORGOT_EMAIL_SENT,GENERIC_CHANGE,LOADING_FAILED,LOADING_FAILED_ERRMSG} from '../actions/types'

const INITIAL_STATE = { errorMessage:'',loading:false, email:'' }

export default (state=INITIAL_STATE, action) => {
    switch(action.type){
        case FORGOT_EMAIL_SEND:
            return {...state,loading:true,errorMessage:''}
        case FORGOT_EMAIL_SENT:
            return {...state,loading:false,errorMessage:''}
        case LOADING_FAILED:
            return {...state,loading:false}
        case LOADING_FAILED_ERRMSG:
            return {...state,loading:false,errorMessage:action.payload}
        case GENERIC_CHANGE:
            return {...state,[action.payload.prop]:action.payload.value}
        case RESET_REGISTRATION:
            return INITIAL_STATE
        default:
            return state
    }
}
