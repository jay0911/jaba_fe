import {GENERIC_ID_REDUCER_CHANGE,RESET_ID_REDUCERS} from '../actions/types'

const INITIAL_STATE = {accountId:''}

export default (state=INITIAL_STATE, action) => {
    switch(action.type){
        case RESET_ID_REDUCERS:
            return INITIAL_STATE
        case GENERIC_ID_REDUCER_CHANGE:
            return {...state,[action.payload.prop]:action.payload.value}
        default:
            return state
    }
}


