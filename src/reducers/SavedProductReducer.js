import {INIT_PRODUCT_LOAD_MORE_SAVED,PRODUCT_SAVED_LIST_CHANGE_PROPERTY,INITIAL_PRODUCT_BUNDLE_SAVED,PRODUCT_LOAD_MORE_TRUE_SAVED,PRODUCT_LOAD_MORE_SAVED,PRODUCT_DONT_LOAD_MORE_SAVED} from '../actions/types'

const INITIAL_STATE = { serverData: [], loading: false, offset:0,showLoadmore:false }

export default (state=INITIAL_STATE, action) => {
    switch(action.type){
        case INITIAL_PRODUCT_BUNDLE_SAVED:
            return { serverData: [], loading: false, offset:0,showLoadmore:false }
        case PRODUCT_LOAD_MORE_SAVED:
            return {...state,
            serverData: [...state.serverData, ...action.payload],
            offset: state.offset+10,
            showLoadmore:true}
        case INIT_PRODUCT_LOAD_MORE_SAVED:
            return {...state,
                serverData: [...action.payload],
                offset: state.offset+10,
                showLoadmore:true}
        case PRODUCT_DONT_LOAD_MORE_SAVED:
            return {...state,
                showLoadmore:false}
        case PRODUCT_LOAD_MORE_TRUE_SAVED:
            return {...state,loading:true}
        case PRODUCT_SAVED_LIST_CHANGE_PROPERTY:
            return {...state,
                serverData: action.payload}
        default:
            return state
    }
}
