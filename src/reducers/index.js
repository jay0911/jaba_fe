import {combineReducers} from 'redux'
import LibraryReducer from './LibraryReducer'
import SelectionReducer from './SelectionReducer'
import TestReducer from './TestReducer'
import TestReducer2 from './TestReducer2'
import AuthReducer from './AuthReducer'
import LoginReducer from './LoginReducer'
import ForgotReducer from './ForgotReducer'
import ProductDetailReducer from './ProductDetailReducer'
import ProductDetailPageReducer from './ProductDetailPageReducer'
import EventReducer from './EventReducer'
import EventListReducer from './EventListReducer'
import BookingReducer from './BookingReducer'
import BookingDetailReducer from './BookingDetailReducer'
import SavedProductReducer from './SavedProductReducer'
import IdReducer from './IdReducer'
import MainTabsReducer from './MainTabsReducer'
import SearchReducer from './SearchReducer'
import ProfileReducer from './ProfileReducer'

export default combineReducers({
    libraries:LibraryReducer,
    selectedLibraryId:SelectionReducer,
    test:TestReducer,
    test2:TestReducer2,
    auth:AuthReducer,
    login:LoginReducer,
    forgot:ForgotReducer,
    product:ProductDetailReducer,
    productDetailItem:ProductDetailPageReducer,
    events:EventReducer,
    eventList:EventListReducer,
    booking:BookingReducer,
    bookingdetails:BookingDetailReducer,
    savedProduct:SavedProductReducer,
    idReducer:IdReducer,
    mainTabReducer:MainTabsReducer,
    searchReducer:SearchReducer,
    profileReducer:ProfileReducer
})