import {INIT_PRODUCT_LOAD_MORE,PRODUCT_LIST_CHANGE_PROPERTY,INITIAL_PRODUCT_BUNDLE,PRODUCT_LOAD_MORE_TRUE,PRODUCT_LOAD_MORE,PRODUCT_DONT_LOAD_MORE} from '../actions/types'

const INITIAL_STATE = { serverData: [], loading: false, offset:0,showLoadmore:false }

export default (state=INITIAL_STATE, action) => {
    switch(action.type){
        case INITIAL_PRODUCT_BUNDLE:
            return { serverData: [], loading: false, offset:0,showLoadmore:false }
        case PRODUCT_LOAD_MORE:
            return {...state,
            serverData: [...state.serverData, ...action.payload],
            offset: state.offset+10,
            showLoadmore:true}
        case INIT_PRODUCT_LOAD_MORE:
            return {...state,
            serverData: [...action.payload],
            offset: state.offset+10,
            showLoadmore:true}
        case PRODUCT_DONT_LOAD_MORE:
            return {...state,
                showLoadmore:false}
        case PRODUCT_LOAD_MORE_TRUE:
            return {...state,loading:true}
        case PRODUCT_LIST_CHANGE_PROPERTY:
            return {...state,
                serverData: action.payload}
        default:
            return state
    }
}
