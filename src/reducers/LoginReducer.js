import {CLOSE_LOGIN_MODAL,RESET_REGISTRATION,LOGIN_FORM_CHANGE,SEND_LOGIN,SENT_LOGIN,LOADING_FAILED,LOADING_FAILED_ERRMSG} from '../actions/types'

const INITIAL_STATE = { errorMessage:'',loading:false, password:'',username:'',modalVisible:false }

export default (state=INITIAL_STATE, action) => {
    switch(action.type){
        case SEND_LOGIN:
            return {...state,loading:true,errorMessage:''}
        case SENT_LOGIN:
            return {...state,loading:false,errorMessage:''}
        case LOADING_FAILED:
            return {...state,loading:false}
        case CLOSE_LOGIN_MODAL:
            return {...state,modalVisible:false}
        case LOADING_FAILED_ERRMSG:
            return {...state,loading:false,errorMessage:action.payload,modalVisible:true}
        case LOGIN_FORM_CHANGE:
            return {...state,[action.payload.prop]:action.payload.value}
        case RESET_REGISTRATION:
            return INITIAL_STATE
        default:
            return state
    }
}
