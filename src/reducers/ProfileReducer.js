import {GENERIC_PROFILE_CHANGE,RESET_PROFILE_CHANGE,GENERIC_PROFILE_EDIT_CHANGE,PROFILE_INFORMATION_EDIT_CHANGE,PROFILE_INFORMATION_EDIT_CHANGE_WITHOUTCONTACT} from '../actions/types'

const INITIAL_STATE = {profileInfo:null,modalVisible:false,modalErrorMessage:'',profileEditInfo:null}

export default (state=INITIAL_STATE, action) => {
    switch(action.type){
        case RESET_PROFILE_CHANGE:
            return INITIAL_STATE
        case GENERIC_PROFILE_CHANGE:
            return {...state,[action.payload.prop]:action.payload.value}
        case GENERIC_PROFILE_EDIT_CHANGE:
            return {...state, profileEditInfo:{
                                ...state.profileEditInfo,[action.payload.prop]:action.payload.value
                                }
                    }
        case PROFILE_INFORMATION_EDIT_CHANGE:
            return {...state, profileEditInfo:{
                            ...state.profileEditInfo,contact:{
                                        ...state.profileEditInfo.contact,contactInformations:{
                                            ...state.profileEditInfo.contact.contactInformations,[action.payload.prop]:action.payload.value
                                        }
                                    }
                            }
                   }
         case PROFILE_INFORMATION_EDIT_CHANGE_WITHOUTCONTACT:
            return {...state, profileEditInfo:{
                            ...state.profileEditInfo,[action.payload.prop]:action.payload.value
                            }
                    }
        default:
            return state
    }
}
