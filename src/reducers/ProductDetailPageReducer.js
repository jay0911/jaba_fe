import {PRODUCT_SELECTED_LOAD_BUNDLE,PRODUCT_SELECTED_LOAD_BUNDLE_ITEMS,PRODUCT_SELECTED_LOAD_BUNDLE_ORG,PRODUCT_SELECTED_LOAD_BUNDLE_OTHER_PRODUCTS,PRODUCTITEM_SAVED_LIST_CHANGE_PROPERTY,RESET_CHECK_USER_HAS_EVENT,CHECK_USER_HAS_EVENT,SELECT_PRODUCT_BUNDLE,INIT_DETAILS_LIST} from '../actions/types'

const INITIAL_STATE = {showModal:false,errMsg:'', bundle:null,itemDetails:null,org:null,otherProducts:null,loading: false, offset:0,showLoadmore:false}

export default (state=INITIAL_STATE, action) => {
    switch(action.type){
        case SELECT_PRODUCT_BUNDLE:
            return {...state,bundle:action.payload.product,itemDetails:action.payload.itemDetails,org:action.payload.org,otherProducts:action.payload.otherProducts}
        case PRODUCT_SELECTED_LOAD_BUNDLE:
            return {...state,bundle:action.payload.product}
        case PRODUCT_SELECTED_LOAD_BUNDLE_ITEMS:
            return {...state,itemDetails:action.payload.itemDetails}
        case PRODUCT_SELECTED_LOAD_BUNDLE_ORG:
            return {...state,org:action.payload.org}
        case PRODUCT_SELECTED_LOAD_BUNDLE_OTHER_PRODUCTS:
            return {...state,otherProducts:action.payload.otherProducts}
        case INIT_DETAILS_LIST:
            return {...state,itemDetails:action.payload}
        case CHECK_USER_HAS_EVENT:
            return {...state,showModal:true,errMsg:action.payload}
        case RESET_CHECK_USER_HAS_EVENT:
            return {...state,showModal:false,errMsg:''}
        case PRODUCTITEM_SAVED_LIST_CHANGE_PROPERTY:
            return {...state,bundle:action.payload}
        default:
            return state
    }
}


