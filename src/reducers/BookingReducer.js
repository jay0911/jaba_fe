import {SET_BOOKING_FINAL_MODAL,RESET_BOOKING,SET_BOOKING_ITEMREMOVAL_MODAL,SET_BOOKING_WARNING_MODAL,SET_BARGAIN_ITEMS,COMPUTE_BOOKING_AMOUNT,CHOSE_BOOKING_EVENT,GENERIC_BOOKING_CHANGE,SET_BOOKING_DEFAULT,LOAD_EVENT_BOOKING_DROPDOWN} from '../actions/types'

const INITIAL_STATE = { bookingModal:false,bookingModalErrMsg:'',warningModal:false,warningModalErrMsg:'',showModalRemovalItem:false,removalItemErrMsg:'',computedAmount:0,bargainItems:null,bargainAmount:'0',errorMessage:'',loading:false, bundle:null,itemDetails:null,eventList:[],eventChosen:'',eventObjectChosen:null }

export default (state=INITIAL_STATE, action) => {
    switch(action.type){
        case SET_BOOKING_DEFAULT:
            return {...state,bundle:action.payload.bundle,itemDetails:action.payload.itemDetails}
        case LOAD_EVENT_BOOKING_DROPDOWN:
            return {...state,eventList:action.payload}
        case GENERIC_BOOKING_CHANGE:
            return {...state,[action.payload.prop]:action.payload.value}
        case CHOSE_BOOKING_EVENT:
            return {...state,eventObjectChosen:action.payload}
        case COMPUTE_BOOKING_AMOUNT:
            return {...state,computedAmount:action.payload}
        case SET_BARGAIN_ITEMS:
            return {...state,bargainItems:action.payload}
        case SET_BOOKING_WARNING_MODAL:
            return {...state,warningModal:action.payload.showModal,warningModalErrMsg:action.payload.errMsg}
        case SET_BOOKING_ITEMREMOVAL_MODAL:
            return {...state,showModalRemovalItem:action.payload.showModal,removalItemErrMsg:action.payload.errMsg}
        case SET_BOOKING_FINAL_MODAL:
            return {...state,bookingModal:action.payload.showModal,bookingModalErrMsg:action.payload.errMsg}
        case RESET_BOOKING:
            return INITIAL_STATE
        default:
            return state
    }
}
