import {INIT_EVENT_LIST_REDUCER,LOAD_EVENT_DETAIL_CANCEL,CANCEL_EVENT,OPEN_CANCEL_EVENT_MODAL,LOAD_CANCELLED_EVENT,LOAD_HAPPENING_EVENT,LOAD_PAST_EVENT,LOAD_PLANNING_EVENT,LOAD_EVENT_DETAIL} from '../actions/types'

const INITIAL_STATE = {isCancelled:false, planning:[], cancelled:[], past:[], happening:[],eventDetail:null,modalVisible:false }

export default (state=INITIAL_STATE, action) => {
    switch(action.type){
        case INIT_EVENT_LIST_REDUCER:
            return INITIAL_STATE
        case OPEN_CANCEL_EVENT_MODAL:
            return {...state,modalVisible:true}
        case CANCEL_EVENT:
            return {...state,modalVisible:false}
        case LOAD_EVENT_DETAIL:
            return {...state,eventDetail:action.payload,isCancelled:false}
        case LOAD_EVENT_DETAIL_CANCEL:
            return {...state,eventDetail:action.payload,isCancelled:true}
        case LOAD_PLANNING_EVENT:
            return {...state,planning:action.payload}
        case LOAD_CANCELLED_EVENT:
            return {...state,cancelled:action.payload}
        case LOAD_HAPPENING_EVENT:
            return {...state,happening:action.payload}
        case LOAD_PAST_EVENT:
            return {...state,past:action.payload}
        default:
            return state
    }
}