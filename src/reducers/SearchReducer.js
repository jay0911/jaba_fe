import {GENERIC_SEARCH_CHANGE,RESET_SEARCH} from '../actions/types'

const INITIAL_STATE = { productMatches:'',productRanges:'', searchLocation:'',searchAddressProductList:[],selectedLoc:'', selectedCategory:{code:'',name:''},selectedSubCategory:{code:'',name:''},categories:[],subcategories:[],high:0,low:0,maxPriceRange:1}

export default (state=INITIAL_STATE, action) => {
    switch(action.type){
        case GENERIC_SEARCH_CHANGE:
            return {...state,[action.payload.prop]:action.payload.value}
        case RESET_SEARCH:
            return INITIAL_STATE
        default:
            return state
    }
}
