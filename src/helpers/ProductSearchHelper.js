

export function loadProduct (callback) {
    const request = `{"filterBy": {"matches": [{ "key": "isActive","value": true}]},"pagination": {"from": ${arguments[2]},"size": 10},"sortBy": {"fieldsAscOrDesc": {"dtimeCreated": "desc"}}}`;
    callback (request, arguments[1],arguments[3]);
}

export function loadProductv2(callback) {
    callback (arguments[1], arguments[2],arguments[3]);
}