import React, { Component } from 'react'
import {View,Text} from 'react-native'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import IconBadge from 'react-native-icon-badge';

import {PENDING,CANCELLED,APPROVED,BOOKED} from '../../constants/BookingStatusConstants'


class BookingCount extends Component{

    renderTextElement(){
      switch(this.props.routeTitle){
        case PENDING.displayName:
            return <Text style={{color:'#FFFFFF'}}>{this.props.pendingCount.length}</Text>
        case CANCELLED.displayName:
            return <Text style={{color:'#FFFFFF'}}>{this.props.rejectCount.length}</Text>
        case APPROVED.displayName:
            return <Text style={{color:'#FFFFFF'}}>{this.props.approveCount.length}</Text>
        case BOOKED.displayName:
            return <Text style={{color:'#FFFFFF'}}>{this.props.bookedCount.length}</Text>
      }
    }

    isBadgeShowable(){
      switch(this.props.routeTitle){
        case PENDING.displayName:
            return this.props.pendingCount.length
        case CANCELLED.displayName:
            return this.props.rejectCount.length
        case APPROVED.displayName:
            return this.props.approveCount.length
        case BOOKED.displayName:
            return this.props.bookedCount.length
      }
    }

    render(){
        return (
            <View style={{flexDirection: 'row',alignItems: 'center',justifyContent: 'center',}}>
            <IconBadge
              MainElement={
                <Text style={{ color:'black', marginVertical:5,marginHorizontal:10 }}>
                  {this.props.routeTitle+'   '}
                </Text>
              }
              BadgeElement={
                <Text style={{color:'#FFFFFF'}}>{this.renderTextElement()}</Text>
              }
              IconBadgeStyle={
                {width:10,
                height:20,
                marginBottom:20,
                backgroundColor: '#ff0000'}
              }
              Hidden={this.isBadgeShowable()==0}
              />
          </View>
        )
    }
}

const styles = {
    header:{
        paddingLeft:25,
        paddingRight:25,
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
        backgroundColor:'#fafafa',
    }
}

const mapStateToProps = state => {
    console.log(state.bookingdetails.detail)
    return {
        pendingCount:state.bookingdetails.bookingPending,
        rejectCount:state.bookingdetails.bookingReject,
        bookedCount:state.bookingdetails.bookingBooked,
        approveCount:state.bookingdetails.bookingApproved,
    }
}
export default connect(mapStateToProps, actions)(BookingCount);

