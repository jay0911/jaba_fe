import React from 'react'
import { View, StyleSheet, Dimensions,Text } from 'react-native'
import Card from '../../commons/Card'
import CardSection from '../../commons/CardSection'

const { width, height } = Dimensions.get('window')

const BookingCard = ({ displayableFrom,displayableTo,eventName,province,municipality,productName }) => {
    return (
        <View>
            <Card>
                <CardSection>
                    <View style={{flexDirection:'column'}}>
                        <View>
                        <Text style={{fontSize:25,fontWeight:'bold',marginHorizontal:15,marginVertical:5}}>{productName}</Text>
                        </View>
                        <View>
                            <Text style={{marginHorizontal:15}}>{eventName}</Text>
                        </View>
                        <View style={{flexDirection:'row',marginHorizontal:15,marginVertical:5}}>
                            <Text>{province}, </Text>
                            <Text>{municipality}</Text>
                        </View>
                    </View>
                 </CardSection>
                <CardSection>
                    <View style={{flex:1,flexDirection:'row',alignItems:'stretch'}}>
                                        <View style={{paddingHorizontal:5,borderRightWidth:1, flex:1}}> 
                                            <Text>Start</Text>
                                            <Text style={{fontWeight:'bold',fontSize:12}}>{displayableFrom}</Text>
                                        </View>
                                        <View style={{paddingHorizontal:5,borderLeftWidth:1, flex:1}}>
                                            <Text>End</Text>
                                            <Text style={{fontWeight:'bold',fontSize:12}}>{displayableTo}</Text>
                                        </View>
                    </View>
                </CardSection>
            </Card>
        </View>
    )
}

const styles = StyleSheet.create({

})

export default BookingCard