import React, { Component } from 'react'
import {View,TouchableOpacity,Text} from 'react-native'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Button from '../../commons/Button'
import Icon from 'react-native-vector-icons/Ionicons'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import ModalCustom from './../../commons/ModalCustom'
import ButtonTransparent from './../../commons/ButtonTransparent'
import Card from './../../commons/Card'
import CardSection from './../../commons/CardSection'
import CreditCard from 'react-native-credit-card';
import Loader from '../../commons/Loader'

class BookingDetail extends Component{

    state={
        totalAdditionalFee:0
    }

    async componentDidMount(){     
        var totalFee = this.props.detail.otherFees.reduce((prev, current) => {
            return prev + + current.value
        },0)
        this.setState({...this.state,totalAdditionalFee:totalFee})

        if(this.props.detail.bookingStatus==='BOOKED'){
            console.log(this.props.paidDetails)
            this.props.getPaidDetailsBooking(this.props.detail.bookingId)
        }
    }

    renderAmount(){
        if(this.props.detail.bookingStatus === 'APPROVED'){
            return(
                <View>
                    <Text style={{paddingHorizontal:10,fontSize:30,fontWeight:'bold'}}>Php {this.props.detail.approvedAmount}</Text>
                    <Text style={{paddingHorizontal:10,paddingBottom:10,fontSize:20,fontWeight:'bold'}}>Approved Amount</Text>
                </View>
            )
        }

        return(
            <View>
                <Text style={{paddingHorizontal:10,fontSize:30,fontWeight:'bold'}}>Php {this.props.detail.originalPrice}</Text>
                <Text style={{paddingHorizontal:10,paddingBottom:10,fontSize:20,fontWeight:'bold'}}>Total Amount</Text>
            </View>
        )
    }

    renderApproved(){
        if(this.props.detail.bookingStatus==='PENDING'){
            return <Text>Pending</Text>
        }
        if(this.props.detail.bookingStatus==='APPROVED'){
            return <Text>Approved</Text>
        }
        if(this.props.detail.bookingStatus==='BOOKED'){
            return <Text>Booked</Text>
        }
        if(this.props.detail.bookingStatus==='CANCEL_REJECT'){
            return null
        }
    }

    renderAdditionalFee(){
        return this.props.detail.otherFees.length>0?(
            
            <View>
                <View style={{flexDirection:'row',justifyContent:'space-between',paddingHorizontal:10}}>
                    <Text style={{paddingBottom:10}}>Additional Fee</Text>
                </View>
                <Card>
                            <CardSection>
                                <View style={{width:'100%'}}>
                                    <View style={{padding:5}}>
                                        {this.renderOtherfeeItems()}
                                    </View>
                                    <View style={{alignItems:'flex-end'}}>
                                        <View>
                                            <Text style={{paddingHorizontal:10,fontSize:30,fontWeight:'bold'}}>Php {this.state.totalAdditionalFee}</Text>
                                            <Text style={{paddingHorizontal:10,paddingBottom:10,fontSize:20,fontWeight:'bold'}}>Total Fee</Text>
                                        </View>
                                    </View> 
                                </View> 
                            </CardSection>
                 </Card> 
            </View>
        ):null
    }

    renderOtherfeeItems(){
        return this.props.detail.otherFees.map((item, index) => {   
            return (<View key={index} style={{flexDirection:'row',justifyContent:'space-between',width:'100%' }}>
                <View style={{flexDirection:'row'}}>
                    <Text style={{fontSize:22,paddingLeft:5}}>{item.feeName}</Text>
                </View>
                <View style={{flexDirection:'row',paddingBottom:10}}>
                    <Text style={{fontSize:22,paddingRight:5}}>{item.value}</Text>
                </View>
            </View>)
        })
    }

    renderItems(){
        if(this.props.detail.items==null){
            return null;
        }
        if (this.props.detail.items && this.props.detail.items) {
            return this.props.detail.items.map((item, index) => {
                return (
                    <View key={index} style={{flexDirection:'row',justifyContent:'space-between',width:'100%',paddingHorizontal:30}}>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{fontSize:22,paddingLeft:5}}>{item.name}</Text>
                        </View>
                        <View style={{flexDirection:'row',paddingBottom:10}}>
                            <Text style={{fontSize:22,paddingRight:5}}>Php {item.originalPrice}</Text>
                        </View>
                    </View>
                ) 
            })
        }
        return null
    }

    renderDisclaimer(){
        if(this.props.detail.bookingStatus==='PENDING'){
            return <Text>Please be adviced that price may vary on additional fees and bargain amount upon approval</Text>
        }
    }

    renderPayment(){
        if(this.props.detail.bookingStatus==='APPROVED'){
            return <View style={{padding:20}}>
            <Button onPress={()=>this.payMeModal()}>
                Pay
            </Button>
            <View style={{justifyContent:'center'}}>
                <Text style={{textAlign:'center'}}>Booking will be cancelled automatically if downpayment is not paid within 5 days</Text>
            </View>
        </View>
        }
    }

    renderPaidText(){
        if(this.props.detail.bookingStatus==='BOOKED'){
            return (<View style={{paddingHorizontal:20}}>
                <View style={{justifyContent:'center'}}>
                    <Text>Paid via</Text>
                </View>
            </View>)
        }
    }
    renderPaid(){
        if(this.props.detail.bookingStatus==='BOOKED'){
            if(this.props.paidDetails != null){

                //for card payments
                if(this.props.paidDetails.cardDetail != null){
                    return (<View style={{padding:20}}>
                    <View style={{justifyContent:'center'}}>
                    <CreditCard
                        shiny={false}
                        bar={false}
                        number={this.props.paidDetails.cardDetail.bin}
                        name={this.props.paidDetails.cardDetail.accountHolder}
                        expiry={this.props.paidDetails.cardDetail.expiryMonth +"/"+ this.props.paidDetails.cardDetail.expiryYear }
                    />
                    </View>
                </View>)
                }
            }

            return null
        }
        return null
    }

    renderCancelButton(){
        if(this.props.detail.bookingStatus==='PENDING' || this.props.detail.bookingStatus==='APPROVED'){
            return (<TouchableOpacity onPress={()=>this.cancelModal()}>
                <Text style={{fontSize:20,textDecorationLine: 'underline'}}>Cancel Booking</Text>
            </TouchableOpacity>)
        }
    }

    cancelModal(){
        this.props.setCancelBookingModal(true,'Are you sure you want to cancel this booking?');
    }

    payMeModal(){
        this.props.payModal(true,'Are you sure you want to proceed paying PHP'+ this.props.detail.approvedAmount +' ?');
    }

    payMe(){
        this.resetModal()
        this.props.payCheckoutCardNow(this.props.navigation,'PayBookingScreen',this.props.id,this.props.detail.approvedAmount,this.props.detail.bookingId);
        //call api checkout then display webpage booking
    }

    resetModal(){
        this.props.setCancelBookingModal(false,'');
        this.props.payModal(false,'');
    }

    applyCancelation(){
        this.resetModal()
        this.props.updateMyBooking(this.props.navigation,'Bookings',this.props.detail.bookingId,'CANCEL_REJECT');
    }

    render(){
        return (
            <View>
                                <Loader loading={this.props.loading} />
                <ModalCustom 
                    modalVisible={this.props.cancelModal}
                    errorMessage={this.props.cancelModalMessage}
                >
                    <ButtonTransparent onPress={()=>this.applyCancelation()}>Yes</ButtonTransparent>
                    <ButtonTransparent onPress={()=>this.resetModal()}>No</ButtonTransparent>
                </ModalCustom>

                <ModalCustom 
                    modalVisible={this.props.loadPayModal}
                    errorMessage={this.props.payModalMessage}
                >
                    <ButtonTransparent onPress={()=>this.payMe()}>Yes</ButtonTransparent>
                    <ButtonTransparent onPress={()=>this.resetModal()}>No</ButtonTransparent>
                </ModalCustom>
                <View style={styles.header}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Bookings')}>
                        <Icon name="close" style={styles.icon}  />
                    </TouchableOpacity>
                    {this.renderCancelButton()}
                </View>
                <KeyboardAwareScrollView  extraScrollHeight={100} contentContainerStyle={{paddingHorizontal:20,paddingTop:20}} >
                <View style={{paddingBottom:150}}>
                    <Text style={{fontSize:25,fontWeight:'bold'}}>Booking</Text>
                    <View style={{paddingTop:20}}>
                        <View style={{flexDirection:'row',justifyContent:'space-between',paddingHorizontal:20}}>
                            <Text style={{paddingBottom:20}}>{this.props.detail.supplierName}</Text>
                            {this.renderApproved()}
                        </View>
                        <Card>
                            <CardSection>
                                <View style={{width:'100%'}}>
                                    <View style={{padding:10}}>
                                        <Text style={{fontSize:25}}>{this.props.detail.productBundleName}</Text>
                                    </View>
                                    <View style={{alignItems:'flex-end'}}>
                                        {this.renderAmount()}
                                    </View> 
                                </View> 
                            </CardSection>
                        </Card> 

                        <Card>
                            <CardSection>
                                <View style={{flex:1}}>
                                    <View style={{paddingHorizontal:20,flexDirection:'row',justifyContent:'space-between'}}>
                                        <View style={{justifyContent: 'center'}}>
                                            <Text style={{fontSize:30,fontWeight:'bold'}}>{this.props.detail.event.eventName}</Text>
                                        </View>
                                        <View style={{paddingVertical:13,flexDirection:'column'}}>
                                            <Text style={{fontSize:12}}>{this.props.detail.event.province.description}, </Text>
                                            <Text style={{fontSize:12}}>{this.props.detail.event.municipality.description}</Text>
                                        </View>
                                    </View>
                                    <View style={{flex:1,flexDirection:'row',alignItems:'stretch',paddingLeft:10,paddingBottom:10}}>
                                        <View style={{paddingHorizontal:5,borderRightWidth:1, flex:1}}> 
                                            <Text>Start</Text>
                                            <Text>{this.props.detail.event.displayableFrom}</Text>
                                        </View>
                                        <View style={{paddingHorizontal:5,borderLeftWidth:1, flex:1}}>
                                            <Text>End</Text>
                                            <Text>{this.props.detail.event.displayableTo}</Text>
                                        </View>
                                    </View>
                                </View>
                            </CardSection>
                        </Card>  
                        <View>
                            <View>
                                <View style={{flexDirection:'row',justifyContent:'space-between',paddingHorizontal:40,paddingBottom:10}}>
                                    <Text style={{fontSize:22}}>Item</Text>
                                    <Text style={{fontSize:22,paddingRight:5}}>Price</Text>
                                </View>
                                <View style={{flexDirection:'column',paddingBottom:30}}>
                                    {this.renderItems()}
                                </View> 
                            </View>
                        </View>  
                        {this.renderDisclaimer()}
                        {this.renderAdditionalFee()}
                        {this.renderPayment()}
                        {this.renderPaidText()}
                        {this.renderPaid()}
                    </View>
                </View>
                </KeyboardAwareScrollView>
            </View>
        )
    }
}

const styles = {
    header:{
        paddingLeft:25,
        paddingRight:25,
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
        backgroundColor:'#fafafa',
    }
}

const mapStateToProps = state => {
    console.log(state.bookingdetails.detail)
    return {
        detail:state.bookingdetails.detail,
        cancelModal:state.bookingdetails.loadCancelModal,
        cancelModalMessage:state.bookingdetails.cancelModalMessage,
        loadPayModal:state.bookingdetails.loadPayModal,
        payModalMessage:state.bookingdetails.payModalMessage,
        paidDetails:state.bookingdetails.paidDetails,
        id:state.idReducer.accountId,
        loading:state.bookingdetails.loading,
    }
}
export default connect(mapStateToProps, actions)(BookingDetail);

