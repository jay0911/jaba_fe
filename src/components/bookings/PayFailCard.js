import React, { Component } from 'react'
import {View,TouchableOpacity,Text} from 'react-native'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Icon from 'react-native-vector-icons/Ionicons'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

class PayFailCard extends Component{

    render(){
        return (
            <View>
                <View style={styles.header}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Bookings')}>
                        <Icon name="close" style={styles.icon}  />
                    </TouchableOpacity>
                </View>
                <KeyboardAwareScrollView  extraScrollHeight={100} contentContainerStyle={{paddingHorizontal:20,paddingTop:20}} >
                <View style={{paddingBottom:150}}>
                    <Text style={{fontSize:25,textAlign:'center',fontWeight:'bold'}}>Payment amount: {this.props.detail.approvedAmount}</Text>
                    <Text style={{fontSize:25,textAlign:'center',fontWeight:'bold'}}>{this.props.detail.productBundleName}</Text>
                    <Text style={{fontSize:25,textAlign:'center',fontWeight:'bold'}}>Payment Rejected</Text>
                    <Text style={{fontSize:18,textAlign:'center',fontWeight:'bold'}}>Kindly contact JABA support for further understanding</Text>
                </View>
                </KeyboardAwareScrollView>
            </View>
        )
    }
}

const styles = {
    header:{
        paddingLeft:25,
        paddingRight:25,
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
        backgroundColor:'#fafafa',
    }
}

const mapStateToProps = state => {
    console.log(state.bookingdetails.verificationInfo)
    return {
        detail:state.bookingdetails.detail,
        cancelModal:state.bookingdetails.loadCancelModal,
        cancelModalMessage:state.bookingdetails.cancelModalMessage,
        loadPayModal:state.bookingdetails.loadPayModal,
        payModalMessage:state.bookingdetails.payModalMessage,
        checkoutId:state.bookingdetails.checkoutId,
    }
}
export default connect(mapStateToProps, actions)(PayFailCard);

