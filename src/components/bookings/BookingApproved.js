import React, { Component } from 'react'
import {View,TouchableWithoutFeedback} from 'react-native'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import AsyncStorage from '@react-native-community/async-storage'
import { ScrollView } from 'react-native-gesture-handler'
import BookingCard from './BookingCard'
import {APPROVED} from '../../constants/BookingStatusConstants'


class BookingApproved extends Component{

    componentDidMount(){
        
    }

    loadBookingDetail(detail){
        this.props.loadBookingDetail(detail,this.props.navigation,'BookingDetailScreen')
    }

    renderBookingList(data){
        if (data && data.length) {
            return data.map(item => 
                <TouchableWithoutFeedback key={item.bookingId} onPress={()=>this.loadBookingDetail(item)}>
                    <View>         
                        <BookingCard 
                            displayableFrom={item.event.displayableFrom}
                            displayableTo={item.event.displayableTo}
                            eventName={item.event.eventName}
                            province={item.event.province.description}
                            municipality={item.event.municipality.description}
                            productName={item.productBundleName}
                        />     
                    </View>
                </TouchableWithoutFeedback>
            )
        }
    }

    render(){
        return (
                <View style={{padding:20}}>
                    <ScrollView>
                    <View>
                        {this.renderBookingList(this.props.details)}
                    </View>
                    </ScrollView>
                </View>  
        )
    }
}

const styles = {
}

const mapStateToProps = state => {
    return {
        details:state.bookingdetails.bookingApproved,
        id:state.idReducer.accountId
    }
}
export default connect(mapStateToProps, actions)(BookingApproved);

