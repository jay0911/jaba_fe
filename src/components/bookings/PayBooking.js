import React, { Component } from 'react'
import {View,TouchableOpacity,Text} from 'react-native'
import WebView from 'react-native-webview-bypass-ssl-errors'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Spinner from '../../commons/Spinner'
import Icon from 'react-native-vector-icons/Ionicons'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import {MAIN_SERVICE} from '../../api/backendserver'

import AsyncStorage from '@react-native-community/async-storage'

class PayBooking extends Component{

    _onNavigationStateChange(webViewState){
        console.log(webViewState.url)
        var str = webViewState.url;
        var patt = /google/i;
        var result = str.match(patt);
        if(!(result===null)){
            if(!this.props.checkoutFlag){
                this.props.changeCheckoutFlagPaymentCard(true)
                this.props.verifyMyCardPayment(this.props.navigation,this.props.checkoutId)
            }   
        }
    }

    async componentDidMount(){   
        this.props.changeCheckoutFlagPaymentCard(false)  
    }

    render(){
        return (
            <View>
                <View style={styles.header}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Bookings')}>
                        <Icon name="close" style={styles.icon}  />
                    </TouchableOpacity>
                </View>
                <KeyboardAwareScrollView  extraScrollHeight={100} contentContainerStyle={{paddingHorizontal:20,paddingTop:20}} >
                <View style={{paddingBottom:150}}>
                    <Text style={{fontSize:25,fontWeight:'bold'}}>Payment amount: {this.props.detail.approvedAmount}</Text>
                    <Text>{this.props.checkoutId}</Text>
                    <Text>Product name:{this.props.detail.productBundleName}</Text>
                    {(this.props.checkoutFlag===false)?<WebView
                        onNavigationStateChange={this._onNavigationStateChange.bind(this)}
                        source={{
                        uri: `${MAIN_SERVICE}/index/${this.props.checkoutId}`
                        }}
                        style={{ marginTop: 20,height: 500, width: '100%' }}
                    />:<Spinner/>}
                </View>
                </KeyboardAwareScrollView>
            </View>
        )
    }
}

const styles = {
    header:{
        paddingLeft:25,
        paddingRight:25,
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
        backgroundColor:'#fafafa',
    }
}

const mapStateToProps = state => {
    console.log(state.bookingdetails.checkoutFlag)
    return {
        detail:state.bookingdetails.detail,
        cancelModal:state.bookingdetails.loadCancelModal,
        cancelModalMessage:state.bookingdetails.cancelModalMessage,
        loadPayModal:state.bookingdetails.loadPayModal,
        payModalMessage:state.bookingdetails.payModalMessage,
        checkoutId:state.bookingdetails.checkoutId,
        checkoutFlag:state.bookingdetails.checkoutFlag
    }
}
export default connect(mapStateToProps, actions)(PayBooking);

