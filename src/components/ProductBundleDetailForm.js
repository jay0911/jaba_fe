import React, { Component } from 'react'
import {View,TouchableOpacity,Text,Image,TouchableWithoutFeedback} from 'react-native'
import { connect } from 'react-redux'
import * as actions from '../actions'
import Icon from 'react-native-vector-icons/Ionicons'
import ButtonLoadMore from './../commons/ButtonLoadMore'
import Button from './../commons/Button'
import CarouselWithoutDot from '../commons/CarouselWithoutDot'
import {MAIN_SERVICE} from '../api/backendserver'
import { FlatList } from 'react-native-gesture-handler';
import Item from '../commons/Item'
import ReviewItem from '../commons/ReviewItem'
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import ModalCustom from '../commons/ModalCustom'
import ButtonTransparent from '../commons/ButtonTransparent'
import {SAVED,SEARCH,PRODUCT_DETAIL} from '../constants/ProductSourceScreen'
import IconHeart from '../commons/IconHeart'

class ProductBundleDetailForm extends Component{

    passProductBundle(data){
        this.props.selectProductBundle(data,this.props.navigation,'ProductBundleDetail',PRODUCT_DETAIL,this.props.id)
    }


    componentDidMount(){
    }

    renderRating(ratings){
        return ratings.length>0?(
            Math.round((ratings.reduce((prev, current) => {
                return prev + + current.rating
            },0)/ratings.length)* 100)/100+ '('+ratings.length+') '
        ):null
    }

    renderOtherProduct(){
        return this.props.otherProducts!=null?(this.props.otherProducts.length>0?(
            
                    <View style={styles.itemListContainerOtherProduct}>
                        <Text style={{paddingBottom:20,fontSize:20,fontWeight:'bold'}}>More packages like this</Text>
                    <FlatList
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            data={this.props.otherProducts}
                            keyExtractor={(result)=>result.id}
                            renderItem={({item})=>{
                                return (
                                    <TouchableWithoutFeedback onPress={()=>this.passProductBundle(item)}>
                                    <View style={styles.otherProductContainer}>

                                            <Image resizeMode='contain'  style={styles.otherProductImage} source={{ uri: `${MAIN_SERVICE}/image/bucket-image/${item.images[0].imageId}` }}/>

                                            <View style={{borderTopColor: '#dbdbdb',borderTopWidth: 2,flex:1,paddingLeft:15,alignItems:'flex-start'}}>
                                                <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end'}}>
                                                    <Text style={{color:'#646866',fontWeight:'bold',fontSize:15}} >{item.name}</Text>
                                                    
                                                    {item.ratings.length>0?<View style={{flex:1,flexDirection:'row',paddingRight:5,justifyContent:'flex-end'}}>
                                                        <View >
                                                            <Image style={{height:20,width:20}} source={require('../assets/star.png')} />     
                                                        </View>
                                                        <Text>{this.renderRating(item.ratings)}</Text>
                                                    </View>:null}
                                                </View>
                                                <View style={{flex:2,flexDirection:'column'}}>
                                                    <Text style={{color:'#646866',fontSize:10}}>{item.organization.name}</Text>
                                                    <Text style={{fontSize:10}} numberOfLines={1} >{item.description}</Text>
                                                    <Text style={{fontSize:10,fontWeight:'bold'}}>{`$ ${item.price}`}</Text>                   
                                                </View>
                                            </View>
                                    </View>
                                    </TouchableWithoutFeedback>
                                )
                            }}
                        />
                        </View>  
            ):null):null    
    }

    renderResultList(){
        return this.props.itemDetails!=null?(       
                <View style={styles.itemListContainer}>
                    
                    <Text style={{paddingBottom:20,fontSize:20,fontWeight:'bold'}}>Package includes</Text>
                    <View style={{paddingBottom:20}}>
                    <FlatList
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            data={this.props.itemDetails.items}
                            keyExtractor={(result)=>result.id}
                            renderItem={({item})=>{
                                return (
                                    <Item result={item}/>
                                )
                            }}
                        />
                        </View>
                </View>            
            ):null  
    }

    renderSupplierVerified(){
        return this.props.org.organizationStatus==="ACTIVE"?(
            <View style={{flex:1,flexDirection:'row'}}>
                <Image style={styles.starImage} source={require('../assets/star.png')} />
                <Text style={styles.orgVerified}>
                    Identity Verified
                </Text>
            </View>
        ):null
    }

    renderSupplier(){
        return this.props.org!=null?(
            <View style={styles.itemListContainer}>
                <View style={{flex:1,flexDirection:'column'}}>
                    <View style={{flex:1,flexDirection:'row',justifyContent:'space-between'}}>   
                        <View style={{paddingLeft:20,paddingTop:20,paddingBottom:20}}>
                            <Text style={styles.orgName}>
                                {this.props.org.name}
                            </Text>
                            <Text>
								Joined in {this.props.org.dtimeCreated}
							</Text>
                        </View>
                        <View style={{paddingRight:30}}>
                            <Image 
                            style={styles.thumbnailStyle}
                            resizeMode='contain'
                            source={{ uri: `${MAIN_SERVICE}/image/bucket-image/${this.props.bundle.organization.logoId}`}} /> 
                        </View>
                    </View>  
                    <View style={{flex:1,flexDirection:'row',justifyContent:'space-between'}}> 
                        {this.props.org.ratings.length>0?<View style={{flex:1,flexDirection:'row',justifyContent:'flex-start',paddingLeft:20}}>
                            <Image style={styles.starImage} source={require('../assets/star.png')} />
                            <Text style={styles.reviewText}>{this.renderRating(this.props.org.ratings)}</Text>
                        </View>:null}
                        <View style={{paddingRight:10}}>
                            {this.renderSupplierVerified()}
						</View>
                    </View>  
                </View>
                <View style={{padding: 20}}>
                    <ButtonLoadMore onPress={()=>this.props.navigation.navigate('OrganizationDetailScreen')}>Contact Supplier</ButtonLoadMore>
                </View>
                <View style={{flex:1,flexDirection:'row',justifyContent:'center'}}>  
                    <View style={{flex:8,paddingHorizontal:20,paddingBottom:20}}>
                        <Text style={{fontSize:10}}>
                            To protect your payment,never transfer money or communicate outside of this app.
                        </Text>
                    </View>
                    <View style={{flex:1}}>
                        <Image style={{height:20,width:20,paddingLeft:20}} source={require('../assets/star.png')} />
                    </View>
                </View>
            </View>
        ):null
    }

    renderReviewText(){
        return this.props.bundle.ratings===1?(
            <Text style={styles.reviewText}>review</Text>
        ):(<Text style={styles.reviewText}>reviews</Text>)
    }

    renderComments(){
        return this.props.bundle.ratings.length>0?(
            <View>
                <View style={{flex:1,flexDirection:'row',marginHorizontal: 30,marginBottom:5}}>
                    <Image style={styles.starImage} source={require('../assets/star.png')} />
                    <Text style={styles.reviewText}>{this.renderRating(this.props.bundle.ratings)}</Text>
                    {this.renderReviewText()}
                </View>
                <View style={styles.itemListContainer}>
                    <FlatList
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            data={this.props.bundle.ratings}
                            keyExtractor={(item, index) => {
                                return 'key'+index
                            }}
                            renderItem={({item})=>{
                                return (
                                    <ReviewItem result={item}/>
                                )
                            }}
                        />
                    <View style={{padding: 20}}>
                        <ButtonLoadMore onPress={()=>this.props.navigation.navigate('RatingScreen')}>
                            Show all {this.props.bundle.ratings.length} ratings
                        </ButtonLoadMore>
                    </View>
                </View>
            </View>    
            ):null    
    }

    backButton(){
        if(!this.props.navigation.goBack()){
            if(this.props.route==SAVED){
                this.initSavedListProduct();
                this.props.navigation.navigate('Saved')
            }else if(this.props.route==SEARCH){
                this.props.navigation.navigate('Explore')
            }
            
        }
    }

    initSavedListProduct(){
        this.props.initSavedProduct();
        this.props.findSavedProduct(this.props.id,0,[])
    }

    bookMe(){
        if(this.props.itemDetails!=null){
            this.props.checkAccountHasEvent(this.props.id,this.props.navigation,'StartBookingScreen',
            this.props.bundle,
            this.props.itemDetails);
        }
    }

    resetModalCheckEvent(){
        this.props.resetModalCheckUserHasEvent();
    }
    resetModalCheckEventGoToEvent(){
        this.props.resetModalCheckUserHasEvent();
        this.props.navigation.navigate('Events')
    }

    likeOrUnlikeProduct(data){
        if(data.isSaved ==='false'){
            this.props.productItemSaveChangeProperty(data,this.props.id,'true')
        }else{
            this.props.productItemSaveChangeProperty(data,this.props.id,'false')
        }
    }

    renderBundle(){
        return this.props.org!=null?(
            <View>
                <View style={styles.imagesContainer}>
                    <CarouselWithoutDot data={this.props.bundle.images}/>
                </View>
                            <View style={styles.productInfo}>
                                <View style={styles.thumbnailInfoStyle}>
                                    <Text style={styles.headerTextStyle}>{this.props.bundle.name}</Text>            
                                    {this.props.bundle.ratings.length>0?<View style={{flex:1,flexDirection:'row',justifyContent:'flex-start'}}>
                                            <>
                                            <Image style={styles.starImageLittle} source={require('../assets/star.png')} />
                                            <Text>{this.renderRating(this.props.bundle.ratings)}</Text>
                                            </>  
                                    </View>:null} 
                                    <View style={{flex:1,flexDirection:'row',justifyContent:'flex-start'}}>
                                        <Text>{this.props.org.name}</Text>
                                        <Text style={{color:'#646866'}}> {this.props.org.address.municipality}, {this.props.org.address.province}</Text>
                                    </View>
                                    <View style={{flex:1,flexDirection:'row',justifyContent:'flex-start'}}>
                                        <Text style={{paddingLeft:2}}>Price: {this.props.bundle.price}</Text>
                                    </View>
                                </View>
                                <View style={styles.imageMerchant}>
                                    <Image 
                                    style={styles.thumbnailStyle}
                                    resizeMode='contain'
                                    source={{ uri: `${MAIN_SERVICE}/image/bucket-image/${this.props.bundle.organization.logoId}`}} />
                                </View>
                            </View>

                            <View style={styles.productDescription}>
                                <Text>{this.props.bundle.description}</Text>
                            </View>
            </View>
        ):null
    }

    render(){
        return (

            <StickyHeaderFooterScrollView
                renderStickyHeader={() => (
                    <View style={styles.header}>
                    <TouchableOpacity onPress={()=>this.backButton()}>
                        <Icon name="arrow-back" style={styles.icon}  />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.likeOrUnlikeProduct(this.props.bundle)} >
                         <IconHeart isSaved={this.props.bundle.isSaved} />     
                    </TouchableOpacity>
                    </View>
                )}
                renderStickyFooter={() => (
                    <View style={styles.header}>
                        <View style={{flex:1}}>
                            <Text style={{paddingTop:10,fontSize:20,fontWeight:'bold'}}>Total Amount: {this.props.bundle.price}</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Button onPress={()=>this.bookMe()}>
                                    Book me
                            </Button>
                        </View>  
                    </View>
                )}
                makeScrollable={true}
            >
            <ModalCustom 
                modalVisible={this.props.showModal}
                errorMessage={this.props.errMsg}
            >
                <ButtonTransparent onPress={()=>this.resetModalCheckEventGoToEvent()}>Go To Event Tab</ButtonTransparent>
                <ButtonTransparent onPress={()=>this.resetModalCheckEvent()}>Book Later</ButtonTransparent>
            </ModalCustom>

            {this.props.bundle!=null?<View>
                            <View>
                                {this.renderBundle()}
                            </View>

                            <View>      
                                {this.renderResultList()}
                            </View>

                            <View>
                                {this.renderComments()}
                            </View>

                            <View>
                                {this.renderSupplier()}
                            </View>

                            <View>
                                {this.renderOtherProduct()}
                            </View>
                </View>:null}
            </StickyHeaderFooterScrollView>


        )
    }
}

const styles = {
    otherProductContainer:{
        borderWidth:1,
        borderRadius:1,
        borderColor:'#ddd',
        borderBottomWidth:1,
        shadowColor:'#000',
        shadowOpacity: 0.1,
        shadowRadius: 1,
        elevation: 1,
        marginLeft:5,
        marginRight:5,
        marginBottom:20,
        position:'relative',
        width:250,
    },
    itemListContainerOtherProduct:{
        marginHorizontal:20,
        marginBottom:20,
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
        height: 270,
        flex:1
    },

    otherProductImage: {
        margin:10,
        width: 230,
        height: 100,
    },
    orgName:{
        fontWeight:'bold',
        fontSize:20
    },
    orgVerified:{
        marginHorizontal: 2,marginBottom:5,fontSize:20
    },
    reviewText:{
        marginHorizontal: 2,marginBottom:5,fontSize:20
    },
    starImageLittle:{
        width: 15,
        height: 15,
    },
    starImage:{
        width: 30,
        height: 30,
    },
    header:{
        paddingLeft:25,
        paddingRight:25,
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
        backgroundColor:'#fafafa',
    }, imagesContainer:{
        margin: 10,
        borderBottomWidth:2,
        justifyContent:'flex-start',
        flexDirection: 'row',
        borderColor:'#ddd',
        position:'relative'
    },
    thumbnailInfoStyle:{
        flex:2,
        marginLeft: 10,
        marginRight:10
    },
    deeperInfo:{
        flex:1,
        flexDirection:'row'
    },
    imageStyle:{
        height: 300,
        flex:1,
        width: null
    },
    headerTextStyle:{
        fontSize:25,
        fontWeight:'bold'
    },
    thumbnailStyle:{
        height:75,
        width:75
    },
    imageMerchant:{
        width: 75,
        height: 75,
        borderRadius: 50,
        overflow: "hidden",
        margin:10
    },
    productInfo:{
        justifyContent:'space-between',
        flexDirection: 'row',
        marginHorizontal: 20
    },
    itemListContainer:{
        marginHorizontal:20,
        marginBottom:20,
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
    },
    productDescription:{
        marginTop:10,
        marginBottom:20,
        marginHorizontal:20,
        paddingBottom:20,
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
    }
}

const mapStateToProps = state => {
    return {
        bundle:state.productDetailItem.bundle,
        itemDetails:state.productDetailItem.itemDetails,
        org:state.productDetailItem.org,
        otherProducts:state.productDetailItem.otherProducts,
        showModal:state.productDetailItem.showModal,
        errMsg:state.productDetailItem.errMsg,
        id:state.idReducer.accountId
    }
}
export default connect(mapStateToProps, actions)(ProductBundleDetailForm);