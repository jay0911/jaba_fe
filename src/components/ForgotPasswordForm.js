import React, { Component } from 'react'
import {
    View,Modal
} from 'react-native';
import Spacer from '../commons/Spacer'
import {Text,Input} from 'react-native-elements'
import Button from '../commons/Button'
import ButtonTransparent from '../commons/ButtonTransparent'
import LoadingButton from '../commons/LoadingButton'
import CardSection from '../commons/CardSection'
import { connect } from 'react-redux'
import * as actions from '../actions'

class ForgotPasswordForm extends Component{

    state = {
        modalVisible: false
    };
    onAccept() {
        this.setState({modalVisible:false})
    }
    async onDecline() {
        this.setState({modalVisible:true})
    }
    componentDidMount(){
        console.log(this.props)
    }

    renderError(){
        if(this.props.err){
            return (
                <View>
                    <Text style={styles.errTextStyle}>
                        {this.props.err}
                    </Text>
                </View>
            )
        }
    }

    renderButton(){
        if(this.props.loading){
            return <LoadingButton />
        }

        return (
            <Button onPress={()=>{
                this.props.forgotMe({email:this.props.email})
                this.onDecline()
            }}>
                Next
            </Button>
        )
    }

    render(){
        return(
        <View>
            <Spacer>
                <Text style={{textAlign:'center'}} h3>Enter your Email</Text>
            </Spacer>
    
            <Input 
                placeholder="user@mail.com"
                onChangeText={text => this.props.genericChange({prop:'email',value:text})}
                value={this.props.email}
                autoCapitalize="none"
                autoCorrect={false}
            />
            {this.renderError()}
            {this.renderButton()}
            <Modal
                    visible={this.state.modalVisible}
                    transparent
                    animationType="slide"
                    onRequestClose={() => {}}
                    >
                    <View style={{ position: 'relative',
                                    borderRadius:4,
                                    flex: 1,
                                    paddingHorizontal:40,
                                    justifyContent: 'center'}}>
                        <CardSection style={{justifyContent: 'center'}}>
                        <Text style={{
                                flex: 1,
                                fontSize: 18,
                                textAlign: 'center',
                                lineHeight: 40
                            }} >
                            Your password has been sent to your email
                        </Text>
                        </CardSection>

                        <CardSection>
                        <View style={{flex:1}}>
                            <ButtonTransparent onPress={this.onAccept.bind(this)}>Done</ButtonTransparent>
                        </View>
                        </CardSection>
                    </View>
            </Modal>
        </View>
        )
    }
}

const styles = {
    errTextStyle:{
        color:'red',
        paddingBottom: 15,
        paddingLeft:10,
        fontSize:18
    }
}

const mapStateToProps = state => {
    return {
        email:state.forgot.email,
        loading:state.forgot.loading,
        err:state.forgot.errorMessage
    }
}

export default connect(mapStateToProps, actions)(ForgotPasswordForm)