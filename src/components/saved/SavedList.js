import React, { Component } from 'react'
import {Text,View,TouchableWithoutFeedback,Dimensions,Image, FlatList, Animated} from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import Card from '../../commons/Card'
import CardSection from '../../commons/CardSection'
import ButtonLoadMore from '../../commons/ButtonLoadMore'
import Loader from '../../commons/Loader'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import CarouselItem from '../../commons/CarouselItem'
import {SAVED} from '../../constants/ProductSourceScreen'
import IconHeart from '../../commons/IconHeart'

class SavedList extends Component{

    loadmore(){
        this.props.findSavedProduct(this.props.id,this.props.offset,this.props.serverData);
    }

    renderCarousel({ data },{fulldata}){
        const { width, heigth } = Dimensions.get('window')

        const scrollX = new Animated.Value(0)
        let position = Animated.divide(scrollX, width)
        if (data && data.length) {
            return (
                <View>
                    <FlatList data={data}
                        keyExtractor={(item, index) => {
                            return 'key'+index
                        }}
                        horizontal
                        pagingEnabled
                        scrollEnabled
                        snapToAlignment="center"
                        scrollEventThrottle={16}
                        decelerationRate={"fast"}
                        showsHorizontalScrollIndicator={false}
                        renderItem={({ item }) => {
                            return (
                                <TouchableWithoutFeedback onPress={()=>this.passProductBundle(fulldata)}>
                                   <View>
                                     <CarouselItem id={item} item={item} />
                                   </View>
                                </TouchableWithoutFeedback>
                            )
                        }}
                        onScroll={Animated.event(
                            [{ nativeEvent: { contentOffset: { x: scrollX } } }],{useNativeDriver: false}
                        )}
                    />
                    
                    <View style={styles.dotView}>
                        {data.length>1?data.map((_, i) => {
                            let opacity = position.interpolate({
                                inputRange: [i - 1, i, i + 1],
                                outputRange: [0.3, 1, 0.3],
                                extrapolate: 'clamp'
                            })
                            return (
                                <Animated.View
                                    key={i}
                                    style={{ opacity, height: 10, width: 10, backgroundColor: '#7f8082', margin: 2, borderRadius: 5 }}
                                />
                            )
                        }):null}
                    </View>
                </View>
            )
        }

        console.log('Please provide Images')
        return null
    }

    renderLoadmore(){
        return(
            <View style={{marginBottom:10}}>
                <ButtonLoadMore onPress={()=>this.loadmore()}>
                    Load more...
                </ButtonLoadMore>
            </View>
        )
    }

    renderRatingCalc(ratings){
        console.log('ratings')
        console.log(ratings)
        return ratings.length>0?(
            Math.round((ratings.reduce((prev, current) => {
                return prev + + current.rating
            },0)/ratings.length)* 100)/100+ '('+ratings.length+') '
        ):null
    }

    passProductBundle(data){
        this.props.selectProductBundle(data,this.props.navigation,'exploreFlow',SAVED)
        this.props.initfindProduct();
    }

    likeOrUnlikeProduct(data){
        this.props.productSavedListChangeProperty(this.props.serverData,data.id,this.props.id,data.isSaved)
    }

    renderMe(){
        return this.props.serverData.map(data => 
                <View key={data.id} >
                    <Card >
                        <CardSection>
                            <TouchableWithoutFeedback onPress={()=>this.likeOrUnlikeProduct(data)}>  
                                <View style={{position: 'absolute',zIndex: 1,padding:10,right:1}}>
                                     <IconHeart isSaved={data.isSaved} />
                                </View>
                            </TouchableWithoutFeedback> 
                            {this.renderCarousel({data:data.images},{fulldata:data})}
                        </CardSection>
                        <CardSection>
                            <View style={{flex:1,paddingLeft:15,alignItems:'flex-start'}}>
                                <View style={{flex:1,flexDirection:'row'}}>
                                    <View>
                                        <Text style={{fontSize:20,color:'#646866'}}>{data.name}</Text>
                                    </View>
                                    {data.ratings.length>0?<View style={{flex:1,flexDirection: 'row',justifyContent: 'flex-end'}}>
                                        
                                      
                                        <View style={{paddingLeft:5,paddingRight:5}}>
                                            <Image style={styles.starImage} source={require('../../assets/star.png')} />     
                                        </View>
                                        <Text>{this.renderRatingCalc(data.ratings)}</Text>
                                     
                                        
                                    </View>:null}
                                </View>
                                <Text style={{color:'#646866'}}>{data.organization.name}</Text>
                                <Text numberOfLines={1} style={{fontSize:12}}>{data.description}</Text>
                                <Text style={{fontSize:12,fontWeight:'bold'}}>{`$ ${data.price}`}</Text>
                            </View>
                        </CardSection>
                    </Card>
                </View>
        )
    }

    render(){

        return (
            <View>
                <Loader loading={this.props.likedPage} />
                <ScrollView>
                    {this.renderMe()}
                    {this.props.showLoadmore?this.renderLoadmore():null}
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    additionalInfo:{
        height: 300,
        flex:1,
        width: null
    },
    starImage:{
        width: 20,
        height: 20
    },
    dotView: {flexDirection: 'row',position: 'absolute', left: 0, right: 0, bottom: 1, justifyContent: 'center', alignItems: 'center'}
}

const mapStateToProps = state => {
    return {
        offset:state.savedProduct.offset,
        serverData:state.savedProduct.serverData,
        loading:state.savedProduct.loading,
        showLoadmore:state.savedProduct.showLoadmore,
        id:state.idReducer.accountId,
        likedPage:state.mainTabReducer.likedPage
    }
}

export default connect(mapStateToProps, actions)(SavedList)

