import React, {Component} from 'react'
import CardSection from './../commons/CardSection'
import {Platform,UIManager,LayoutAnimation ,Text, TouchableWithoutFeedback, View} from 'react-native'
import * as actions from '../actions'
import {connect} from 'react-redux'

class ListItem extends Component {

    componentDidUpdate(){
        console.log('updateme');
    }

    renderDescription(){
        if(this.props.expanded){
            return (
                <CardSection>
                    <Text style={{flex:1}}>{this.props.item.item.description}</Text>
                </CardSection>
            )
        }
    }

    render() {
        const {id,title} = this.props.item.item
       
        return (
            <TouchableWithoutFeedback
                onPress={()=> this.props.selectLibrary(id)}
            >
                <View>
                    <CardSection>
                        <Text style={styles.titleStyle}>
                            {title}
                        </Text>
                    </CardSection>
                    {this.renderDescription()}
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = {
    titleStyle:{
        fontSize: 18,
        paddingLeft:15
    }
}

const mapStateToProps = (state,ownProps) => {
    const expanded = state.selectedLibraryId === ownProps.item.index
    return {expanded}
}

export default connect(mapStateToProps, actions)(ListItem)