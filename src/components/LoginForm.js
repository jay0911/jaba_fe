import React,{Component} from 'react'
import {
  View,Modal
} from 'react-native';
import {Input,Text} from 'react-native-elements'
import {createSession} from '../api/apis'
import { connect } from 'react-redux'
import * as actions from '../actions'
import Button from '../commons/Button'
import ButtonTransparent from '../commons/ButtonTransparent'
import ModalCustom from '../commons/ModalCustom'
import LoadingButton from '../commons/LoadingButton'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Loader from '../commons/Loader'

class LoginForm extends Component {

    onAccept() {
        this.props.reset();
    }

    renderButton(){
        if(this.props.loading){
            return (<View>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('Forgot')}>
                    <Text style={styles.textFooterLink}>Forgot password?</Text>
                </TouchableOpacity>
                <LoadingButton />
            </View>)
        }

        return (
            <View>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('Forgot')}>
                <Text style={styles.textFooterLink}>Forgot password?</Text>
            </TouchableOpacity>
            
            <Button onPress={()=>{
                this.props.loginUser(this.props.user,this.props.navigation,'Explore')
            }
            }>
                Log in
            </Button>
            </View>
        )
    }

    render(){
        return(
            <View> 
                <Loader loading={this.props.loading} />
                <ModalCustom 
                    modalVisible={this.props.modalVisible}
                    errorMessage={this.props.err}
                >
                    <ButtonTransparent onPress={this.onAccept.bind(this)}>Try again</ButtonTransparent>
                </ModalCustom>

                <Input 
                    placeholder="Email"
                    value={this.props.user.username}
                    onChangeText={username => this.props.loginFormChange({prop:'username',value:username})}
                    autoCapitalize="none"
                    autoCorrect={false}
                />
            
                <Input 
                    secureTextEntry
                    placeholder="Password"
                    value={this.props.user.password}
                    onChangeText={password =>this.props.loginFormChange({prop:'password',value:password})}
                    autoCapitalize="none"
                    autoCorrect={false}
                />
                {this.renderButton()}
            </View>
        )
    }

}

const styles = {
    textFooterLink:{
        alignSelf:'flex-end',
        color:'#3b5998',
        paddingRight:10,
        paddingBottom:15
    },
    errTextStyle:{
        color:'red',
        paddingBottom: 15,
        paddingLeft:10,
        fontSize:18
    },
    cardSectionStyle: {
        justifyContent: 'center'
    },
    textStyle: {
        flex: 1,
        fontSize: 18,
        textAlign: 'center',
        lineHeight: 40
    },
    containerStyle: {
        position: 'relative',
        flex: 1,
        justifyContent: 'center'
    }
}

const mapStateToProps = state => {
    const {username,password} = state.login;
    return {
        user:{username,password},
        loading:state.login.loading,
        err:state.login.errorMessage,
        modalVisible:state.login.modalVisible
    }
}



export default connect(mapStateToProps, actions)(LoginForm)
