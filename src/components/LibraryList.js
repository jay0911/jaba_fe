import React, { Component } from 'react'
import {connect} from 'react-redux'
import { FlatList,View } from 'react-native'
import ListItem from './ListItem'
import {createSession,getCategory,register} from '../api/apis'
import Spacer from '../commons/Spacer'
import {Button} from 'react-native-elements'

class LibraryList extends Component{

    renderItem(item){
        return <ListItem item={item} />
    }

    componentDidMount(){
        createSession();
    }

    async get(){
        console.log('get')
        const user = {email:'gfsffdsfsfgs',firstname:'john',lastname:'john',password:'john'};
        console.log(await register(user).then(async respose => {
            console.log(respose.data.data)
            return respose; 
        }).catch(respose => {
            createSession();
            console.log(respose.response.data.message);  
            return respose;
        }))
    }

    render(){
        return (
            <View>
                <FlatList
                    data={this.props.items}
                    renderItem={(this.renderItem)}
                    keyExtractor={item=>item.id}
                />
                <Spacer>
                    <Button title="Logout" onPress={()=>this.get()} />
                </Spacer>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return { items: state.libraries }
}

export default connect(mapStateToProps)(LibraryList)