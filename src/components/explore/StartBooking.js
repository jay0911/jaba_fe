import React, { Component } from 'react'
import {Text,TextInput,View,TouchableOpacity,Image,ScrollView,findNodeHandle} from 'react-native'
import {Input} from 'react-native-elements'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Icon from 'react-native-vector-icons/Ionicons'
import Card from './../../commons/Card'
import Button from '../../commons/Button'
import CardSection from './../../commons/CardSection'
import {Picker} from '@react-native-community/picker';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import ModalCustom from './../../commons/ModalCustom'
import ButtonTransparent from './../../commons/ButtonTransparent'
import Loader from './../../commons/Loader'

class StartBooking extends Component{

    state = {
        chosenId:''
    }

    componentDidMount(){
        this.props.loadEventDropDown();
        this.props.replaceItems(this.props.itemDetails.items)
        this.props.bookingPropsChange({prop:'computedAmount',value:this.props.bundle.price})
        this.resetModal();
    }

    selectEventType(itemValue){
        var item = this.props.eventList.find(x => x.eventId === itemValue)
        this.props.bookingPropsChange({prop:'eventChosen',value:itemValue})
        this.props.chooseEventInBooking(item)
    }

    onChanged(text) {
        this.props.bookingPropsChange({prop:'bargainAmount',value:text.replace(/[^0-9]/g, '')})
    }

    renderNewItems(id){
        
        var array = [...this.props.bargainItems]; // make a separate copy of the array
        var index = array.map(item => item.id)
                       .indexOf(id);
        if(array.length<=1){
            this.resetModal()
            this.props.setBookingWarningModal(true,'Must have 1 item in a booking')
            return;
        }

        if (index !== -1) {
            array.splice(index, 1);
        }

        var total = array.reduce((prev, current) => {
            return prev + + current.price
        },0)
        this.props.bookingPropsChange({prop:'computedAmount',value:total})
        this.props.replaceItems(array)
        this.resetModal()
    }

    beforeRemoveItem(id,name){
        this.setState({...this.state,chosenId:id});
        this.props.setBookingItemRemovalModal(true,'Are you sure you want to remove item '+name+'?')
    }

    resetModal(){
        this.props.setBookingWarningModal(false,'')
        this.props.setBookingItemRemovalModal(false,'')
        this.props.setBookingFinalModal(false,'')
    }

    validateSubmit(){
        if(this.props.eventChosen==''){
            this.props.setBookingWarningModal(true,'Please select your event before proceeding')
            return
        }

        this.props.setBookingFinalModal(true,'Are you sure you want to book this product?')
        
    }

    submitMe(){
        var newVal = "";
        
        for(let i=0;i<this.props.bargainItems.length;i++){
            if(i===0){
                newVal = newVal+`{"bargainPrice": ${Number(this.props.bargainItems[i].price)},"id": "${this.props.bargainItems[i].id}"}`
            }else{
                newVal = newVal+`,{"bargainPrice": ${Number(this.props.bargainItems[i].price)},"id": "${this.props.bargainItems[i].id}"}`
            }   
        }

        const baseReq = `{"accountId": "${this.props.id}","bargainPrice": ${Number(this.props.bargainAmount)},"eventId": "${this.props.eventChosen}","items": [${newVal}],"productId": "${this.props.bundle.id}"}`
        this.props.bookMePlease(this.props.navigation,'Bookings',baseReq)
    }

    renderItems(){
        if(this.props.bargainItems==null){
            return null;
        }
        if (this.props.bargainItems && this.props.bargainItems) {
            return this.props.bargainItems.map((item, index) => {
                return (
                    <View key={index} style={{flexDirection:'row',justifyContent:'space-between',width:'100%',paddingHorizontal:30}}>
                        <View style={{flexDirection:'row'}}>
                            <TouchableOpacity onPress={()=>this.beforeRemoveItem(item.id,item.name)}><Icon name="remove-circle-outline" style={styles.icon}  /></TouchableOpacity>
                            <Text style={{fontSize:22,paddingLeft:5}}>{item.name}</Text>
                        </View>
                        <View style={{flexDirection:'row',paddingBottom:10}}>
                            <Text style={{fontSize:22,paddingRight:5}}>Php {item.price}</Text>
                        </View>
                    </View>
                ) 
            })
        }
        return null
    }
      
    render(){
        return (
            <View>
            <Loader loading={this.props.loading} />
            <ModalCustom 
                modalVisible={this.props.showModalRemovalItem}
                errorMessage={this.props.removalItemErrMsg}
            >
                <ButtonTransparent onPress={()=>this.renderNewItems(this.state.chosenId)}>Yes</ButtonTransparent>
                <ButtonTransparent onPress={()=>this.resetModal()}>No</ButtonTransparent>
            </ModalCustom>
            <ModalCustom 
                modalVisible={this.props.bookingModal}
                errorMessage={this.props.bookingModalErrMsg}
            >
                <ButtonTransparent onPress={()=>this.submitMe()}>Yes</ButtonTransparent>
                <ButtonTransparent onPress={()=>this.resetModal()}>No</ButtonTransparent>
            </ModalCustom>
            <ModalCustom 
                modalVisible={this.props.warningModal}
                errorMessage={this.props.warningModalErrMsg}
            >
                <ButtonTransparent onPress={()=>this.resetModal()}>Confirm</ButtonTransparent>
            </ModalCustom>
                <View style={styles.header}>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                        <Icon name="arrow-back" style={styles.icon}  />
                    </TouchableOpacity>
                </View>
                <KeyboardAwareScrollView  extraScrollHeight={100} contentContainerStyle={{paddingHorizontal:20,paddingTop:20}} >
                <View style={{paddingBottom:150}}>
                    <Text style={{fontSize:25,fontWeight:'bold'}}>Booking</Text>
                    <View style={{paddingTop:20}}>
                        <Text style={{paddingBottom:20}}>{this.props.bundle.organization.name}</Text>
                        <Card>
                            <CardSection>
                                <View style={{width:'100%'}}>
                                    <View style={{padding:10}}>
                                        <Text style={{fontSize:25}}>{this.props.bundle.name}</Text>
                                    </View>
                                    <View style={{alignItems:'flex-end'}}>
                                        {this.props.bundle.isNegotiable==="true"?<Text style={{paddingHorizontal:10}}>Negotiable</Text>:null}
                                        <Text style={{paddingHorizontal:10,fontSize:30,fontWeight:'bold'}}>Php {this.props.computedAmount}</Text>
                                        <Text style={{paddingHorizontal:10,paddingBottom:10,fontSize:20,fontWeight:'bold'}}>Total Amount</Text>
                                    </View> 
                                </View> 
                            </CardSection>
                        </Card>
                        <Card>
                            <CardSection>
                                <View style={{flex:1}}>
                                    <View style={{paddingHorizontal:20,flexDirection:'row',width:'100%'}}>
                                        <View>
                                            <Picker
                                                style={{ height: 50, width: 200}}
                                                mode="dropdown"
                                                selectedValue={this.props.eventChosen}
                                                onValueChange={(itemValue, itemIndex)=>{this.selectEventType(itemValue)}}> 
                                                <Picker.Item label='Choose you event' value='' />
                                                {this.props.eventList.map((item, index) => {
                                                    return (<Picker.Item label={item.eventName} value={item.eventId} key={index}/>) 
                                                })}
                                            </Picker>
                                        </View>
                                        {this.props.eventObjectChosen!=null?
                                            <View style={{paddingVertical:13,flexDirection:'column'}}>
                                                <Text style={{fontSize:12}}>{this.props.eventObjectChosen.province.description}, </Text>
                                                <Text style={{fontSize:12}}>{this.props.eventObjectChosen.municipality.description}</Text>
                                            </View>:null}
                                    </View>
                                    {this.props.eventObjectChosen!=null?<View style={{flex:1,flexDirection:'row',alignItems:'stretch'}}>
                                        <View style={{paddingHorizontal:5,borderRightWidth:1, flex:1}}> 
                                            <Text>Start</Text>
                                            <Text>{this.props.eventObjectChosen.displayableFrom}</Text>
                                        </View>
                                        <View style={{paddingHorizontal:5,borderLeftWidth:1, flex:1}}>
                                            <Text>End</Text>
                                            <Text>{this.props.eventObjectChosen.displayableTo}</Text>
                                        </View>
                                    </View>:null}
                                </View>
                            </CardSection>
                        </Card>
                        <View>
                            <View>
                                <View style={{flexDirection:'row',justifyContent:'space-between',paddingHorizontal:40,paddingBottom:10}}>
                                    <Text style={{fontSize:22}}>Item</Text>
                                    <Text style={{fontSize:22,paddingRight:5}}>Price</Text>
                                </View>
                                <View style={{flexDirection:'column',paddingBottom:30}}>
                                    {this.renderItems()}
                                </View> 
                            </View>
                        </View>

                        <View>
                            <Input
                                placeholder="Input desired bargain amount"
                                value={this.props.bargainAmount}
                                keyboardType = 'numeric'
                                inputStyle={{textAlign:'right'}}
                                onChangeText={text => this.onChanged(text)}
                            />
                        </View>
                        <View>
                            <Text>This is to inform you that bargaining is subject for approval and price may change upon approval.</Text>
                        </View>
                        <View style={{padding:20}}>
                            <Button onPress={()=>this.validateSubmit()}>
                                Submit
                            </Button>
                        </View>
                    </View>
                </View>
                </KeyboardAwareScrollView>
        </View>  
        )
    }
}

const styles = {
    reviewText:{
        marginHorizontal: 2,marginBottom:5,fontSize:30,fontWeight:'bold'
    },
    starImage:{
        width: 30,
        height: 30,
    },
    header:{
        paddingLeft:25,
        paddingRight:25,
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
        backgroundColor:'#fafafa',
    }
}

const mapStateToProps = state => {
    return {
        bundle:state.booking.bundle,
        itemDetails:state.booking.itemDetails,
        eventList:state.booking.eventList,
        eventChosen:state.booking.eventChosen,
        eventObjectChosen:state.booking.eventObjectChosen,
        bargainAmount:state.booking.bargainAmount,
        bargainItems:state.booking.bargainItems,
        warningModal:state.booking.warningModal,
        showModalRemovalItem:state.booking.showModalRemovalItem,
        warningModalErrMsg:state.booking.warningModalErrMsg,
        removalItemErrMsg:state.booking.removalItemErrMsg,
        computedAmount:state.booking.computedAmount,
        bookingModal:state.booking.bookingModal,
        bookingModalErrMsg:state.booking.bookingModalErrMsg,
        id:state.idReducer.accountId,
        loading:state.booking.loading
    }
}
export default connect(mapStateToProps, actions)(StartBooking);
