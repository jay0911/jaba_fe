import React, { Component } from 'react'
import {Text,View,TouchableOpacity,Image, FlatList} from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Icon from 'react-native-vector-icons/Ionicons'
import {MAIN_SERVICE} from '../../api/backendserver'

class OrganizationInfo extends Component{

    renderContact(key){
        var newarr = this.props.org.contact.contactInformations.filter(x => x.key===key)  
        console.log(newarr.key) 
        
        if(newarr.length===1){
            return newarr[0].value
        }
        
        return newarr.length>0?newarr.reduce((total,item)=>total.value+', '+item.value):null   
    }

    renderSocial(key){
        var newarr = this.props.org.contact.socialLinks.filter(x => x.key===key)  
        console.log(newarr.key) 
        
        if(newarr.length===1){
            return newarr[0].value
        }
        
        return newarr.length>0?newarr.reduce((total,item)=>total.value+', '+item.value):null   
    }

    renderFacebook(){
        return (this.renderSocial('facebook')!=null?<View style={{paddingHorizontal:20, flexDirection:'row'}}>
            <View>
                <Icon name="arrow-back" style={styles.icon} />
            </View>
            <View>
                <Text style={{paddingLeft:20,fontSize:20}}>{this.renderSocial('facebook')}</Text>
            </View>
        </View>:null)
    }

    renderTwitter(){
        return (this.renderSocial('twitter')!=null?<View style={{paddingHorizontal:20, flexDirection:'row'}}>
            <View>
                <Icon name="arrow-back" style={styles.icon} />
            </View>
            <View>
                <Text style={{paddingLeft:20,fontSize:20}}>{this.renderSocial('twitter')}</Text>
            </View>
        </View>:null)
    }

    renderInstagram(){
        return (this.renderSocial('instagram')!=null?<View style={{paddingHorizontal:20, flexDirection:'row'}}>
            <View>
                <Icon name="arrow-back" style={styles.icon} />
            </View>
            <View>
                <Text style={{paddingLeft:20,fontSize:20}}>{this.renderSocial('instagram')}</Text>
            </View>
        </View>:null)
    }

    renderMobileNumbers(){
        return (this.renderContact('mobilenumber')!=null?<View style={{paddingHorizontal:20, flexDirection:'row'}}>
            <View>
                <Icon name="arrow-back" style={styles.icon} />
            </View>
            <View>
                <Text style={{paddingLeft:20,fontSize:20}}>{this.renderContact('mobilenumber')}</Text>
            </View>
        </View>:null)
    }

    renderPhones(){
        return(this.renderContact('phonenumber')!=null?<View style={{paddingHorizontal:20, flexDirection:'row'}}>
            <View>
                <Icon name="arrow-back" style={styles.icon} />
            </View>
            <View>
                <Text style={{paddingLeft:20,fontSize:20}}>{this.renderContact('phonenumber')}</Text>
            </View>
        </View>:null)
    }

    renderEmails(){
        return(this.renderContact('email')!=null?<View style={{paddingHorizontal:20, flexDirection:'row'}}>
            <View>
                <Icon name="arrow-back" style={styles.icon} />
            </View>
            <View>
                <Text style={{paddingLeft:20,fontSize:20}}>{this.renderContact('email')}</Text>
            </View>
        </View>:null)
    }

    render(){
        return (
                <View>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                            <Icon name="arrow-back" style={styles.icon}  />
                        </TouchableOpacity>
                    </View>
                        <View style={{padding:20}}>
                            <Text style={styles.orgName}>Contact Supplier</Text>
                        </View>
                    <View style={{flexDirection:'column',paddingBottom:20,borderBottomColor:'#dbdbdb',borderBottomWidth:1,marginHorizontal:20}}>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>   
                            <View style={{paddingLeft:20,paddingTop:20,paddingBottom:20}}>
                                <Text style={styles.orgName}>
                                    {this.props.org.name}
                                </Text>
                                <Text>
                                    Joined in {this.props.org.dtimeCreated}
                                </Text>
                            </View>
                            <View style={{paddingRight:30}}>
                                <Image 
                                style={styles.thumbnailStyle}
                                resizeMode='contain'
                                source={{ uri: `${MAIN_SERVICE}/image/bucket-image/${this.props.org.logoId}`}} /> 
                            </View>
                        </View> 
                    </View> 

                    <View style={{paddingBottom:20,borderBottomColor:'#dbdbdb',borderBottomWidth:1,marginHorizontal:20}} >
                        <View style={{paddingVertical:20}}>
                                <Text style={styles.orgName}>Talk with Us</Text>
                        </View>

                        {this.renderMobileNumbers()}

                        {this.renderPhones()}
                
                        {this.renderEmails()}
                    </View>


                    {this.props.org.contact.socialLinks!=null&&this.props.org.contact.socialLinks.length>0?<View style={{paddingBottom:20,borderBottomColor:'#dbdbdb',borderBottomWidth:1,marginHorizontal:20}} >
                        <View style={{paddingVertical:20}}>
                                <Text style={styles.orgName}>Social Us</Text>
                        </View>
 
                        {this.renderFacebook()}

                        {this.renderTwitter()}
        
                        {this.renderInstagram()}
                    </View>:null}
                </View>  
        )
    }
}

const styles = {
    reviewText:{
        marginHorizontal: 2,marginBottom:5,fontSize:30,fontWeight:'bold'
    },
    starImage:{
        width: 30,
        height: 30,
    },
    thumbnailStyle:{
        height:90,
        width:100
    },
    header:{
        paddingLeft:25,
        paddingRight:25,
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
        backgroundColor:'#fafafa',
    },
    orgName:{
        fontWeight:'bold',
        fontSize:30
    },
}

const mapStateToProps = state => {
    return {
        org:state.productDetailItem.org
    }
}
export default connect(mapStateToProps, actions)(OrganizationInfo);
