import React, { Component } from 'react'
import {Text,View,TouchableOpacity,Image, FlatList} from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Icon from 'react-native-vector-icons/Ionicons'
import RatingItem from './RatingItem'

class RatingForm extends Component{

    renderReviewText(){
        return this.props.bundle.ratings===1?(
            <Text style={styles.reviewText}>review</Text>
        ):(<Text style={styles.reviewText}>reviews</Text>)
    }

    renderRating(ratings){
        console.log('ratings')
        console.log(ratings)
        return ratings.length>0?(
            Math.round((ratings.reduce((prev, current) => {
                return prev + + current.rating
            },0)/ratings.length)* 100)/100+ '('+ratings.length+') '
        ):null
    }

    render(){
        return (


                <View>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                            <Icon name="arrow-back" style={styles.icon}  />
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection:'row',paddingLeft:10}}>
                        <View style={{padding:6}}>
                            <Image style={styles.starImage} source={require('../../assets/star.png')} />
                        </View>
                        <View style={{flex:1,flexDirection:'row'}} >
                            <View>
                                <Text style={styles.reviewText}>{this.renderRating(this.props.bundle.ratings)}</Text>               
                            </View>
                            <View>
                                {this.renderReviewText()}
                            </View>
                        </View>
                    </View>
                    <View style={{paddingBottom:300}}>
                        <FlatList
                                data={this.props.bundle.ratings}
                                keyExtractor={(item, index) => {
                                    return 'key'+index
                                }}
                                renderItem={({item})=>{
                                    return (
                                        <RatingItem result={item}/>
                                    )
                                }}
                        />
                    </View>
                </View>  
        )
    }
}

const styles = {
    reviewText:{
        marginHorizontal: 2,marginBottom:5,fontSize:30,fontWeight:'bold'
    },
    starImage:{
        width: 30,
        height: 30,
    },
    header:{
        paddingLeft:25,
        paddingRight:25,
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
        backgroundColor:'#fafafa',
    }
}

const mapStateToProps = state => {
    console.log(state.productDetailItem)
    return {
        bundle:state.productDetailItem.bundle
    }
}
export default connect(mapStateToProps, actions)(RatingForm);
