import React,{Component} from 'react'
import {
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,Image, Dimensions
} from 'react-native';
import {Text,Input} from 'react-native-elements'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Icon from 'react-native-vector-icons/Ionicons'
import {Picker} from '@react-native-community/picker';
import DateTimePicker from '@react-native-community/datetimepicker';
import Loader from './../../commons/Loader'

class EditProfileInfo extends Component {

    save(){
        this.props.editMyProfile(this.props.profile,this.props.id,this.props.navigation,true);
    }

    renderMobileNumber(){
        if(this.props.profile.contact){
            if(this.props.profile.contact.contactInformations){
                var contact = this.props.profile.contact.contactInformations.find(function (obj) {  
                    return obj.key === 'mobilenumber';
                });
                return (
                    <View style={{paddingTop:10,paddingHorizontal:10}}>
                        <Input 
                        label="Mobile number"
                        placeholder="Mobile number"
                        value={contact.value}
                        onChangeText={text => this.props.profileEditContactChange(this.props.profile,text)}
                        autoCapitalize="none"
                        autoCorrect={false}
                        />
                    </View>
                )
            }
        }else{
            return (
                <View style={{paddingTop:10,paddingHorizontal:10}}>
                    <Input 
                    label="Mobile number"
                    placeholder="Mobile number"
                    value={''}
                    onChangeText={text => this.props.profileEditContactChange(this.props.profile,text)}
                    autoCapitalize="none"
                    autoCorrect={false}
                    />
                </View>
            )
        }
    }

    renderGender(){
        return(
            <View style={{paddingTop:10,paddingHorizontal:30}}> 
                <Text style={{fontWeight:'bold'}}>Gender:</Text>
                <Picker
                    style={{ height: 50, width: 250}}
                    mode="dropdown"
                    selectedValue={this.props.profile.gender}
                    onValueChange={(itemValue, itemIndex)=>{this.props.profileEditChange({prop:'gender',value:itemValue})}}> 
                    <Picker.Item label='Not specified' value='NOT_SPECIFIED' />
                    <Picker.Item label='Male' value='MALE' />
                    <Picker.Item label='Female' value='FEMALE' />
                    <Picker.Item label='Others' value='OTHERS' />
                </Picker>
            </View>
        )
    }

    render(){
        return(
            <View> 
                <View style={styles.header}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Profile')}>
                        <Icon name="arrow-back" style={styles.icon}  />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.save()}>
                        <Text style={{textDecorationLine:'underline'}}>Save</Text>
                    </TouchableOpacity>
                </View>
                <View style={{padding:10}}>
                    <Text style={{fontSize:30,fontWeight:'bold'}}>Edit personal info</Text>
                </View>
                <View style={{paddingTop:10,paddingHorizontal:10}}>
                    <Input 
                    label="First Name"
                    placeholder="First Name"
                    value={this.props.profile.firstName}
                    onChangeText={text => this.props.profileEditChange({prop:'firstName',value:text})}
                    autoCapitalize="none"
                    autoCorrect={false}
                    />
                </View>
                <View style={{paddingHorizontal:10}}>
                    <Input 
                    label="Last Name"
                    placeholder="Last Name"
                    value={this.props.profile.lastName}
                    onChangeText={text => this.props.profileEditChange({prop:'lastName',value:text})}
                    autoCapitalize="none"
                    autoCorrect={false}
                    />
                </View>
                {this.renderMobileNumber()}
                {this.renderGender()}
            </View>
        )
    }

}

const styles = {
    header:{
        paddingLeft:25,
        paddingRight:25,
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
        backgroundColor:'#fafafa',
    }
}

const mapStateToProps = state => {
    console.log(state.profileReducer.profileEditInfo)
    return {
        id:state.idReducer.accountId,
        profile:state.profileReducer.profileEditInfo
    }
}

export default connect(mapStateToProps, actions)(EditProfileInfo)
