import React,{Component} from 'react'
import {
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,Image, Dimensions
} from 'react-native';
import {Text} from 'react-native-elements'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Icon from 'react-native-vector-icons/Ionicons'
import CardSection from './../../commons/CardSection'
import * as ImagePicker from "react-native-image-picker"
import * as RNFS from 'react-native-fs';
import ModalCustom from '../../commons/ModalCustom'
import ButtonTransparent from '../../commons/ButtonTransparent'

import {MAIN_SERVICE} from '../../api/backendserver'

class ShowProfile extends Component {

    handleChoosePhoto = () => {
        let options = {
            storageOptions: {
              skipBackup: true,
              path: 'images',
            },
            maxWidth:500,
            maxHeight:500,
        };
        ImagePicker.launchImageLibrary(options, (response) => {
            console.log('Responsed = ', response);
      
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else {

                if(response.width <= 450 && response.height <=500){
                    RNFS.readFile(response.uri, 'base64').then(res => {
                        console.log(res)

                        var request = {  
                            "displayPhotoBase64": res,
                        };

                        this.props.editMyProfile(request,this.props.id,this.props.navigation,true);

                    })
                    .catch(err => {
                        console.log(err.message, err.code);
                    });
                }else{
                    this.controllErrorModal(true,'Must be 500 by 500 dimensions only');
                }
            }
          });
    }

    controllErrorModal(isOpening,message){
        this.props.showUploadProfilePhotoErrorModal(isOpening);
        this.props.showUploadProfilePhotoErrorModalMessage(message);
    }

    renderImage(){
        if(this.props.profileInfo!=null){
            if(this.props.profileInfo.displayPhotoBase64==null){
                console.log('this.props.profileInfo.displayPhotoBase64 == null')
                console.log(this.props.profileInfo)
                return(
                    <Image style={styles.image} source={{ uri: `${MAIN_SERVICE}/image/bucket-image/` }} style={styles.profileImg}/>
                )
            }else{
                console.log('this.props.profileInfo != null'+this.props.profileInfo.displayPhotoBase64)
                return(
                    <Image style={styles.image} source={{ uri: `${MAIN_SERVICE}/image/bucket-image/${this.props.profileInfo.displayPhotoBase64}` }} style={styles.profileImg}/>
                )
            }
        }else{
            console.log('this.props.profileInfo == null')
            return(
                <Image style={styles.image} source={{ uri: `${MAIN_SERVICE}/image/bucket-image/` }} style={styles.profileImg}/>
            )
        }
    }

    render(){
        return(
            <View> 

                <ModalCustom 
                    modalVisible={this.props.modalVisible}
                    errorMessage={this.props.modalErrorMessage}
                 >
                    <ButtonTransparent onPress={()=>this.controllErrorModal(false,'')}>Try again</ButtonTransparent>
                </ModalCustom>

                <View style={styles.header}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Profile')}>
                        <Icon name="arrow-back" style={styles.icon}  />
                    </TouchableOpacity>
                </View>


  

                <CardSection>
                    <TouchableWithoutFeedback onPress={()=>this.handleChoosePhoto()}>
                        <View style={{flex:1,paddingHorizontal:10,flexDirection:'row',justifyContent:'space-between'}}>
                            <View style={{flexDirection:'column',padding:20}}>
                                <Text  style={{fontSize:20,fontWeight:'bold'}}>Hi, I'm {this.props.profileInfo.firstName}</Text>
                                <Text>Joined in {this.props.profileInfo.joinDate.substring(0, 4)}</Text>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                {this.renderImage()}
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </CardSection>
            </View>
        )
    }

}

const styles = {
    header:{
        paddingLeft:25,
        paddingRight:25,
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
        backgroundColor:'#fafafa',
    },
    profileImg: {
        height: 70,
        width: 70,
        margin:5,
        borderRadius: 100,
    },
}

const mapStateToProps = state => {
    return {
        id:state.idReducer.accountId,
        profileInfo:state.profileReducer.profileInfo,
        modalVisible:state.profileReducer.modalVisible,
        modalErrorMessage:state.profileReducer.modalErrorMessage,
    }
}



export default connect(mapStateToProps, actions)(ShowProfile)
