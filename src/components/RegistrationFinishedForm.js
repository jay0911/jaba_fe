import React, { Component } from 'react'
import {
    View
} from 'react-native';
import Spacer from '../commons/Spacer'
import {Text,Input} from 'react-native-elements'
import Button from '../commons/Button'
import LoadingButton from '../commons/LoadingButton'
import { connect } from 'react-redux'
import * as actions from '../actions'

class RegistrationFinishedForm extends Component{

    renderError(){
        if(this.props.err){
            return (
                <View>
                    <Text style={styles.errTextStyle}>
                        {this.props.err}
                    </Text>
                </View>
            )
        }
    }

    renderButton(){
        if(this.props.loading){
            return <LoadingButton />
        }

        return (
            <Button onPress={()=>this.props.registerAcc(this.props.user,this.props.navigation,'mainFlow')}>
                Submit
            </Button>
        )
    }

    render(){
        return(
            <View> 

                <Spacer>
                <Text style={{textAlign:'center'}} h3>Submit Application</Text>
                </Spacer>

                <Input 
                placeholder="First Name"
                value={this.props.user.firstname}
                onChangeText={text => this.props.profileChange({prop:'firstname',value:text})}
                autoCapitalize="none"
                autoCorrect={false}
                />
                <Input 
                placeholder="Last Name"
                value={this.props.user.lastname}
                onChangeText={text => this.props.profileChange({prop:'lastname',value:text})}
                autoCapitalize="none"
                autoCorrect={false}
                />
                <Input 
                secureTextEntry
                placeholder="Password"
                value={this.props.user.password}
                onChangeText={text => this.props.profileChange({prop:'password',value:text})}
                autoCapitalize="none"
                autoCorrect={false} 
                />

                {this.renderError()}
                {this.renderButton()}
        </View>
        )
    }
}

const styles = {
    errTextStyle:{
        color:'red',
        paddingBottom: 15,
        paddingLeft:10,
        fontSize:18
    }
}

const mapStateToProps = state => {
    const {firstname,lastname,password} = state.auth.user;
    return {
        user:{firstname:firstname,lastname:lastname,password:password,email:state.auth.email},
        loading:state.auth.loading,
        err:state.auth.errorMessageConfirm
    }
}

export default connect(mapStateToProps, actions)(RegistrationFinishedForm)