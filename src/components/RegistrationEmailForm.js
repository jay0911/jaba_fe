import React, { Component } from 'react'
import {
    View
} from 'react-native';
import Spacer from '../commons/Spacer'
import {Text,Input} from 'react-native-elements'
import Button from '../commons/Button'
import LoadingButton from '../commons/LoadingButton'
import { connect } from 'react-redux'
import * as actions from '../actions'
import Loader from '../commons/Loader'

class RegistrationEmailForm extends Component{
    onEmailChange(text){
        this.props.emailChanged(text);
    }

    componentDidMount(){
        this.props.reset();
    }

    renderError(){
        if(this.props.err){
            return (
                <View>
                    <Text style={styles.errTextStyle}>
                        {this.props.err}
                    </Text>
                </View>
            )
        }
    }

    renderButton(){
        if(this.props.loading){
            return <LoadingButton />
        }

        return (
            <Button onPress={()=>this.props.sendEmailAction(this.props.email,this.props.navigation,'EmailConfirm')}>
                Next
            </Button>
        )
    }

    render(){
        return(
        <View>
            <Loader loading={this.props.loading} />
            <Spacer>
                <Text style={{textAlign:'center'}} h3>Enter your Email</Text>
            </Spacer>
    
            <Input 
                placeholder="user@mail.com"
                onChangeText={this.onEmailChange.bind(this)}
                value={this.props.email}
                autoCapitalize="none"
                autoCorrect={false}
            />
            {this.renderError()}
            {this.renderButton()}

        </View>
        )
    }
}

const styles = {
    errTextStyle:{
        color:'red',
        paddingBottom: 15,
        paddingLeft:10,
        fontSize:18
    }
}

const mapStateToProps = state => {
    
    return {
        email:state.auth.email,
        loading:state.auth.loading,
        err:state.auth.errorMessageEmail
    }
}

export default connect(mapStateToProps, actions)(RegistrationEmailForm)