import React, { Component } from 'react'
import {View,TouchableWithoutFeedback} from 'react-native'
import Button from '../../commons/Button'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import EventCard from '../../components/events/EventCard'
import { ScrollView } from 'react-native-gesture-handler'
import {PLANNING,HAPPENING,PAST,CANCELLED} from '../../constants/EventStatusConstants'
import Loader from '../../commons/Loader'

class PlanningForm extends Component{
    
    componentDidMount(){
       
    }

    loadNextPage(){
        this.props.loadEventTypes()
        this.props.loadProvince(this.props.navigation,'CreateEvent')
    }

    loadEventDetail(eventdetail){
        this.props.loadEventDetail(eventdetail,this.props.navigation,'EventDetailScreen',false)
    }

    renderEventList(data){
        if (data && data.length) {
            return data.map(item => 
                <TouchableWithoutFeedback key={item.eventId} onPress={()=>this.loadEventDetail(item)}>
                    <View>         
                        <EventCard 
                            eventStartDtime={item.displayableFrom}
                            eventEndDtime={item.displayableTo}
                            eventName={item.eventName}
                            province={item.province.description}
                            municipality={item.municipality.description}
                            barangay={item.barangay.description}
                            eventType={item.eventType.description}
                        />     
                    </View>
                </TouchableWithoutFeedback>
            )
        }
    }

    render(){
        return (
                <View style={{padding:20}}>
                
                    <ScrollView>
                    <View>
                        {this.renderEventList(this.props.eventlist.planning)}
                    </View>
                    <Button onPress={()=>this.loadNextPage()}>
                            Create Event now
                    </Button>
                    </ScrollView>
                </View>  
        )
    }
}

const styles = {
}

const mapStateToProps = state => {
    return {
        eventlist:state.eventList,
        id:state.idReducer.accountId,
        eventPage:state.mainTabReducer.eventPage
    }
}
export default connect(mapStateToProps, actions)(PlanningForm);

