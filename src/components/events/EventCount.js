import React, { Component } from 'react'
import {View,Text} from 'react-native'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import IconBadge from 'react-native-icon-badge';

import {PLANNING,HAPPENING,PAST,CANCELLED} from '../../constants/EventStatusConstants'


class EventCount extends Component{

    renderTextElement(){
      switch(this.props.routeTitle){
        case PLANNING.displayName:
            return <Text style={{color:'#FFFFFF'}}>{this.props.planningCount.length}</Text>
        case HAPPENING.displayName:
            return <Text style={{color:'#FFFFFF'}}>{this.props.happeningCount.length}</Text>
        case PAST.displayName:
            return <Text style={{color:'#FFFFFF'}}>{this.props.pastCount.length}</Text>
        case CANCELLED.displayName:
            return <Text style={{color:'#FFFFFF'}}>{this.props.cancelledCount.length}</Text>
      }
    }

    isBadgeShowable(){
      switch(this.props.routeTitle){
        case PLANNING.displayName:
            return this.props.planningCount.length
        case HAPPENING.displayName:
            return this.props.happeningCount.length
        case PAST.displayName:
            return this.props.pastCount.length
        case CANCELLED.displayName:
            return this.props.cancelledCount.length
      }
    }

    render(){
        return (
          <View style={{flexDirection: 'row',alignItems: 'center',justifyContent: 'center',}}>
            
            <IconBadge
              MainElement={
                <Text style={{ color:'black', marginVertical:5,marginHorizontal:10 }}>
                  {this.props.routeTitle+'   '}
                </Text>
              }
              BadgeElement={
                <Text style={{color:'#FFFFFF'}}>{this.renderTextElement()}</Text>
              }
              IconBadgeStyle={
                {width:10,
                height:20,
                marginBottom:20,
                backgroundColor: '#ff0000'}
              }
              Hidden={this.isBadgeShowable()==0}
              />
          </View>
        )
    }
}

const styles = {
    header:{
        paddingLeft:25,
        paddingRight:25,
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
        backgroundColor:'#fafafa',
    }
}

const mapStateToProps = state => {
    return {
        planningCount:state.eventList.planning,
        cancelledCount:state.eventList.cancelled,
        pastCount:state.eventList.past,
        happeningCount:state.eventList.happening,
    }
}
export default connect(mapStateToProps, actions)(EventCount);

