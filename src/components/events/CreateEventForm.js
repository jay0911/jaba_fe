import React, { Component } from 'react'
import {View,TouchableOpacity,Modal} from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Icon from 'react-native-vector-icons/Ionicons'
import {Input,Text} from 'react-native-elements'
import {Picker} from '@react-native-community/picker';
import DateTimePicker from '@react-native-community/datetimepicker';
import { format } from "date-fns";
import ModalCustom from '../../commons/ModalCustom'
import ButtonTransparent from '../../commons/ButtonTransparent'
import Loader from '../../commons/Loader'

class CreateEventForm extends Component{

    onAccept() {
        this.props.resetEvent();
        this.setState({dateStart: new Date(),dateEnd: new Date()});
    }

    state = {
        dateStart: new Date(),
        dateEnd: new Date(),
        mode:'date',
        showStart:false,
        showEnd:false
    }

    componentDidMount(){
        this.props.refreshEvent();
        this.props.setEventAccountId(this.props.id);    
    }

    selectProvince(itemValue){
        this.props.eventPropsChange({prop:'province',value:itemValue})
        this.props.loadMunicipality(itemValue)
    }

    selectMunicipality(itemValue){
        this.props.loadBarangay(itemValue)
        this.props.eventPropsChange({prop:'municipality',value:itemValue})
    }

    selectBarangay(itemValue){
        this.props.eventPropsChange({prop:'barangay',value:itemValue})
    }

    selectEventType(itemValue){
        this.props.eventPropsChange({prop:'eventType',value:itemValue})
    }

    onChangeStart(event,selectedDate){
        const currentDate = selectedDate || this.state.dateStart;
        console.log(currentDate+'here')
        this.setState({showStart:false,dateStart:currentDate})
    }

    onChangeEnd(event,selectedDate){
        const currentDate = selectedDate || this.state.dateEnd;
        console.log(currentDate+'here')
        this.setState({showEnd:false,dateEnd:currentDate})
    }

    render(){
        return (
            <View>
                <Loader loading={this.props.events.loading} />
                <ModalCustom 
                    modalVisible={this.props.events.modalVisible}
                    errorMessage={this.props.events.errorMessage}
                 >
                        <ButtonTransparent onPress={()=>this.onAccept()}>Try again</ButtonTransparent>
                </ModalCustom>
                <View style={styles.header}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Events')}>
                        <Icon name="close" style={styles.icon}  />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.props.saveMyEvent(this.state.dateStart,this.state.dateEnd,this.props.events,this.props.navigation,'Events')}>
                        <Text style={{fontSize:20,textDecorationLine: 'underline'}}>Save</Text>
                    </TouchableOpacity>
                </View>
            <View >
            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>


                <View style={{paddingHorizontal:20,paddingTop:20}}>
                    <Text style={{fontWeight:'bold',fontSize:30,paddingBottom:10}}>Create Event</Text>
                    <Input 
                        placeholder="Event Name"
                        value={this.props.eventName}
                        onChangeText={eventName => this.props.eventPropsChange({prop:'eventName',value:eventName})}
                        autoCapitalize="none"
                        autoCorrect={false}
                    />

                            <Text style={{fontWeight:'bold'}}>Event Type:</Text>
                            <Picker
                                style={{ height: 50, width: 250}}
                                mode="dropdown"
                                selectedValue={this.props.events.eventType}
                                onValueChange={(itemValue, itemIndex)=>{this.selectEventType(itemValue)}}> 
                                <Picker.Item label='Not specified' value='' />
                                {this.props.events.eventTypeList.map((item, index) => {
                                    return (<Picker.Item label={item.type} value={item.id} key={index}/>) 
                                })}
                            </Picker>
                </View>

                <View style={{paddingHorizontal:20}}>
                    <Text style={{fontWeight:'bold',fontSize:30,paddingBottom:10}}>Location</Text>
                    <View
                        style={{
                        flexDirection: "column",
                        alignItems: "flex-start",
                        }}
                    >
                        <Text style={{fontWeight:'bold'}}>Province:</Text>
                            <Picker
                                style={{ height: 50, width: 250}}
                                mode="dropdown"
                                selectedValue={this.props.events.province}
                                onValueChange={(itemValue, itemIndex)=>{this.selectProvince(itemValue)}}> 
                                <Picker.Item label='Not specified' value='' />
                                {this.props.events.addressList.province.map((item, index) => {
                                    return (<Picker.Item label={item.description} value={item.code} key={index}/>) 
                                })}
                            </Picker>
                    </View>
                    <View
                        style={{
                        flexDirection: "column",
                        alignItems: "flex-start",
                        }}
                    >
                            <Text style={{fontWeight:'bold'}}>Municipality:</Text>
                            <Picker
                                style={{ height: 50, width: 250}}
                                mode="dropdown"
                                selectedValue={this.props.events.municipality}
                                onValueChange={(itemValue, itemIndex)=>{this.selectMunicipality(itemValue)}}> 
                                <Picker.Item label='Not specified' value='' />
                                {this.props.events.addressList.municipality.map((item, index) => {
                                    return (<Picker.Item label={item.description} value={item.code} key={index}/>) 
                                })}
                            </Picker>
                    </View>
                    <View
                        style={{
                        flexDirection: "column",
                        alignItems: "flex-start",
                        }}
                    >
                            <Text style={{fontWeight:'bold'}}>Barangay:</Text>
                            <Picker
                                style={{ height: 50, width: 250}}
                                mode="dropdown"
                                selectedValue={this.props.events.barangay}
                                onValueChange={(itemValue, itemIndex)=>{this.selectBarangay(itemValue)}}> 
                                <Picker.Item label='Not specified' value='' />
                                {this.props.events.addressList.barangay.map((item, index) => {
                                    return (<Picker.Item label={item.description} value={item.code} key={index}/>) 
                                })}
                            </Picker>
                    </View>
                    <Input 
                        placeholder="Address/Landmarks"
                        value={this.props.otherAddress}
                        onChangeText={otherAddress => this.props.eventPropsChange({prop:'otherAddress',value:otherAddress})}
                        autoCapitalize="none"
                        autoCorrect={false}
                    />
                </View>

                <View style={{paddingHorizontal:20 ,paddingBottom:200}}>
                    <Text style={{fontWeight:'bold',fontSize:30,paddingBottom:10}}>Date</Text>

                    <View style={{flex:1,flexDirection:'row',paddingVertical:10}}>
                        <View style={{flex:1}}>
                            <Text style={{fontWeight:'bold'}}>Start:</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{fontWeight:'bold'}}>End:</Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'column', paddingBottom:20}}>
                        <View style={{flex:1,flexDirection:'row'}}>
                            <View style={{flex:1,flexDirection:'column',height:50}}>
                                <View style={{paddingHorizontal:5,borderWidth:1}}>
                                    <TouchableOpacity onPress={()=>this.setState({showStart:true,mode:'date'})}>
                                        <Text style={{fontSize:17,padding:10}}>{format(this.state.dateStart, "MMMM dd, yyyy")}</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{paddingHorizontal:5,borderWidth:1}}>
                                    <TouchableOpacity onPress={()=>this.setState({showStart:true,mode:'time'})}>
                                        <Text style={{fontSize:17,padding:10}}>{format(this.state.dateStart, "hh:mm a")}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{flex:1,flexDirection:'column',height:50,paddingLeft:10}}>
                               <View style={{paddingHorizontal:5,borderWidth:1}}>
                                    <TouchableOpacity onPress={()=>this.setState({showEnd:true,mode:'date'})}>
                                        <Text style={{fontSize:17,padding:10}}>{format(this.state.dateEnd, "MMMM dd, yyyy")}</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{paddingHorizontal:5,borderWidth:1}}>
                                    <TouchableOpacity onPress={()=>this.setState({showEnd:true,mode:'time'})}>
                                        <Text style={{fontSize:17,padding:10}}>{format(this.state.dateEnd, "hh:mm a")}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>

                    <View>
                        {this.state.showStart && (
                            <DateTimePicker
                            testID="dateTimePicker1"
                            value={this.state.dateStart}
                            mode={this.state.mode}
                            is24Hour={false}
                            display="default"
                            onChange={(event,selectedDate)=>this.onChangeStart(event,selectedDate)}
                            />
                        )}
                    </View> 

                    <View>
                        {this.state.showEnd && (
                            <DateTimePicker
                            testID="dateTimePicker2"
                            value={this.state.dateEnd}
                            mode={this.state.mode}
                            is24Hour={false}
                            display="default"
                            onChange={(event,selectedDate)=>this.onChangeEnd(event,selectedDate)}
                            />
                        )}
                    </View>          
                </View>
            </ScrollView>

            </View>
            </View>
        )
    }
}

const styles = {
    header:{
        paddingLeft:25,
        paddingRight:25,
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
        backgroundColor:'#fafafa',
    },

}

const mapStateToProps = state => {
    console.log(state.events)
    return {
        events:state.events,
        id:state.idReducer.accountId
    }
}

export default connect(mapStateToProps, actions)(CreateEventForm)

