import React, { Component } from 'react'
import {View,TouchableOpacity,Text} from 'react-native'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Icon from 'react-native-vector-icons/Ionicons'
import Card from '../../commons/Card'
import CardSection from '../../commons/CardSection'
import Button from '../../commons/Button'
import ButtonTransparent from '../../commons/ButtonTransparent'
import ModalCustom from '../../commons/ModalCustom'

class EventDetail extends Component{

    cancelEvent(){
        this.props.updateYourEventStatus(this.props.detail.eventId,'CANCELLED',this.props.navigation,'Events')
    }

    renderCancelButton(){
        return this.props.showCancelButton?null:(
            <TouchableOpacity onPress={()=>this.props.openCancelEventModal()}>
                <Text style={{fontSize:20,textDecorationLine: 'underline'}}>Cancel Event</Text>
            </TouchableOpacity>
        )
    }

    renderBookingButton(){
        return this.props.showCancelButton?null:(
            <Button onPress={()=>this.props.navigation.navigate('Explore')}>
                Book products for this event
            </Button>
        )
    }

    render(){
        return (
                <View>
                    <ModalCustom 
                        modalVisible={this.props.modalVisible}
                        errorMessage={'Are you sure you want to cancel '+this.props.detail.eventName+'?'}
                    >
                        <ButtonTransparent onPress={()=>this.cancelEvent()}>Yes</ButtonTransparent>
                        <ButtonTransparent onPress={()=>this.props.closeCancelEventModal()}>No</ButtonTransparent>
                    </ModalCustom>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Events')}>
                            <Icon name="close" style={styles.icon}  />
                        </TouchableOpacity>
                        {this.renderCancelButton()}
                    </View>
                    <View style={{paddingHorizontal:20,paddingTop:20}}>
                        <Card>
                            <CardSection>
                                <View style={{padding:10}}>
                                    <View>
                                        <Text style={{fontSize:16}}>{this.props.detail.displayableFrom}</Text>
                                    </View>
                                    <View style={{flexDirection:'row'}}>
                                        <Text style={{fontSize:16,fontWeight:'bold'}}>{this.props.detail.province.description}, {this.props.detail.municipality.description}</Text>
                                    </View>
                                    <View style={{flexDirection:'row'}}>
                                        <Text style={{fontSize:16}}>({this.props.detail.barangay.description}) </Text>
                                        <Text style={{fontSize:16}}>{this.props.detail.otherAddress}</Text>
                                    </View>
                                </View>
                            </CardSection>
                        </Card>
                        <Card>
                            <CardSection>
                                <View style={{flex:1,padding:10}}>
                                    <View style={{paddingBottom:10}}>
                                        <Text style={{fontSize:30,fontWeight:'bold'}}>{this.props.detail.eventName}</Text>
                                    </View>
                                    <View style={{paddingBottom:10}}>
                                        <Text style={{fontSize:20}}>{this.props.detail.eventType.description}</Text>
                                    </View>
                                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                        <View style={{flex:1}}>
                                            <Text>Start</Text>
                                            <View style={{paddingHorizontal:20,paddingVertical:5}}>
                                                <Text>{this.props.detail.displayableFrom}</Text>
                                            </View>
                                        </View>
                                        <View style={{flex:1}}>
                                            <Text>End</Text>
                                            <View style={{paddingHorizontal:20,paddingVertical:5}}>
                                                <Text>{this.props.detail.displayableTo}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </CardSection>
                        </Card>

                        {this.renderBookingButton()}
                    </View>
                </View>  
        )
    }
}

const styles = {
    header:{
        paddingLeft:25,
        paddingRight:25,
        paddingTop:10,
        paddingBottom:10,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor: '#dbdbdb',
        borderBottomWidth: 2,
        backgroundColor:'#fafafa',
    }
}

const mapStateToProps = state => {
    console.log(state.eventList.isCancelled+'@@@@@@@@@@@@@@')
    return {
        detail:state.eventList.eventDetail,
        modalVisible:state.eventList.modalVisible,
        showCancelButton:state.eventList.isCancelled
    }
}
export default connect(mapStateToProps, actions)(EventDetail);

