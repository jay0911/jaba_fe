import React, { Component } from 'react'
import {View,TouchableWithoutFeedback} from 'react-native'
import Button from '../../commons/Button'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import AsyncStorage from '@react-native-community/async-storage'
import EventCard from '../../components/events/EventCard'
import { ScrollView } from 'react-native-gesture-handler'
import {PLANNING,HAPPENING,PAST,CANCELLED} from '../../constants/EventStatusConstants'
import Loader from '../../commons/Loader'

class PastForm extends Component{

    componentDidMount(){
      
    }
    

    loadEventDetail(eventdetail){
        this.props.loadEventDetail(eventdetail,this.props.navigation,'EventDetailScreen',true)
    }

    renderEventList(data){
        if (data && data.length) {
            return data.map(item => 
                <TouchableWithoutFeedback key={item.eventId} onPress={()=>this.loadEventDetail(item)}>
                    <View>         
                        <EventCard 
                            eventStartDtime={item.displayableFrom}
                            eventEndDtime={item.displayableTo}
                            eventName={item.eventName}
                            province={item.province.description}
                            municipality={item.municipality.description}
                            barangay={item.barangay.description}
                            eventType={item.eventType.description}
                        />     
                    </View>
                </TouchableWithoutFeedback>
            )
        }
    }

    render(){
        return (
                <View style={{padding:20}}>
                    <ScrollView>
                    <View>
                        {this.renderEventList(this.props.eventlist.past)}
                    </View>
                    </ScrollView>
                </View>  
        )
    }
}

const styles = {
}

const mapStateToProps = state => {
    return {
        eventlist:state.eventList,
        id:state.idReducer.accountId,
        eventPage:state.mainTabReducer.eventPage
    }
}
export default connect(mapStateToProps, actions)(PastForm);

