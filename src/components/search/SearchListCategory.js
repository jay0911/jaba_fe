import React,{Component} from 'react'
import {
    View,
    TextInput,
    Text
  } from 'react-native';
import { connect } from 'react-redux'
import * as actions from '../../actions'
import { TouchableOpacity,FlatList } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons'
import {Picker} from '@react-native-community/picker';
import {Dimensions} from 'react-native' 
import CommonSearchHeaderFlow from '../../commons/CommonSearchHeaderFlow'

class SearchListCategory extends Component {

    back(){
        this.props.navigation.navigate('mainFlow')
    }

    selectCategory(code,name){
        this.props.genericSearchChange({prop:'selectedCategory',value:{code:code,name:name}})
    }

    next(){
        if(this.props.selectedCategory.code === ''){
            this.props.navigation.navigate('SearchListSubCategoryScreen')
            this.props.getSubCategoryListing();
        }else{
            this.props.navigation.navigate('SearchListSubCategoryScreen')
            this.props.getSubCategoryListFromCategory(this.props.selectedCategory.code.charAt(0))
        }
    }

    skip(){
        this.props.navigation.navigate('SearchListSubCategoryScreen')
        this.props.getSubCategoryListing();
        this.selectCategory('','')
    }

    async componentDidMount(){     
        this.props.getCategoryList();
    }

    render(){
        const WIDTH = Dimensions.get('window').width;
        const HEIGHT = Dimensions.get('window').height;
    
        return(
            <View>
                <View style={{padding:20}}>
                    <View style={{paddingTop:80}}>
                        <Text style={styles.header}>What category </Text>
                        <Text style={styles.header}>are you </Text>
                        <Text style={styles.header}>looking for?</Text>
                    </View>
                </View>
                <View style={{justifyContent:'space-between',borderWidth:1,
        borderColor:'#ddd',
        borderBottomWidth:0,
        shadowColor:'#000',
        shadowOpacity: 0.1,
        shadowRadius: 10,
        elevation: 3,height:HEIGHT-240}}>
                    <View>
                        <CommonSearchHeaderFlow onPress={()=>{
                            this.props.navigation.goBack()
                        }} children={[this.props.selectedLoc,this.props.selectedCategory.name]} />
                        <View>
                            <View
                                style={{
                                flexDirection: "row",
                                alignItems: "stretch",

                                }}
                            >
                                <Text style={{fontWeight:'bold',padding:15,paddingLeft:60}}>Category:</Text>
                                    <Picker
                                        style={{ height: 50, width: 250}}
                                        mode="dropdown"
                                        selectedValue={this.props.selectedCategory.code}
                                        onValueChange={(itemValue, itemIndex)=>{
                                                if(itemIndex!=0){
                                                    this.selectCategory(itemValue,this.props.categories[itemIndex-1].name)
                                                }else{
                                                    this.selectCategory('','')
                                                }
                                            }}> 
                                        <Picker.Item label='Not specified' value='' />
                                        {this.props.categories.map((item, index) => {
                                            return (<Picker.Item label={item.name} value={item.code} key={index}/>) 
                                        })}
                                    </Picker>
                            </View>
                        </View>
                    </View>
                    <View>
                        <View style={{flexDirection:'row',alignContent:'space-between',borderTopWidth:1,borderColor:'gray'}}>
                            <View style={{padding:20,flex:1}}>
                                <TouchableOpacity onPress={()=>this.skip()}>
                                    <Text style={{textDecorationLine: 'underline',fontSize:25,marginVertical:5,marginHorizontal:15}}>Skip</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{padding:20}}>
                                <TouchableOpacity onPress={()=>this.next() } style={{borderWidth:1,borderRadius:5,backgroundColor:'gray'}}>
                                    <Text style={{textDecorationLine: 'underline',fontSize:25,marginVertical:5,marginHorizontal:25,color:'white'}}>Next</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

}

const styles = {
    header:{
        fontWeight:'bold',
        fontSize:30,
        color:'gray'
    },
}

const mapStateToProps = state => {
    return {
        searchLocation:state.searchReducer.searchLocation,
        searchAddressProductList:state.searchReducer.searchAddressProductList,
        selectedLoc:state.searchReducer.selectedLoc,
        selectedCategory:state.searchReducer.selectedCategory,
        categories:state.searchReducer.categories
    }
}


export default connect(mapStateToProps, actions)(SearchListCategory)
