import React,{Component,useCallback} from 'react'
import {
    View,
    TextInput,
    Text
  } from 'react-native';
import { connect } from 'react-redux'
import * as actions from '../../actions'
import { TouchableOpacity,FlatList } from 'react-native-gesture-handler';
import CommonSearchHeaderFlow from '../../commons/CommonSearchHeaderFlow'
import Slider from 'rn-range-slider';
import {Dimensions} from 'react-native' 

import Thumb from '../../commons/slider/Thumb';
import Rail from '../../commons/slider/Rail';
import RailSelected from '../../commons/slider/RailSelected';
import Notch from '../../commons/slider/Notch';
import Label from '../../commons/slider/Label';
import {loadProductv2} from '../../helpers/ProductSearchHelper'

class SearchListPriceRange extends Component {

    back(){
        this.props.navigation.navigate('mainFlow')
    }

    setPrice(high,low){
        this.props.genericSearchChange({prop:'high',value:high})
        this.props.genericSearchChange({prop:'low',value:low})
    }

    skip(){
        //todo
    }

    initValuesAfterSearch(){
        this.props.genericSearchChange({prop:'selectedLoc',value:''})
        this.props.genericSearchChange({prop:'selectedCategory',value:{code:'',name:''}})
        this.props.genericSearchChange({prop:'selectedSubCategory',value:{code:'',name:''}})
        this.setPrice(this.props.maxPriceRange,0)
    }

    search(){

        var searchVal = {
            "addressIndex.address": this.props.selectedLoc,
            "category.id": this.props.selectedCategory.code,
            "items.category.id": this.props.selectedSubCategory.code 
        }

        var matchesVal = "";
        var i = 0;
        for (var key in searchVal) {

            if(searchVal[key]===''){
                continue;
            }

            matchesVal = matchesVal+`,{"key": "${key}","value": "${searchVal[key]}"}`

            i = i+1;
        }

        var rangePrice = `,"ranges": [{"key": "price","value": {"from": "${this.props.low}", "to": ${this.props.high}}}]`

        var searchString = `{"filterBy": {"matches": [{ "key": "isActive","value": true} ${matchesVal}]${rangePrice}},"pagination": {"from": 0,"size": 10},"sortBy": {"fieldsAscOrDesc": {"dtimeCreated": "desc"}} }}`


        this.props.genericSearchChange({prop:'productMatches',value:matchesVal})
        this.props.genericSearchChange({prop:'productRanges',value:rangePrice})


        this.props.initfindProduct();

        this.initValuesAfterSearch()
        loadProductv2(this.props.findProductInit,JSON.stringify(searchString),[],this.props.myid)
        this.props.navigation.navigate('Explore')
    }
    
    renderSearchButton(){
        return (
            <TouchableOpacity onPress={()=>this.search() } style={{borderWidth:1,borderRadius:5,backgroundColor:'gray'}}>
                <Text style={{textDecorationLine: 'underline',fontSize:25,marginVertical:5,marginHorizontal:25,color:'white'}}>Search</Text>
            </TouchableOpacity>
        )
    }

    render(){

        const WIDTH = Dimensions.get('window').width;
        const HEIGHT = Dimensions.get('window').height;
    
        return(
            <View>
                <View style={{padding:20}}>
                    <View style={{paddingTop:80}}>
                        <Text style={styles.header}>What price range</Text>
                        <Text style={styles.header}>are you </Text>
                        <Text style={styles.header}>looking for?</Text>
                    </View>
                </View>
                <View style={{justifyContent:'space-between',borderWidth:1,
        borderColor:'#ddd',
        borderBottomWidth:0,
        shadowColor:'#000',
        shadowOpacity: 0.1,
        shadowRadius: 10,
        elevation: 3,height:HEIGHT-240}}>
                    <View>
                        <CommonSearchHeaderFlow onPress={()=>{
                            this.props.navigation.goBack()
                        }} children={[this.props.selectedLoc,this.props.selectedCategory.name,this.props.selectedSubCategory.name, 'price between Php '+this.props.low+' - Php '+this.props.high]} />
                        <View>
                            <Slider
                                min={0}
                                max={this.props.maxPriceRange}
                                step={1}
                                renderThumb={() => <Thumb/>}
                                renderRail={() => <Rail/>}
                                renderRailSelected={() => <RailSelected/>}
                                renderLabel={value => <Label text={value}/>}
                                renderNotch={() => <Notch/>}
                                onValueChanged={(low, high, fromUser) => {
                                    this.setPrice(high,low)
                                }}
                            />
                        </View>
                    </View>
                    <View>
                        <View style={{flexDirection:'row',alignContent:'space-between',borderTopWidth:1,borderColor:'gray'}}>
                            <View style={{padding:20,flex:1}}>
                                <TouchableOpacity onPress={()=>this.skip()}>
                                    <Text style={{textDecorationLine: 'underline',fontSize:25,marginVertical:5,marginHorizontal:15}}>Skip</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{padding:20}}>
                                {this.renderSearchButton()}
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

}

const styles = {
    header:{
        fontWeight:'bold',
        fontSize:30,
        color:'gray'
    },
}

const mapStateToProps = state => {
    return {
        searchLocation:state.searchReducer.searchLocation,
        searchAddressProductList:state.searchReducer.searchAddressProductList,
        selectedLoc:state.searchReducer.selectedLoc,
        selectedCategory:state.searchReducer.selectedCategory,
        selectedSubCategory:state.searchReducer.selectedSubCategory,
        subcategories:state.searchReducer.subcategories,
        high:state.searchReducer.high,
        low:state.searchReducer.low,
        maxPriceRange:state.searchReducer.maxPriceRange,
        myid:state.idReducer.accountId
    }
}


export default connect(mapStateToProps, actions)(SearchListPriceRange)
