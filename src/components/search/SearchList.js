import React,{Component} from 'react'
import {
    View,
    TextInput,
    Text
  } from 'react-native';
import { connect } from 'react-redux'
import * as actions from '../../actions'
import { TouchableOpacity,FlatList } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import { isThisQuarter } from 'date-fns';

class SearchList extends Component {

    state = {
        timer: null,
    };


    back(){
        this.props.resetSearch()
        this.props.navigation.navigate('mainFlow')
    }

    render(){
        return(
            <View>
                <View style={{flexDirection:'row'}}>
                    <View style={{padding:20}}>
                        <TouchableOpacity onPress={()=>this.back()}>
                            <Icon name="ios-arrow-back" />
                        </TouchableOpacity>
                    </View>
                    <View style={{paddingHorizontal:5,flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'stretch'}}>
                        <TextInput
                            ref="textInput"
                            autoFocus 
                            placeholder='Where will be the booking place'
                            style={{ height: 40 }}
                            onChangeText={text => this.invokeSearch(text)}
                            value={this.props.searchLocation}
                    />                 
                    </View>
                    {this.renderRemoveText()}
                </View>

                <FlatList
                            data={this.props.searchAddressProductList}
                            keyExtractor={(item, index) => {
                                return 'key'+index
                            }}
                            renderItem={({item})=>{
                                return (
                                    <TouchableOpacity onPress={()=>this.goToCategorySearchPage(item.address)}>
                                        <View style={{flexDirection:'row',borderBottomColor:'gray',borderBottomWidth:1,borderBottomEndRadius:30,borderBottomStartRadius:30}}>
                                            
                                                <View style={{padding:20}}>
                                                    <Icon name="ios-pin" />
                                                </View>
                                                <View style={{paddingHorizontal:5,flex: 1,
                                                    flexDirection: 'column',
                                                    justifyContent: 'center',
                                                    alignItems: 'stretch'}}>
                                                    <Text>{item.address}</Text>
                                                </View>
                                            
                                        </View>
                                    </TouchableOpacity>
                                )
                            }}
                />
            </View>
        )
    }

    goToCategorySearchPage(loc){
        this.props.navigation.navigate('SearchListScreenCategory')
        this.props.genericSearchChange({prop:'selectedLoc',value:loc})
    }

    renderRemoveText(){
        if(this.props.searchLocation != ''){
            return (<View style={{padding:20}}>
                <TouchableOpacity onPress={()=>this.invokeSearch('')}>
                    <Icon name="close-circle" />
                </TouchableOpacity>
            </View>)  
        }else{
            return null;
        }
    }

    invokeSearch(searchText){
        console.log(searchText.length)
        this.props.genericSearchChange({prop:'searchLocation',value:searchText})
        if(searchText.length>=1){
            clearTimeout(this.state.timer);
            let timer = setTimeout(this.searchApi, 500);
            this.setState({timer});
        }else{
            this.props.genericSearchChange({prop:'searchAddressProductList',value:[]})
        }

    }

    searchApi =() => {
        this.props.addressProductSearchApi(this.props.searchLocation)
    }

}

const mapStateToProps = state => {
    return {
        searchLocation:state.searchReducer.searchLocation,
        searchAddressProductList:state.searchReducer.searchAddressProductList
    }
}


export default connect(mapStateToProps, actions)(SearchList)
