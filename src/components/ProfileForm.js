import React,{Component} from 'react'
import {
  View,TouchableWithoutFeedback,Image, Dimensions
} from 'react-native';
import {Input,Text} from 'react-native-elements'
import { connect } from 'react-redux'
import * as actions from '../actions'
import CardSection from './../commons/CardSection'

const { width, height } = Dimensions.get('window')
import Icon from 'react-native-vector-icons/Ionicons'
import {MAIN_SERVICE} from '../api/backendserver'

class ProfileForm extends Component {

    renderToken(){
        return (
            <View>
                <Text style={styles.errTextStyle}>
                    {this.props.id}
                </Text>
            </View>
        )
    }

    renderImage(){
        if(this.props.profileInfo!=null){
            if(this.props.profileInfo.displayPhotoBase64==null){
                console.log('this.props.profileInfo.displayPhotoBase64 == null')
                console.log(this.props.profileInfo)
                return(
                    <Image style={styles.image} source={{ uri: `${MAIN_SERVICE}/image/bucket-image/` }} style={styles.profileImg}/>
                )
            }else{
                console.log('this.props.profileInfo != null'+this.props.profileInfo.displayPhotoBase64)
                return(
                    <Image style={styles.image} source={{ uri: `${MAIN_SERVICE}/image/bucket-image/${this.props.profileInfo.displayPhotoBase64}` }} style={styles.profileImg}/>
                )
            }
        }else{
            console.log('this.props.profileInfo == null')
            this.props.prefillMyProfile(this.props.id,this.props.navigation,false)
            return(
                <Image style={styles.image} source={{ uri: `${MAIN_SERVICE}/image/bucket-image/` }} style={styles.profileImg}/>
            )
        }
    }

    logout(){
        this.props.navigation.navigate('Login');
        this.props.resetIds();
        this.props.resetBooking();
        this.props.initBookingDetail();
        this.props.initEventList();
        this.props.initEvents();
        this.props.resetProfile();
    }

    editProfile(){
        this.props.prefillMyProfileEdit(this.props.id,this.props.navigation,true)
    }

    render(){
        return(
            <View>
                <CardSection>
                    <TouchableWithoutFeedback onPress={()=>this.props.navigation.navigate('ShowProfileScreen')}>
                        <View style={{flex:1,paddingHorizontal:10,flexDirection:'row'}}>
                            <View style={{flexDirection:'row'}}>
                                {this.renderImage()}
                            </View>
                            <View style={{flexDirection:'column',padding:20}}>
                                <Text  style={{fontSize:20,fontWeight:'bold'}}>Account settings</Text>
                                <Text>Show profile</Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </CardSection>

                <CardSection>
                    <View style={{flex:1,paddingHorizontal:10}}>
                        <Text>Account settings</Text>
                        <TouchableWithoutFeedback onPress={()=>this.editProfile()}>
                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>

                                    <Text style={{fontSize:20,padding:10}}>Personal information</Text>
                                    <Icon style={{fontSize:20,padding:10}} name="ios-person" />

                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </CardSection>
                <CardSection>
                    <View style={{flex:1,paddingHorizontal:10}}>
                    <TouchableWithoutFeedback onPress={()=>this.logout()}>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>

                                <Text style={{fontSize:20,padding:10}}>Transactions</Text>
                                <Icon style={{fontSize:20,padding:10}} name="receipt" />

                        </View>
                        </TouchableWithoutFeedback>
                    </View>
                </CardSection>
                <CardSection>
                    <View style={{flex:1,paddingHorizontal:10}}>
                    <TouchableWithoutFeedback onPress={()=>this.logout()}>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>

                                <Text style={{fontSize:20,padding:10}}>Log out</Text>
                                <Icon style={{fontSize:20,padding:10}} name="power" />

                        </View>
                        </TouchableWithoutFeedback>
                    </View>
                </CardSection>
            </View>
        )
    }
}

const styles = {
    errTextStyle:{
        color:'red',
        paddingBottom: 15,
        paddingLeft:10,
        fontSize:18
    },
    profileImg: {
        height: 70,
        width: 70,
        margin:5,
        borderRadius: 100,
    },
}

const mapStateToProps = state => {
    return {
        id:state.idReducer.accountId,
        profileInfo:state.profileReducer.profileInfo
    }
}


export default connect(mapStateToProps, actions)(ProfileForm)
