import React, { Component } from 'react'
import {
    View
} from 'react-native';
import Spacer from '../commons/Spacer'
import {Text,Input} from 'react-native-elements'
import { connect } from 'react-redux'
import * as actions from '../actions'
import Button from '../commons/Button'
import LoadingButton from '../commons/LoadingButton'
import Loader from '../commons/Loader'

class RegistrationConfirmForm extends Component{

    onCodeChange(text){
        this.props.codeChanged(text);
    }

    componentDidMount(){
        this.props.resetConfirm();
    }

    renderError(){
        if(this.props.err){
            return (
                <View>
                    <Text style={styles.errTextStyle}>
                        {this.props.err}
                    </Text>
                </View>
            )
        }
    }

    renderButton(){
        if(this.props.loading){
            return <LoadingButton />
        }

        return (
            <Button onPress={()=>this.props.verifyCodeEmail(this.props.email,this.props.code,this.props.navigation,'RegistrationFinished')}>
                Next
            </Button>
        )
    }

    render(){
        return(
            <View>
                <Loader loading={this.props.loading} />
                <Spacer>
                    <Text style={{textAlign:'center'}} h3>Enter Confirmation Code</Text>
                </Spacer>

                <Input 
                    placeholder="Confirmation Code"
                    value={this.props.code}
                    onChangeText={this.onCodeChange.bind(this)}
                    autoCapitalize="none"
                    autoCorrect={false}
                />
                {this.renderError()}
                {this.renderButton()}
            </View>
        )
    }
}

const styles = {
    errTextStyle:{
        color:'red',
        paddingBottom: 15,
        paddingLeft:10,
        fontSize:18
    }
}

const mapStateToProps = state => {
    return {
        email:state.auth.email,
        code:state.auth.code,
        loading:state.auth.loading,
        err:state.auth.errorMessageCode
    }
}

export default connect(mapStateToProps, actions)(RegistrationConfirmForm)