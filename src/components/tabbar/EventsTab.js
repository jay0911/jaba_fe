import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Icon from 'react-native-vector-icons/Ionicons'
import {View,TouchableWithoutFeedback} from 'react-native'
import {PLANNING,HAPPENING,PAST,CANCELLED} from '../../constants/EventStatusConstants'

class EventsTab extends Component{

    init(){    
        this.props.loadEvents(this.props.id,PLANNING.value) ;
        this.props.navigation.navigate('Events')
    }

    componentDidMount(){
        //this.props.loadEvents(this.props.id,PLANNING.value)
       // this.props.loadEvents(this.props.id,HAPPENING.value)
        //this.props.loadEvents(this.props.id,PAST.value)
       // this.props.loadEvents(this.props.id,CANCELLED.value)
    }

    render(){
        return (
            <TouchableWithoutFeedback onPress={()=>this.init()}>
                <View>
                    <Icon name="ios-images" />
                </View>
            </TouchableWithoutFeedback>
        )
    }
}


const mapStateToProps = state => {
    return {
      id:state.idReducer.accountId,
      eventPage:state.mainTabReducer.eventPage
    }
  }
export default connect(mapStateToProps, actions)(EventsTab)

