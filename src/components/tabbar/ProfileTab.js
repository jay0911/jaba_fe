import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Icon from 'react-native-vector-icons/Ionicons'
import {View,TouchableWithoutFeedback} from 'react-native'

class ProfileTab extends Component{


    init(navigateMe){    
        console.log('prefilling'+this.props.id)
        this.props.prefillMyProfile(this.props.id,this.props.navigation,navigateMe)
    }

    componentDidMount(){
        this.init(false);
    }

    render(){
        return (
            <View>
                <Icon name="ios-person" />
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        id:state.idReducer.accountId
    }
}

export default connect(mapStateToProps, actions)(ProfileTab)

