import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Icon from 'react-native-vector-icons/Ionicons'
import {View,TouchableWithoutFeedback} from 'react-native'
import {loadProduct} from '../../helpers/ProductSearchHelper'
import AsyncStorage from '@react-native-community/async-storage'

class SavedTab extends Component{


    init(){    

        this.props.initSavedProduct();

        //future ref this is for loading while api is called
        this.props.productSavedLoadMoreTrue();

        this.loadmore(0,[]);
        this.props.navigation.navigate('Saved')
    }

    loadmore(offset,serverData){
        this.props.findSavedProductInit(this.props.id,offset,serverData);
    }

    render(){
        return (
            <TouchableWithoutFeedback onPress={()=>this.init()}>
                <View>
                    <Icon name="heart-outline" />
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const mapStateToProps = state => {
    return {
        serverData:state.savedProduct.serverData,
        offset:state.savedProduct.offset,
        id:state.idReducer.accountId
    }
}

export default connect(mapStateToProps, actions)(SavedTab)

