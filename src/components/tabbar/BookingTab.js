import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Icon from 'react-native-vector-icons/Ionicons'
import {View,TouchableWithoutFeedback} from 'react-native'
import {PENDING,CANCELLED,APPROVED,BOOKED} from '../../constants/BookingStatusConstants'

class BookingTab extends Component{

    init(){    
        this.props.loadBookings(this.props.id,PENDING.value)
        this.props.navigation.navigate('Bookings')
    }

    componentDidMount(){
        //this.props.loadBookings(this.props.id,PENDING.value)
        //this.props.loadBookings(this.props.id,CANCELLED.value)
        //this.props.loadBookings(this.props.id,APPROVED.value)
        //this.props.loadBookings(this.props.id,BOOKED.value)
    }

    render(){
        return (
            <TouchableWithoutFeedback onPress={()=>this.init()}>
                <View>
                    <Icon name="book" />
                </View>
            </TouchableWithoutFeedback>
        )
    }
}


const mapStateToProps = state => {
    return {
      id:state.idReducer.accountId,
      bookingPage:state.mainTabReducer.bookingPage
    }
  }
export default connect(mapStateToProps, actions)(BookingTab)

