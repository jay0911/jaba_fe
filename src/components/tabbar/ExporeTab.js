import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import Icon from 'react-native-vector-icons/Ionicons'
import {View,TouchableWithoutFeedback} from 'react-native'
import {loadProduct} from '../../helpers/ProductSearchHelper'

class ExploreTab extends Component{

    init(){    
        this.props.initfindProduct();
        this.loadmore(0,[]);
        this.props.navigation.navigate('Explore')
    }

    loadmore(offset,serverData){
        console.log('------------searching-')
        loadProduct(this.props.findProductInit,serverData,offset,this.props.myid)
    }

    render(){
        return (
            <TouchableWithoutFeedback onPress={()=>this.init()}>
                <View>
                    <Icon name="ios-search" />
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const mapStateToProps = state => {
    return {
        serverData:state.product.serverData,
        offset:state.product.offset,
        myid:state.idReducer.accountId
    }
}

export default connect(mapStateToProps, actions)(ExploreTab)

