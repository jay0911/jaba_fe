import React from 'react'
import {Text,View, TouchableOpacity} from 'react-native'

const ButtonTransparent = ({onPress,children}) => {
    return (
        <View style={styles.containerStyle}>
            <TouchableOpacity
                style={styles.buttonStyle}
                onPress={onPress}
            >
                <Text style={styles.textStyle}>
                    {children}
                </Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = {
    containerStyle:{
        justifyContent:'flex-start',
        flexDirection: 'row',
        position:'relative'
    },
    textStyle:{
        alignSelf:'center',
        color:'#d60909',
        fontSize:16,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10
    },
    buttonStyle:{
        flex:1,
        alignSelf: 'stretch',
        backgroundColor:'#fff',
        borderColor:'#d60909',
        borderRadius:5,
        borderWidth:1,
    }
}

export default ButtonTransparent