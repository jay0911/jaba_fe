import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import {MAIN_SERVICE} from '../api/backendserver'

const Item = ({result}) => {
    return (
    <View style={styles.container}>
        <Image resizeMode='contain' style={styles.image} source={{ uri: `${MAIN_SERVICE}/image/bucket-image/${result.images[0].imageId}` }}/>
        <View style={styles.info}>
            <Text style={styles.name}>{result.name}</Text>
            <Text>{result.category.name}</Text>
            <Text>{result.price}</Text>
        </View>
    </View>
    )
};

const styles = StyleSheet.create({
    container:{
        marginLeft:15,
        borderRadius:5,
        borderWidth:1,
        borderColor: '#dbdbdb',
    },
    image:{
        width:250,
        height:120,
        borderRadius: 4,
        marginBottom:1
    },
    info:{
        padding:10
    },
    name:{
        fontWeight:'bold'
    }
})

export default Item;