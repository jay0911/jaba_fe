import React from 'react'
import {Text,View, TouchableOpacity} from 'react-native'

const Button = ({onPress,children}) => {
    return (
        <View style={styles.containerStyle}>
            <TouchableOpacity
                style={styles.buttonStyle}
                onPress={onPress}
            >
                <Text style={styles.textStyle}>
                    {children}
                </Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = {
    containerStyle:{
        justifyContent:'flex-start',
        flexDirection: 'row',
        position:'relative'
    },
    textStyle:{
        alignSelf:'center',
        color:'#fff',
        fontSize:16,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10
    },
    buttonStyle:{
        flex:1,
        alignSelf: 'stretch',
        backgroundColor:'#d60909',
        borderRadius:5,
        borderWidth:1,
        borderColor:'#d60909',
        marginLeft:10,
        marginRight:10
    }
}

export default Button