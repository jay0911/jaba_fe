import React from 'react'
import {Text,View, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

const CommonSearchHeaderFlow = ({onPress,children}) => {
    return (
        <View style={{flexDirection:'row',paddingBottom:20}}>
            <View style={{padding:20}}>
                <TouchableOpacity onPress={onPress}>
                    <Icon name="ios-arrow-back" />
                </TouchableOpacity>
            </View>
            <View style={{paddingHorizontal:5,flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'stretch',
                }}>
                {children.map((item, index) => {
                    return (children[index]!=''?<Text key={index} style={{textAlign: 'center',fontSize:20,fontWeight:'bold',paddingTop:5}}>{item}</Text>:null) 
                })}
            </View>
            <View style={{padding:20}}>
            </View>
        </View>
    )
}

const styles = {

}

export default CommonSearchHeaderFlow