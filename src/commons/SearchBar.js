import React from 'react';
import {
  StyleSheet,
  TextInput,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
 
const SearchBar = ({term,onTermChange,onTermSubmit,onFocuscode}) => {
    return (
    <View style={styles.backgroundStyle}>
        <Icon name="search" style={styles.iconStyle} />
        <TextInput
        onEndEditing={onTermSubmit}
        autoCapitalize='none'
        autoCorrect={false}
        style={styles.inputStyle} 
        placeholder="Search"
        onFocus={onFocuscode}
        value={term}
        onChangeText={onTermChange} />
    </View>
    )
};

const styles = StyleSheet.create({
    backgroundStyle: {
        backgroundColor:'#f2f3f2',
        marginTop: 15,
        height:40,
        borderRadius:5,
        marginHorizontal: 15,
        flexDirection: 'row',
        marginBottom: 10
    },
    inputStyle:{
        borderColor: 'black',
        flex:1,
        fontSize: 12
    },
    iconStyle:{
        fontSize:35,
        alignSelf:'center',
        marginHorizontal:15
    }
})

export default SearchBar;