import React from 'react';
import {View,Modal,Text} from 'react-native'
import CardSection from './CardSection'

const ModalCustom = ({children,modalVisible,errorMessage}) => {
    return (
        <Modal
        visible={modalVisible}
        transparent
        animationType="slide"
        onRequestClose={() => {}}
        >
        <View style={{ position: 'relative',
                        borderRadius:4,
                        flex: 1,
                        paddingHorizontal:40,
                        justifyContent: 'center'}}>
            <CardSection style={{justifyContent: 'center'}}>
            <Text style={{
                    flex: 1,
                    fontSize: 18,
                    textAlign: 'center',
                    lineHeight: 40
                }} >
                {errorMessage}
            </Text>
            </CardSection>

            <CardSection>
            <View style={{flex:1}}>
                {children}
            </View>
            </CardSection>
        </View>
    </Modal>
    )
}

const styles = {
    containerStyle:{
        borderBottomWidth:1,
        padding:5,
        backgroundColor:'#fff',
        justifyContent:'flex-start',
        flexDirection: 'row',
        borderColor:'#ddd',
        position:'relative'
    }
}

export default ModalCustom;