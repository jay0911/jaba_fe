import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';
import {MAIN_SERVICE} from '../api/backendserver'

const ReviewItem = ({result}) => {
    return (
    <View style={styles.outerContainer}>
        <View style={styles.container}>
            <Image  style={styles.image} source={{ uri: `${MAIN_SERVICE}/image/bucket-image/${result.imageId}` }}/>
            <View style={styles.reviewInfo}>
                <Text style={styles.name}>{result.name}</Text> 
                <Text style={{fontSize:12}}>{result.dateTime}</Text>
            </View>
            <View style={styles.reviewPoints} >
                <Image style={styles.starImage} source={require('../assets/star.png')} />
                <Text>{result.rating}</Text>
            </View>
        </View>
        
        <Text numberOfLines={5} style={{flex:2,margin:10}}>{result.message}</Text>
    </View>
    )
};

const styles = StyleSheet.create({
    reviewPoints:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end',
        marginTop:10,
        paddingRight:20,

    },
    reviewInfo:{
        flex:4,
        marginTop:10,

    }, 
    outerContainer:{
        marginLeft:15,
        borderRadius:5,
        borderWidth:1,
        borderColor: '#dbdbdb',
        width:250,
        height:150,
    },
    container:{
        flex:1,
        flexDirection:'row',
        justifyContent: 'space-between',
        paddingBottom: 10
    },
    starImage:{
        width: 20,
        height: 20,
    },
    image: {
        width: 50,
        height: 50,
        borderRadius: 100,
        overflow: "hidden",
        margin:10
    },
    name:{
        fontWeight:'bold'
    }
})

export default ReviewItem;