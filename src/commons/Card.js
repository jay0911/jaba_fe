import React from 'react';
import {View} from 'react-native'

const Card = (props) => {
    return (
        <View style={styles.containerStyle}>
            {props.children}   
        </View>
    )
}

const styles = {
    containerStyle:{
        borderWidth:1,
        borderRadius:10,
        borderColor:'#ddd',
        borderBottomWidth:0,
        shadowColor:'#000',
        shadowOffset: {width:0,height:10},
        shadowOpacity: 0.1,
        shadowRadius: 10,
        elevation: 10,
        marginLeft:5,
        marginRight:5,
        marginBottom:20
    }
}

export default Card;