import React from 'react'
import Icon from 'react-native-vector-icons/Ionicons'

const IconHeart = ({isSaved}) => {
    console.log(isSaved)
    return (
        isSaved==='true'?<Icon name="heart-sharp" />:<Icon name="heart-outline" />
    )
}

export default IconHeart