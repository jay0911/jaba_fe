import React from 'react'
import { View, StyleSheet, Image, Dimensions } from 'react-native'
import {MAIN_SERVICE} from '../api/backendserver'

const { width, height } = Dimensions.get('window')

const CarouselItem = ({ item }) => {
    return (
        <View style={styles.cardView}>
            <Image resizeMode='contain' style={styles.image} source={{ uri: `${MAIN_SERVICE}/image/bucket-image/${item.imageId}` }} />
        </View>
    )
}

const styles = StyleSheet.create({
    cardView: {
        flex: 1,
        width: width - 20,
        height: height / 3,
    },
    image: {
        width: width - 20,
        height: height / 3,
        borderRadius: 10,
    }
})

export default CarouselItem