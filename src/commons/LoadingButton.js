import React from 'react'
import {View,ActivityIndicator,TouchableOpacity} from 'react-native'

const LoadingButton = () => {
    return (
        <View style={styles.containerStyle}>
            <TouchableOpacity
                disabled={true}
                style={styles.buttonStyle}
            >
            <View style={styles.activityStyle}>
                <ActivityIndicator size={'small'} />
            </View>
            </TouchableOpacity>
        </View>
    )
}

const styles = {
    activityStyle:{
        padding:10
    },
    containerStyle:{
        justifyContent:'flex-start',
        flexDirection: 'row',
        position:'relative'
    },
    textStyle:{
        alignSelf:'center',
        color:'#fff',
        fontSize:16,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10
    },
    buttonStyle:{
        flex:1,
        alignSelf: 'stretch',
        backgroundColor:'#d60909',
        borderRadius:5,
        borderWidth:1,
        borderColor:'#d60909',
        marginLeft:5,
        marginRight:5
    }
}

export default LoadingButton