import React from 'react'
import {
  StyleSheet,
  Image,
  View,
  KeyboardAvoidingView,ScrollView
} from 'react-native';
import {Text,SocialIcon} from 'react-native-elements'
import Spacer from '../commons/Spacer'
import { TouchableOpacity } from 'react-native-gesture-handler';
import LoginForm from '../components/LoginForm'

const LoginScreen = ({navigation}) => {
  
  return(
    <KeyboardAvoidingView
    behavior={'height'} style={{flexGrow:1,height:'100%'}}
    >
      <Image style={styles.image} source={require('../assets/logo_badge.png')} />
      <Spacer>
        <Text h3>Start your event now!</Text>
      </Spacer>
      
      <Text style={styles.appName}>App name here</Text>
      <ScrollView>
      <LoginForm navigation={navigation} />

      <View style={styles.otherSignIn}>
        <TouchableOpacity style={styles.imageText}>
        <SocialIcon
          style={styles.button}
          type='facebook'
        /><Text style={styles.textInImageFb}>Sign In With Facebook</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.imageText}>
        <SocialIcon
          style={styles.button}
          type='google'
        /><Text style={styles.textInImageGoogle}>Sign In With Google</Text>
        </TouchableOpacity>
      </View>
      </ScrollView>
      <View style={styles.signUp}>
        <Text style={styles.textFooter}>Don't have an account? </Text>
        <TouchableOpacity onPress={()=>navigation.navigate('Registration')}>
          <Text style={styles.textFooterLink}>Sign Up</Text>
        </TouchableOpacity>
      </View>
    </KeyboardAvoidingView >
  )  
};

const styles = StyleSheet.create({
  otherSignIn:{
    paddingTop:10
  },
  textFooter:{
    color:'#808080'
  },
  textFooterLink:{
    color:'#3b5998'
  },
  signUp:{
    paddingTop:50,
    flex:1,
    flexDirection:'row',
    justifyContent: 'center'
  },
  imageText:{
    flexDirection:'row',
    flexWrap:'wrap',
    marginLeft: 100
  },
  textInImageFb:{
    marginTop:11,
    marginLeft:12,
    color:'#3b5998'
  },
  textInImageGoogle:{
    marginTop:11,
    marginLeft:12,
    color:'#EA4335'
  },
  image:{
    marginLeft:20,
    width: 100,
    height: 125,
    resizeMode: 'contain' 
  },
  button:{
    height:30,
    width:30,
    borderRadius:4,
    resizeMode: 'contain' 
  },
  appName:{
    textAlign:'center'
  }
})

LoginScreen.navigationOptions = () =>{
  return {
    title: 'Login',
    headerShown: false
  }
}

export default LoginScreen;