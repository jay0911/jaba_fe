import React,{useState} from 'react'
import {
  StyleSheet,
} from 'react-native';
import RegistrationConfirmForm from '../components/RegistrationConfirmForm'

const RegistrationEmailConfirmScreen = ({navigation}) => {
  return(
    <RegistrationConfirmForm navigation={navigation}/>
   )
};

const styles = StyleSheet.create({
})

RegistrationEmailConfirmScreen.navigationOptions = {
  headerTitle: ''
}

export default RegistrationEmailConfirmScreen;