import React,{useState} from 'react'
import {
  StyleSheet,
  Image,
  View
} from 'react-native';
import RegistrationFinishedForm from './../components/RegistrationFinishedForm'

const RegistrationFinishedScreen = ({navigation}) => {
    return (
      <RegistrationFinishedForm navigation={navigation} />
    )
  };
  

const styles = StyleSheet.create({
})

RegistrationFinishedScreen.navigationOptions = {
  headerTitle: ''
}

export default RegistrationFinishedScreen;