import React,{useState} from 'react'
import {
  StyleSheet,
} from 'react-native';
import RegistrationEmailForm from './../components/RegistrationEmailForm'

const RegistrationEmailScreen = ({navigation}) => {
  return(
    <RegistrationEmailForm navigation={navigation} />
  )
};

const styles = StyleSheet.create({
})

RegistrationEmailScreen.navigationOptions = {
  headerTitle: ''
}

export default RegistrationEmailScreen;