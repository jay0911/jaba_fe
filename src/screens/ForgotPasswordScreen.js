import React,{useState} from 'react'
import {
  StyleSheet,
} from 'react-native';
import ForgotPasswordForm from '../components/ForgotPasswordForm'

const ForgotPasswordScreen = ({navigation}) => {
  return(
    <ForgotPasswordForm navigation={navigation}/>
   )
};

const styles = StyleSheet.create({
})

ForgotPasswordScreen.navigationOptions = {
  headerTitle: ''
}

export default ForgotPasswordScreen;