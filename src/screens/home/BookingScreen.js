import * as React from 'react';
import { View, StyleSheet,TouchableOpacity,Text , Dimensions } from 'react-native';
import { TabView, SceneMap,TabBar } from 'react-native-tab-view';
import BookingPending from '../../components/bookings/BookingPending'
import BookingBooked from '../../components/bookings/BookingBooked'
import BookingCancelReject from '../../components/bookings/BookingCancelReject'
import BookingApproved from '../../components/bookings/BookingApproved'
import Icon from 'react-native-vector-icons/Ionicons'
import BookingCount from '../../components/bookings/BookingCount'
import {PENDING,CANCELLED,APPROVED,BOOKED} from '../../constants/BookingStatusConstants'
import Loader from '../../commons/Loader'
import BookingTab from '../../components/tabbar/BookingTab'
import * as actions from '../../actions'
import { connect } from 'react-redux'

const initialLayout = { width: Dimensions.get('window').width };

const BookingScreen = ({navigation,loadBookings,id,bookingPage}) => {

  const FirstRoute = () => (
    <View style={[styles.scene, { backgroundColor: 'white' }]} >
      <BookingPending navigation={navigation} />
    </View>
  );
   
  const SecondRoute = () => (
    <View style={[styles.scene, { backgroundColor: 'white' }]} >
      <BookingApproved navigation={navigation} />
    </View>
  );
  
  const ThirdRoute = () => (
    <View style={[styles.scene, { backgroundColor: 'white' }]} >
      <BookingBooked navigation={navigation} />
    </View>
  );

  const FourthRoute = () => (
    <View style={[styles.scene, { backgroundColor: 'white' }]} >
      <BookingCancelReject navigation={navigation} />
    </View>
  );
  
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: PENDING.displayName },
    { key: 'second', title: APPROVED.displayName },
    { key: 'third', title: BOOKED.displayName },
    { key: 'fourth', title: CANCELLED.displayName },
  ]);
 
  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: ThirdRoute,
    fourth: FourthRoute,
  });
 
  const renderTabBar = props => (
    <TabBar
      {...props}
      getLabelText={({ route }) => route.title}
      indicatorStyle={{ backgroundColor: 'red' }}
      style={{ backgroundColor: 'white' }}
      renderLabel={({ route, focused, color }) => (
        <BookingCount routeTitle={route.title} />
      )}
      onTabPress={({ route, preventDefault }) => {
        if (route.key === 'first') {
          loadBookings(id,PENDING.value)
        }
        if (route.key === 'second') {
          loadBookings(id,APPROVED.value)
        }
        if (route.key === 'third') {
          loadBookings(id,BOOKED.value)
        }
        if (route.key === 'fourth') {
          loadBookings(id,CANCELLED.value)
        }
      }}
    />
  );

  return(   <View style={{flex:1}}> 
      <Loader loading={bookingPage} />
    <View><Text style={{paddingHorizontal:20,paddingBottom:20,paddingTop:40,fontSize:30,fontWeight:'bold'}}>Bookings</Text></View>
    <TabView
    navigationState={{ index, routes }}
    renderTabBar={renderTabBar}
    renderScene={renderScene}
    onIndexChange={setIndex}
    initialLayout={initialLayout}
  />
  
  </View> )
        
};

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
});

BookingScreen.navigationOptions = ({navigation}) => ({
  headerTitle: '',
  headerVisible: false,
  tabBarIcon: () => {
    return <BookingTab navigation={navigation} />
  },
})


const mapStateToProps = state => {
  return {
    id:state.idReducer.accountId,
    bookingPage:state.mainTabReducer.bookingPage
  }
}

export default connect(mapStateToProps, actions)(BookingScreen);