import React,{useState} from 'react'
import {
  StyleSheet,
  View,
} from 'react-native';
import SavedList from '../../components/saved/SavedList'
import SavedTab from '../../components/tabbar/SavedTab'

const SavedScreen = ({navigation}) => {

  return(
    <View style={{flex:1}}>
      <SavedList navigation={navigation}/>
    </View> )
};

const styles = StyleSheet.create({})

SavedScreen.navigationOptions = ({navigation}) => ({
  headerTitle: '',
  headerVisible: false,
  tabBarIcon: () => {
    return <SavedTab navigation={navigation} />
  },
})

export default SavedScreen;