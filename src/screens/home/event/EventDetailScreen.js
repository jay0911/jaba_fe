import React from 'react'
import {
  StyleSheet,
  View,Text
} from 'react-native';
import EventDetail from '../../../components/events/EventDetail'

const EventDetailScreen = ({navigation}) => {

  return(<View>
        <EventDetail navigation={navigation} />
  </View> )
};

const styles = StyleSheet.create({})

EventDetailScreen.navigationOptions = () =>{
    return {
      title: '',
      headerShown: false,
    }
}

export default EventDetailScreen;