import React from 'react'
import {
  StyleSheet,
  View,Text
} from 'react-native';
import CreateEventForm from '../../../components/events/CreateEventForm'

const CreateEvent = ({navigation}) => {

  return(<View>
        <CreateEventForm navigation={navigation} />
  </View> )
};

const styles = StyleSheet.create({})

CreateEvent.navigationOptions = () =>{
    return {
      title: '',
      headerShown: false,
    }
}

export default CreateEvent;