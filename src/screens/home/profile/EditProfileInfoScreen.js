import React from 'react'
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import EditProfileInfo from '../../../components/profile/EditProfileInfo'

const EditProfileInfoScreen = ({navigation}) => {
  return(
    <View>
        <EditProfileInfo navigation={navigation} />
    </View> )
};

const styles = StyleSheet.create({})

EditProfileInfoScreen.navigationOptions = () =>{
  return {
    title: '',
    headerShown: false,
  }
}

export default EditProfileInfoScreen;