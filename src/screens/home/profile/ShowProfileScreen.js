import React from 'react'
import {
  StyleSheet,
  View,
} from 'react-native';

import ShowProfile from '../../../components/profile/ShowProfile'

const ShowProfileScreen = ({navigation}) => {
  return(
    <View>
        <ShowProfile navigation={navigation} />
    </View> )
};

const styles = StyleSheet.create({})

ShowProfileScreen.navigationOptions = () =>{
  return {
    title: '',
    headerShown: false,
  }
}

export default ShowProfileScreen;