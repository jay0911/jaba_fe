import * as React from 'react';
import { View, StyleSheet,Text , Dimensions } from 'react-native';
import { TabView, SceneMap,TabBar } from 'react-native-tab-view';
import PlanningForm from '../../components/events/PlanningForm'
import CancelForm from '../../components/events/CancelForm'
import HappeningForm from '../../components/events/HappeningForm'
import PastForm from '../../components/events/PastForm'
import EventCount from '../../components/events/EventCount'
import {PLANNING,HAPPENING,PAST,CANCELLED} from '../../constants/EventStatusConstants'
import * as actions from '../../actions'
import { connect } from 'react-redux'
import EventsTab from '../../components/tabbar/EventsTab'
import Loader from '../../commons/Loader'

const initialLayout = { width: Dimensions.get('window').width };

const EventsScreen = ({navigation,loadEvents,id,eventPage}) => {

  const FirstRoute = () => (
    <View style={[styles.scene, { backgroundColor: 'white' }]} >
      <PlanningForm navigation={navigation} />
    </View>
  );
   
  const SecondRoute = () => (
    <View style={[styles.scene, { backgroundColor: 'white' }]} >
      <HappeningForm navigation={navigation} />
    </View>
  );
  
  const ThirdRoute = () => (
    <View style={[styles.scene, { backgroundColor: 'white' }]} >
      <PastForm navigation={navigation} />
    </View>
  );

  const FourthRoute = () => (
    <View style={[styles.scene, { backgroundColor: 'white' }]} >
      <CancelForm navigation={navigation} />
    </View>
  );
  
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: PLANNING.displayName },
    { key: 'fourth', title: CANCELLED.displayName },
    { key: 'second', title: HAPPENING.displayName },
    { key: 'third', title: PAST.displayName },
  ]);
 
  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: ThirdRoute,
    fourth: FourthRoute,
  });
 
  const renderTabBar = props => (
    <TabBar
      {...props}
      getLabelText={({ route }) => route.title}
      indicatorStyle={{ backgroundColor: 'red' }}
      style={{ backgroundColor: 'white' }}
      renderLabel={({ route, focused, color }) => (
        <EventCount routeTitle={route.title} />
      )}
      onTabPress={({ route, preventDefault }) => {
        if (route.key === 'first') {
          loadEvents(id,PLANNING.value)
        }
        if (route.key === 'second') {
          loadEvents(id,CANCELLED.value)
        }
        if (route.key === 'third') {
          loadEvents(id,HAPPENING.value)
        }
        if (route.key === 'fourth') {
          loadEvents(id,PAST.value)
        }
      }}
    />
  );

  return(   <View style={{flex:1}}> 
    <Loader loading={eventPage} />
    <View><Text style={{paddingHorizontal:20,paddingBottom:20,paddingTop:40,fontSize:30,fontWeight:'bold'}}>Events</Text></View>
    <TabView
    navigationState={{ index, routes }}
    renderTabBar={renderTabBar}
    renderScene={renderScene}
    onIndexChange={setIndex}
    initialLayout={initialLayout}
  />
  
  </View> )
        
};

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
});

EventsScreen.navigationOptions = ({navigation}) => ({
  headerTitle: '',
  headerVisible: false,
  tabBarIcon: () => {
    return <EventsTab navigation={navigation} />
  },
})


const mapStateToProps = state => {
  return {
    id:state.idReducer.accountId,
    eventPage:state.mainTabReducer.eventPage
  }
}

export default connect(mapStateToProps, actions)(EventsScreen);
