import React from 'react'
import {
  StyleSheet,
  View,Text
} from 'react-native';
import PayFailCard from '../../../components/bookings/PayFailCard'

const PayFailCardScreen = ({navigation}) => {

  return(<View>
        <PayFailCard navigation={navigation} />
  </View> )
};

const styles = StyleSheet.create({})

PayFailCardScreen.navigationOptions = () =>{
    return {
      title: '',
      headerShown: false,
    }
}

export default PayFailCardScreen;