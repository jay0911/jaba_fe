import React from 'react'
import {
  StyleSheet,
  View,Text
} from 'react-native';
import BookingDetail from '../../../components/bookings/BookingDetail'

const BookingDetailScreen = ({navigation}) => {

  return(<View>
        <BookingDetail navigation={navigation} />
  </View> )
};

const styles = StyleSheet.create({})

BookingDetailScreen.navigationOptions = () =>{
    return {
      title: '',
      headerShown: false,
    }
}

export default BookingDetailScreen;