import React from 'react'
import {
  StyleSheet,
  View,Text
} from 'react-native';
import PayBooking from '../../../components/bookings/PayBooking'

const PayBookingScreen = ({navigation}) => {

  return(<View>
        <PayBooking navigation={navigation} />
  </View> )
};

const styles = StyleSheet.create({})

PayBookingScreen.navigationOptions = () =>{
    return {
      title: '',
      headerShown: false,
    }
}

export default PayBookingScreen;