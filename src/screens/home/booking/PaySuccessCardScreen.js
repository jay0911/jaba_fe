import React from 'react'
import {
  StyleSheet,
  View,Text
} from 'react-native';
import PaySuccessCard from '../../../components/bookings/PaySuccessCard'

const PaySuccessCardScreen = ({navigation}) => {

  return(<View>
        <PaySuccessCard navigation={navigation} />
  </View> )
};

const styles = StyleSheet.create({})

PaySuccessCardScreen.navigationOptions = () =>{
    return {
      title: '',
      headerShown: false,
    }
}

export default PaySuccessCardScreen;