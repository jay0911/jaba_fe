import React, { Component,useEffect  } from 'react'
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import ProfileTab from '../../components/tabbar/ProfileTab'
import ProfileForm from '../../components/ProfileForm'
import { useDispatch,connect } from "react-redux";
import * as actions from '../../actions'

const ProfileScreen = ({navigation,id,tryme,prefillMyProfile}) => {

  useEffect(() => {
    navigation.setParams({ tabBarOnPress: tryme('hello')});
  }, []);

  return(<View style={{flex:1,marginBottom:70}}>
        <ProfileForm navigation={navigation}/>
  </View> )
};

ProfileScreen.navigationOptions = ({navigation,id}) => ({
  tabBarIcon: () => {
    return <ProfileTab navigation={navigation} />
  },
  tabBarOnPress: ({ navigation, defaultHandler }) => {

    // do extra stuff here
    //navigation.getParam('tabBarOnPress')();
    
    console.log(navigation.getParam('tabBarOnPress'))
    navigation.setParams({ tabBarOnPress: navigation.getParam('tabBarOnPress')});

    defaultHandler(); // Call the default handler to actually switch tabs
  }
})

function mapStateToProps(state) {
  return {id:state.idReducer.accountId}
}
export default connect(mapStateToProps,actions)(ProfileScreen)