import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  Button
} from 'react-native';

const ProductItemDetailScreen = ({navigation}) => {
  return(
    <View>
        <Text>ProductItemDetailScreen</Text>
        <Button title="Go to TrackDetail"
      onPress={()=>navigation.navigate('Explore')} />
      <Button title="Go to TrackDetail"
      onPress={()=>navigation.navigate('ProductBundleDetail')} />
    </View> )
};

const styles = StyleSheet.create({})

ProductItemDetailScreen.navigationOptions = () =>{
  return {
    title: '',
    headerShown: false,
  }
}

export default ProductItemDetailScreen;