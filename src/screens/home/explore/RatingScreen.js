import React from 'react'
import {
  StyleSheet,
  View,
  Text
} from 'react-native';

import RatingForm from '../../../components/explore/RatingForm'

const RatingScreen = ({navigation}) => {
  return(
    <View>
        <RatingForm navigation={navigation} />
    </View> )
};

const styles = StyleSheet.create({})

RatingScreen.navigationOptions = () =>{
  return {
    title: '',
    headerShown: false,
  }
}

export default RatingScreen;