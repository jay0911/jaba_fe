import React from 'react'
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import ProductBundleDetailForm from '../../../components/ProductBundleDetailForm'

const ProductBundleDetailScreen = ({navigation}) => {
  return(
    <View>
      <ProductBundleDetailForm navigation={navigation} route={navigation.getParam('fromScreen')} />
    </View> )
};

const styles = StyleSheet.create({})

ProductBundleDetailScreen.navigationOptions = () =>{
  return {
    title: '',
    headerShown: false,
  }
}

export default ProductBundleDetailScreen;