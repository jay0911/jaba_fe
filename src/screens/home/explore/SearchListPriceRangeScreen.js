import React from 'react'
import {
  StyleSheet,
  View,
  TextInput
} from 'react-native';

import SearchListPriceRange from '../../../components/search/SearchListPriceRange'

const SearchListPriceRangeScreen = ({navigation}) => {
  return(
    <View>
        <SearchListPriceRange navigation={navigation} />
    </View> )
};

const styles = StyleSheet.create({})

SearchListPriceRangeScreen.navigationOptions = () =>{
  return {
    title: '',
    headerShown: false,
  }
}

export default SearchListPriceRangeScreen;