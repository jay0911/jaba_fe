import React from 'react'
import {
  StyleSheet,
  View,
  TextInput
} from 'react-native';

import SearchList from '../../../components/search/SearchList'

const SearchListScreen = ({navigation}) => {
  return(
    <View>
        <SearchList navigation={navigation} />
    </View> )
};

const styles = StyleSheet.create({})

SearchListScreen.navigationOptions = () =>{
  return {
    title: '',
    headerShown: false,
  }
}

export default SearchListScreen;