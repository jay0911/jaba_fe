import React from 'react'
import {
  StyleSheet,
  View,
  Text
} from 'react-native';

import StartBooking from '../../../components/explore/StartBooking'

const StartBookingScreen = ({navigation}) => {
  return(
    <View>
        <StartBooking navigation={navigation} />
    </View> )
};

const styles = StyleSheet.create({})

StartBookingScreen.navigationOptions = () =>{
  return {
    title: '',
    headerShown: false,
  }
}

export default StartBookingScreen;