import React from 'react'
import {
  StyleSheet,
  View,
  TextInput
} from 'react-native';

import SearchListCategory from '../../../components/search/SearchListCategory'

const SearchListScreenCategory = ({navigation}) => {
  return(
    <View>
        <SearchListCategory navigation={navigation} />
    </View> )
};

const styles = StyleSheet.create({})

SearchListScreenCategory.navigationOptions = () =>{
  return {
    title: '',
    headerShown: false,
  }
}

export default SearchListScreenCategory;