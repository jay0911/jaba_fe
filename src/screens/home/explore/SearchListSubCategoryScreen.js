import React from 'react'
import {
  StyleSheet,
  View,
  TextInput
} from 'react-native';

import SearchListSubCategory from '../../../components/search/SearchListSubCategory'

const SearchListSubCategoryScreen = ({navigation}) => {
  return(
    <View>
        <SearchListSubCategory navigation={navigation} />
    </View> )
};

const styles = StyleSheet.create({})

SearchListSubCategoryScreen.navigationOptions = () =>{
  return {
    title: '',
    headerShown: false,
  }
}

export default SearchListSubCategoryScreen;