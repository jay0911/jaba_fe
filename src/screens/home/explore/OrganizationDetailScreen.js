import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  Button
} from 'react-native';
import OrganizationInfo from '../../../components/explore/OrganizationInfo'

const OrganizationDetailScreen = ({navigation}) => {
  return(
    <View>
        <OrganizationInfo navigation={navigation} />
    </View> )
};

const styles = StyleSheet.create({})

OrganizationDetailScreen.navigationOptions = () =>{
  return {
    title: '',
    headerShown: false,
  }
}

export default OrganizationDetailScreen;