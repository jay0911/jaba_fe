import React from 'react'
import {
  StyleSheet,
  View
} from 'react-native';
import ProductBundleSearch from '../../components/ProductBundleSearch'
import ExploreTab from '../../components/tabbar/ExporeTab'

const ExploreScreen = ({navigation}) => {

  return(<View style={{flex:1,marginBottom:70}}>
        <ProductBundleSearch navigation={navigation}/>
  </View> )
};

const styles = StyleSheet.create({})

ExploreScreen.navigationOptions = ({navigation}) => ({
  headerTitle: '',
  headerVisible: false,
  tabBarIcon: () => {
    return <ExploreTab navigation={navigation} />
  },
})

export default ExploreScreen;