export const dummyData = [
    {
       "title":"Taylor Swift",
       "description":"Taylor Swift",
       "url":"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg",
       "id":1
    },
    {
       "title":"Fearless",
       "description":"Taylor Swift",
       "url":"https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg",
       "id":2
    },
    {
       "title":"Speak Now",
       "description":"Taylor Swift",
       "url":"https://images-na.ssl-images-amazon.com/images/I/51vlGuX7%2BFL.jpg",
       "id":3
    },
    {
       "title":"Red",
       "artist":"Taylor Swift",
       "url":"https://images-na.ssl-images-amazon.com/images/I/41j7-7yboXL.jpg",
       "id": 4
    }
 ]